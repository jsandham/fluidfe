//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************


#ifndef FEMODEL_H
#define FEMODEL_H

#include"BoundaryData.h"

class FeModel
{
  public:            
    BoundaryData *tdata;  //object containing information on boundary conditions for temperature
    BoundaryData *udata;  //object containing information on boundary conditions for u-velocity
    BoundaryData *vdata;  //object containing information on boundary conditions for v-velocity
    BoundaryData *wdata;  //object containing information on boundary conditions for w-velocity

  private:
    double theta;           //theta parameter in theta time stepping method   
    double kappa;           //thermal conductivity coefficient
    double Re;              //Reynolds number        
    double T;               //final time                            
    double dt;              //timestep size                                
    int steps;              //number of timesteps                            
    bool time;              //time dependence flag
                                               
  public: 
    FeModel(BoundaryData *td, BoundaryData *ud, BoundaryData *vd, BoundaryData *wd);
    ~FeModel();

    void set_theta(double i);
    void set_kappa(double i);
    void set_Re(double i);
    void set_T(double i);
    void set_dt(double i);
    void set_steps(int i);
    void set_time(bool i);
   
    double get_theta();
    double get_kappa();
    double get_Re();
    double get_T();
    double get_dt();
    int get_steps();
    bool get_time();
};

#endif
