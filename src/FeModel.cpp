//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#include"FeModel.h"
#include"BoundaryData.h"


//-----------------------------------------------------------------------------
// Constructor for Finite Element Model
//-----------------------------------------------------------------------------
FeModel::FeModel(BoundaryData *td, BoundaryData *ud, BoundaryData *vd, BoundaryData *wd)
{
  tdata = td;
  udata = ud;
  vdata = vd;
  wdata = wd;
}  



//-----------------------------------------------------------------------------
// Destructor for Finite Element Model
//-----------------------------------------------------------------------------
FeModel::~FeModel()
{
 
}


//-----------------------------------------------------------------------------
// set methods
//-----------------------------------------------------------------------------
void FeModel::set_theta(double i)
{
  theta = i;
}


void FeModel::set_kappa(double i)
{
  kappa = i;
}


void FeModel::set_Re(double i)
{
  Re = i;
}


void FeModel::set_T(double i)
{
  T = i;
}


void FeModel::set_dt(double i)
{
  dt = i;
}


void FeModel::set_steps(int i)
{
  steps = i;  
}


void FeModel::set_time(bool i)
{
  time = i;
}


//-----------------------------------------------------------------------------
// get methods
//-----------------------------------------------------------------------------
double FeModel::get_theta()
{
  return theta;
}


double FeModel::get_kappa()
{
  return kappa;
}


double FeModel::get_Re()
{
  return Re;
}


double FeModel::get_T()
{
  return T;
}


double FeModel::get_dt()
{
  return dt;
}


int FeModel::get_steps()
{
  return steps;
}


bool FeModel::get_time()
{
  return time;
}
