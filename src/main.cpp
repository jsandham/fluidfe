//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************
#include <iostream>
#include "Mesh.h"
#include "BoundaryData.h"
#include "FeModel.h"
#include "Solver.h"

#include "IterSolvers.h"
#include "Tree.h" 




//********************************************************************************
//
// Main Program 
//
//********************************************************************************
int main(int argc, char *argv[])
{
  //Mesh objects
  Mesh *vmesh = new Mesh(argv[1]);   //velocity mesh
  Mesh *pmesh = new Mesh(argv[2]);   //pressure mesh

  //define boundary data
  BoundaryData *tdata = new BoundaryData(vmesh);  //boundary conditions for temperature
  BoundaryData *udata = new BoundaryData(vmesh);  //boundary conditions for u-velocity
  BoundaryData *vdata = new BoundaryData(vmesh);  //boundary conditions for v-velocity
  BoundaryData *wdata = new BoundaryData(vmesh);  //boundary conditions for w-velocity

  tdata->addDirichlet(vmesh->get_group(0),-2.0);
  tdata->addDirichlet(vmesh->get_group(1),5.0);
  udata->addDirichlet(vmesh->get_group(0),0.0);
  udata->addDirichlet(vmesh->get_group(1),1.0);
  vdata->addDirichlet(vmesh->get_group(0),0.0);
  vdata->addDirichlet(vmesh->get_group(1),0.0);

  //define finite element model
  FeModel *fem = new FeModel(tdata,udata,vdata,wdata);

  fem->set_time(true);
  fem->set_dt(0.025);
  fem->set_steps(50);
  fem->set_theta(1);
  fem->set_kappa(10);
  fem->set_Re(10);

  //create solver objects and solve
  //SolverPoisson solver1(vmesh,fem);        //create solver object and pass model to constructor
  SolverHeat solver2(vmesh,fem);           //create solver object and pass model to constructor
  //SolverFluid solver3(vmesh,pmesh,fem);  //create solver object and pass model to constructor

  //solver1.solve();     //solve Global system
  solver2.solve();     //solve Global system 
  //solver3.solve();     //solve Global system

  std::cout<<"Ne: "<<vmesh->get_Ne()<<std::endl;


  delete vmesh;
  delete pmesh;
  delete tdata;
  delete udata;
  delete vdata;
  delete wdata;
  delete fem; 

}

