//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#include<iostream>
#include"MainWindow.h"
#include"GLWidget.h"
#include"RunWizard.h"
#include"Mesh.h"
	

//--------------------------------------------------------------------------------
// Main Window Constructor
//--------------------------------------------------------------------------------
MainWindow::MainWindow()
{
  openglWindow = new GLWidget();  
  
  setCentralWidget(openglWindow);

  createStatusBar();
  createActions();
  createToolBars();
  createMenus();

  setWindowTitle(tr("Finite Element Solver for Heat Equation"));
  this->setMenuBar(menuBar);
}


//--------------------------------------------------------------------------------
// Main Window Constructor
//--------------------------------------------------------------------------------
MainWindow::~MainWindow()
{
    //delete input_mesh;

    delete openglWindow;
    //delete runWindow;
    
    delete menuBar;
    delete fileMenu;
    delete editMenu;
    delete helpMenu;
    delete clearAct;
    delete openAct;
    delete runAct;
    delete exitAct;
    delete aboutAct;
    delete aboutQtAct;
}


//--------------------------------------------------------------------------------
// Main Window Methods
//--------------------------------------------------------------------------------
void MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}


void MainWindow::createActions()
{
  clearAct = new QAction(tr("&Clear"),this);
  connect(clearAct, SIGNAL(triggered()), this, SLOT(clear()));

  openAct = new QAction(tr("&Open..."), this);
  connect(openAct, SIGNAL(triggered()), this, SLOT(open()));
  
  runAct = new QAction(tr("&Run"),this);
  connect(runAct, SIGNAL(triggered()), this, SLOT(run()));
  runAct->setEnabled(false);  

  exitAct = new QAction(tr("&Exit"),this);
  connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

  aboutAct = new QAction(tr("&about"),this);
  connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

  aboutQtAct = new QAction(tr("&about Qt"),this);
  connect(aboutQtAct, SIGNAL(triggered()), this, SLOT(aboutQt()));  
}


void MainWindow::createToolBars()
{
  QToolBar *fileToolBar;

  fileToolBar = addToolBar(tr("File"));
  fileToolBar->addAction(clearAct);
  fileToolBar->addAction(openAct);
  fileToolBar->addAction(runAct);
}


void MainWindow::createMenus()
{
  menuBar = new QMenuBar;
  fileMenu = new QMenu(tr("&file"),this);
  editMenu = new QMenu(tr("&edit"),this);
  helpMenu = new QMenu(tr("&help"),this);

  fileMenu->addAction(clearAct);
  fileMenu->addAction(openAct);
  fileMenu->addAction(runAct);
  fileMenu->addAction(exitAct);
  helpMenu->addAction(aboutAct);
  helpMenu->addAction(aboutQtAct);

  menuBar->addMenu(fileMenu);
  menuBar->addMenu(editMenu);
  menuBar->addMenu(helpMenu);
}



//--------------------------------------------------------------------------------
// Main Window Slots
//--------------------------------------------------------------------------------
void MainWindow::clear()
{
    (*openglWindow).setObjectToZero();
    runAct->setEnabled(false);
}


void MainWindow::open()
{
    QString fileName = QFileDialog::getOpenFileName(this);
    if (!fileName.isEmpty()){
        const char* myChar = fileName.toStdString().c_str();
        loadFile(myChar);
        runAct->setEnabled(true);
    } 
}


void MainWindow::run()
{
  runWindow = new RunWizard(input_mesh);
  runWindow->show();
}


void MainWindow::about()
{
   QMessageBox::about(this, tr("About Application"),
            tr("<b>Heat Finite Element Model</b> is a C++ finite element solver for multi-physics "
               " problems involving heat transfer and fluid flow"));
}



void MainWindow::aboutQt()
{
  QMessageBox::about(this, tr("About Application"),
            tr("<b>Insert message about Qt here"));
}



void MainWindow::loadFile(const char *fileName)
{
  input_mesh = new Mesh(fileName);
  (*openglWindow).setObjectToMesh(input_mesh->connect,input_mesh->xpoints,input_mesh->ypoints,
                                  input_mesh->zpoints,input_mesh->Ne,input_mesh->Type);
  statusBar()->showMessage(tr("File loaded"), 2000);
}



//--------------------------------------------------------------------------------
// Main Window keyboard and mouse events
//--------------------------------------------------------------------------------
void MainWindow::keyPressEvent(QKeyEvent* e)
{
  switch(e->key())
  {
    case(Qt::Key_Left):
      (*openglWindow).setXTranslation(1);
      break;
    case(Qt::Key_Right):
      (*openglWindow).setXTranslation(-1);
      break;
    case(Qt::Key_Up):
      (*openglWindow).setYTranslation(1);
      break;
    case(Qt::Key_Down):
      (*openglWindow).setYTranslation(-1);
      break;
    case(Qt::Key_A):
      (*openglWindow).setScale(1.1);
      break;
    case(Qt::Key_B):
      (*openglWindow).setScale(0.9);
      break;
  }
};
