//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#include <iostream>
#include "math.h"
#include "Element.h"
#include "Shape.h"
#include "QuadratureRule.h"

//e.g. linear triangle in "natural" coordinates
//   2
//   (0,1)
//   |.
//   |  .
//   |    .
//   |      .
//   |        .
//   |          .
//   |            .
//   |              .
//   |                .
//   |__________________.
//  (0,0)               (1,0)
//   3                   1 
//
// transformation matrix 
// e.g    |x|   |x2, x3, x1||xi      |         
//        |y| = |y2, y3, y1||eta     |   
//        |1|   |1,  1,  1 ||1-xi-eta|
//
// jacobian matrix
// e.g.   J = |x2-x1, y2-y1| = |dx/dxi,  dy/dxi |     !!should it be J = |x2-x1, x3-x1|???
//            |x3-x1, y3-y1|   |dx/deta, dy/deta|                        |y2-x1, y3-y1|
//
// change coordinate from natural (xi,eta) to global (x1,y1)
// e.g. |x-x1| = |x2-x1, y2-y1|*|xi | = J*|xi |
//      |y-y1|   |x3-x1, y3-y1| |eta|     |eta|
//
// local shape functions
// e.g.   N = |xi, eta, 1-xi-eta|
//         
// derivatives of local shape functions
// e.g.   D = |-1, 1, 0|
//            |-1, 0, 1|


//--------------------------------------------------------------------
// element constructor 
//--------------------------------------------------------------------
Element::Element(int typ)
{
  Type = typ;
  
  switch (typ)
  {
  case 1:     //linear 1D lines
    Npe = 2; nDim = 1; nGauss = 3; 
    break;
  case 2:     //linear 2D triangles
    Npe = 3; nDim = 2; nGauss = 3;
    break;
  case 3:     //linear 2D squares
    Npe = 4; nDim = 2; nGauss = 4;
    break;
  case 4:     //linear 3D tetrahedra
    Npe = 4; nDim = 3; nGauss = 4;
    break;
  case 8:     //quadratic 1D lines
    Npe = 3; nDim = 1; nGauss = 3;
    break;
  case 9:     //quadratic 2D triangles
    Npe = 6; nDim = 2; nGauss = 3;
    break;
  case 10:    //quadratic 2D squares
    Npe = 8; nDim = 2; nGauss = 4;
    break;
  case 11:    //quadratic 3D tetrahedra
    Npe = 10; nDim = 3; nGauss = 4;
    break;
  case 15:    //0D point
    Npe = 1; nDim = 0; nGauss = 1;
    break;
  default:    //just set values to their maximum
    Npe = 10; nDim = 3; nGauss = 4;
  }

  nodes = new int[Npe];
  groups = new int[Npe];
  xpts = new double[Npe];
  ypts = new double[Npe];
  zpts = new double[Npe];
  evec = new double[Npe];
  kmat = new double*[Npe];        
  mmat = new double*[Npe];   
  axmat = new double*[Npe];   
  aymat = new double*[Npe];    
  azmat = new double*[Npe];

  for(int i=0;i<Npe;i++){
    kmat[i] = new double[Npe];
    mmat[i] = new double[Npe];
    axmat[i] = new double[Npe];
    aymat[i] = new double[Npe];
    azmat[i] = new double[Npe];
  }

  //jmat = new double*[nDim];
  //imat = new double*[nDim];
  //nmat = new double[Npe];    
  //bmat = new double*[nDim];  
}



//--------------------------------------------------------------------
// element destructor 
//--------------------------------------------------------------------
Element::~Element()
{
  delete [] nodes;
  delete [] groups;
  delete [] xpts;
  delete [] ypts;
  delete [] zpts;
  delete [] evec;

  for(int i=0;i<Npe;i++){
    delete [] kmat[i];
    delete [] mmat[i];
    delete [] axmat[i];
    delete [] aymat[i];
    delete [] azmat[i];
  }

  delete [] kmat;
  delete [] mmat;
  delete [] axmat;
  delete [] aymat;
  delete [] azmat;
}



//--------------------------------------------------------------------
// get methods for Npe, Type, nDim, and nGauss
//--------------------------------------------------------------------
int Element::get_Npe()
{
  return Npe;
}


int Element::get_Type()
{
  return Type;
}


int Element::get_nDim()
{
  return nDim;
}


int Element::get_nGauss()
{
  return nGauss;
}




//--------------------------------------------------------------------
// set jacobian and inverse jacobian matrix and return jacobian 
//--------------------------------------------------------------------
double Element::jacobian(double xi, double et, double ze)
{
  Shape *sh = ShapeFactory::NewShape(Type);

  (*sh).dshape(xi,et,ze);
  // for(int i=0;i<nDim;i++){
  //   for(int j=0;j<nDim;j++){
  //     jmat[i][j] = 0.0;
  //     for(int k=0;k<Npe;k++){
  //       if(j==0)
  //        jmat[i][j] += (*sh).dN[i][k]*xpts[k];
  //       else if(j==1)
  //        jmat[i][j] += (*sh).dN[i][k]*ypts[k];
  //       else if(j==2)
  //        jmat[i][j] += (*sh).dN[i][k]*zpts[k];
  //     }
  //   }
  // }

  // //jacobian, jacobian matrix, and inverse jacobian matrix
  // double jac = 1.0;
  // if(nDim==1){
  //   jac = jmat[0][0];
  //   imat[0][0] = 1.0/jac;
  // }
  // else if(nDim==2){
  //   jac = jmat[0][0]*jmat[1][1] - jmat[0][1]*jmat[1][0];
  //   imat[0][0] = jmat[1][1]/jac;
  //   imat[0][1] = -jmat[0][1]/jac;
  //   imat[1][0] = -jmat[1][0]/jac;
  //   imat[1][1] = jmat[0][0]/jac;
  // }
  // else if(nDim==3){
  //   jac = jmat[0][0]*(jmat[1][1]*jmat[2][2] - jmat[2][1]*jmat[1][2])
  //       - jmat[0][1]*(jmat[1][0]*jmat[2][2] - jmat[2][0]*jmat[1][2])
  //       + jmat[0][2]*(jmat[1][0]*jmat[2][1] - jmat[1][1]*jmat[2][0]);
  //   imat[0][0] = (jmat[1][1]*jmat[2][2]-jmat[2][1]*jmat[1][2])/jac;
  //   imat[0][1] = (jmat[0][2]*jmat[2][1]-jmat[0][1]*jmat[2][2])/jac;
  //   imat[0][2] = (jmat[0][1]*jmat[1][2]-jmat[0][2]*jmat[1][1])/jac;
  //   imat[1][0] = (jmat[1][2]*jmat[2][0]-jmat[1][0]*jmat[2][2])/jac;
  //   imat[1][1] = (jmat[0][0]*jmat[2][2]-jmat[0][2]*jmat[2][0])/jac;
  //   imat[1][2] = (jmat[0][2]*jmat[1][0]-jmat[0][0]*jmat[1][2])/jac;
  //   imat[2][0] = (jmat[1][0]*jmat[2][1]-jmat[1][1]*jmat[2][0])/jac;
  //   imat[2][1] = (jmat[0][1]*jmat[2][0]-jmat[0][0]*jmat[2][1])/jac;
  //   imat[2][2] = (jmat[0][0]*jmat[1][1]-jmat[0][1]*jmat[1][0])/jac;
  // }

  double jac = 1.0;
  switch(nDim)
  {
    case 1:
      jmat[0][0] = 0.0;
      for(int k=0;k<Npe;k++){
        jmat[0][0] += (*sh).dN[0][k]*xpts[k];
      }

      //jacobian, jacobian matrix, and inverse jacobian matrix
      jac = jmat[0][0];
      imat[0][0] = 1.0/jac;
      break;

    case 2:
      jmat[0][0] = 0.0;
      jmat[0][1] = 0.0;
      jmat[1][0] = 0.0;
      jmat[1][1] = 0.0;
      for(int k=0;k<Npe;k++){
        jmat[0][0] += (*sh).dN[0][k]*xpts[k];
        jmat[0][1] += (*sh).dN[0][k]*ypts[k];
        jmat[1][0] += (*sh).dN[1][k]*xpts[k];
        jmat[1][1] += (*sh).dN[1][k]*ypts[k];
      } 

      //jacobian, jacobian matrix, and inverse jacobian matrix
      jac = jmat[0][0]*jmat[1][1] - jmat[0][1]*jmat[1][0];
      imat[0][0] = jmat[1][1]/jac;
      imat[0][1] = -jmat[0][1]/jac;
      imat[1][0] = -jmat[1][0]/jac;
      imat[1][1] = jmat[0][0]/jac;
      break;

    case 3:
      jmat[0][0] = 0.0;
      jmat[0][1] = 0.0;
      jmat[0][2] = 0.0;
      jmat[1][0] = 0.0;
      jmat[1][1] = 0.0;
      jmat[1][2] = 0.0;
      jmat[2][0] = 0.0;
      jmat[2][1] = 0.0;
      jmat[2][2] = 0.0;
      for(int k=0;k<Npe;k++){
        jmat[0][0] += (*sh).dN[0][k]*xpts[k];
        jmat[0][1] += (*sh).dN[0][k]*ypts[k];
        jmat[0][2] += (*sh).dN[0][k]*zpts[k];
        jmat[1][0] += (*sh).dN[1][k]*xpts[k];
        jmat[1][1] += (*sh).dN[1][k]*ypts[k];
        jmat[1][2] += (*sh).dN[1][k]*zpts[k];
        jmat[2][0] += (*sh).dN[2][k]*xpts[k];
        jmat[2][1] += (*sh).dN[2][k]*ypts[k];
        jmat[2][2] += (*sh).dN[2][k]*zpts[k];
      }

      //jacobian, jacobian matrix, and inverse jacobian matrix
      jac = jmat[0][0]*(jmat[1][1]*jmat[2][2] - jmat[2][1]*jmat[1][2])
          - jmat[0][1]*(jmat[1][0]*jmat[2][2] - jmat[2][0]*jmat[1][2])
          + jmat[0][2]*(jmat[1][0]*jmat[2][1] - jmat[1][1]*jmat[2][0]);
      imat[0][0] = (jmat[1][1]*jmat[2][2]-jmat[2][1]*jmat[1][2])/jac;
      imat[0][1] = (jmat[0][2]*jmat[2][1]-jmat[0][1]*jmat[2][2])/jac;
      imat[0][2] = (jmat[0][1]*jmat[1][2]-jmat[0][2]*jmat[1][1])/jac;
      imat[1][0] = (jmat[1][2]*jmat[2][0]-jmat[1][0]*jmat[2][2])/jac;
      imat[1][1] = (jmat[0][0]*jmat[2][2]-jmat[0][2]*jmat[2][0])/jac;
      imat[1][2] = (jmat[0][2]*jmat[1][0]-jmat[0][0]*jmat[1][2])/jac;
      imat[2][0] = (jmat[1][0]*jmat[2][1]-jmat[1][1]*jmat[2][0])/jac;
      imat[2][1] = (jmat[0][1]*jmat[2][0]-jmat[0][0]*jmat[2][1])/jac;
      imat[2][2] = (jmat[0][0]*jmat[1][1]-jmat[0][1]*jmat[1][0])/jac;
      break;

  }

  delete sh;

  return jac;
}




//--------------------------------------------------------------------
// temperature matrix 
//--------------------------------------------------------------------
double Element::tempMatrix(double xi, double et, double ze)
{
  Shape *sh = ShapeFactory::NewShape(Type);
 
  (*sh).shape(xi,et,ze);

  //set jacobian and inverse jacobian matrix and return jacobian
  double jac = jacobian(xi,et,ze);

  //temperature matrix N
  for(int i=0;i<Npe;i++){
    nmat[i] = (*sh).N[i];
  }

  delete sh;
  return jac;
}



//--------------------------------------------------------------------
// temperature differentiation matrix
//--------------------------------------------------------------------
double Element::tempDiffMatrix(double xi, double et, double ze)
{
  Shape *sh = ShapeFactory::NewShape(Type);

  (*sh).dshape(xi,et,ze);

  //set jacobian and inverse jacobian matrix and return jacobian
  double jac = jacobian(xi,et,ze);

  //temperature differentiation matrix B
  if(nDim==1){
    for(int i=0;i<Npe;i++){
      bmat[0][i] = imat[0][0]*(*sh).dN[0][i];
    }
  }
  else if(nDim==2){
    for(int i=0;i<Npe;i++){
      bmat[0][i] = imat[0][0]*(*sh).dN[0][i]+imat[0][1]*(*sh).dN[1][i];
      bmat[1][i] = imat[1][0]*(*sh).dN[0][i]+imat[1][1]*(*sh).dN[1][i];
    }
  }
  else if(nDim==3){
    for(int i=0;i<Npe;i++){
      bmat[0][i] = imat[0][0]*(*sh).dN[0][i]+imat[0][1]*(*sh).dN[1][i]+imat[0][2]*(*sh).dN[2][i];
      bmat[1][i] = imat[1][0]*(*sh).dN[0][i]+imat[1][1]*(*sh).dN[1][i]+imat[1][2]*(*sh).dN[2][i];
      bmat[2][i] = imat[2][0]*(*sh).dN[0][i]+imat[2][1]*(*sh).dN[1][i]+imat[2][2]*(*sh).dN[2][i];
    }
  }

  delete sh;
  return jac;
}




//--------------------------------------------------------------------
// Stiffness matrix 
//--------------------------------------------------------------------
void Element::stiffnessMatrix()
{
  for(int i=0;i<Npe;i++){
    for(int j=0;j<Npe;j++){
      kmat[i][j] = 0.0;
    }
  }

  double u=1.0, v=1.0, w=1.0;

  QuadratureRule g(nGauss,nDim,Type);
  for(int ip=0;ip<g.nIntPoints;ip++)
  {
    if(nDim>0){u=g.xii[ip];}
    if(nDim>1){v=g.eti[ip];}
    if(nDim>2){w=g.zei[ip];}

    double det = tempDiffMatrix(u,v,w);
    double dv = det*g.wi[ip];

    for(int i=0;i<Npe;i++){
      for(int j=0;j<Npe;j++){
        double s = 0.0;
        for(int k=0;k<nDim;k++)
          s += bmat[k][i]*bmat[k][j];
        kmat[i][j] += dv*s;
      }
    }
  }
}




//--------------------------------------------------------------------
// Mass matrix 
//--------------------------------------------------------------------
void Element::massMatrix()
{
  for(int i=0;i<Npe;i++){
    for(int j=0;j<Npe;j++){
      mmat[i][j] = 0.0;
    }
  }

  double u=1.0, v=1.0, w=1.0;

  QuadratureRule g(nGauss,nDim,Type);
  for(int ip=0;ip<g.nIntPoints;ip++)
  {
    if(nDim>0){u=g.xii[ip];}
    if(nDim>1){v=g.eti[ip];}
    if(nDim>2){w=g.zei[ip];}

    double det = tempMatrix(u,v,w);
    double dv = det*g.wi[ip];

    for(int i=0;i<Npe;i++){
      for(int j=0;j<Npe;j++){
        mmat[i][j] += dv*nmat[i]*nmat[j];
      }
    }
  }
}



//--------------------------------------------------------------------
// advection x matrix 
//--------------------------------------------------------------------
void Element::advecXMatrix()
{
  for(int i=0;i<Npe;i++){
    for(int j=0;j<Npe;j++){
      axmat[i][j] = 0.0;
    }
  }

  double u=1.0, v=1.0, w=1.0;

  QuadratureRule g(nGauss,nDim,Type);
  for(int ip=0;ip<g.nIntPoints;ip++)
  {
    if(nDim>0){u=g.xii[ip];}
    if(nDim>1){v=g.eti[ip];}
    if(nDim>2){w=g.zei[ip];}

    double det = 0;
    det = tempMatrix(u,v,w);
    det = tempDiffMatrix(u,v,w);
    double dv = det*g.wi[ip];

    for(int i=0;i<Npe;i++){
      for(int j=0;j<Npe;j++){
        axmat[i][j] += dv*nmat[i]*bmat[0][j];
      }
    }
  }
}




//--------------------------------------------------------------------
// advection y matrix 
//--------------------------------------------------------------------
void Element::advecYMatrix()
{
  for(int i=0;i<Npe;i++){
    for(int j=0;j<Npe;j++){
      aymat[i][j] = 0.0;
    }
  }

  double u=1.0, v=1.0, w=1.0;

  QuadratureRule g(nGauss,nDim,Type);
  for(int ip=0;ip<g.nIntPoints;ip++)
  {
    if(nDim>0){u=g.xii[ip];}
    if(nDim>1){v=g.eti[ip];}
    if(nDim>2){w=g.zei[ip];}

    double det = 0;
    det = tempMatrix(u,v,w);
    det = tempDiffMatrix(u,v,w);
    double dv = det*g.wi[ip];

    for(int i=0;i<Npe;i++){
      for(int j=0;j<Npe;j++){
        aymat[i][j] += dv*nmat[i]*bmat[1][j];
      }
    }
  }
}




//--------------------------------------------------------------------
// advection z matrix 
//--------------------------------------------------------------------
void Element::advecZMatrix()
{
  for(int i=0;i<Npe;i++){
    for(int j=0;j<Npe;j++){
      azmat[i][j] = 0.0;
    }
  }

  double u=1.0, v=1.0, w=1.0;

  QuadratureRule g(nGauss,nDim,Type);
  for(int ip=0;ip<g.nIntPoints;ip++)
  {
    if(nDim>0){u=g.xii[ip];}
    if(nDim>1){v=g.eti[ip];}
    if(nDim>2){w=g.zei[ip];}

    double det = 0;
    det = tempDiffMatrix(u,v,w);
    det = tempMatrix(u,v,w);
    double dv = det*g.wi[ip];

    for(int i=0;i<Npe;i++){
      for(int j=0;j<Npe;j++){
        azmat[i][j] += dv*nmat[i]*bmat[2][j];
      }
    }
  }
}





//--------------------------------------------------------------------
// Element vector
//--------------------------------------------------------------------
void Element::elementVector()
{
  for(int i=0;i<Npe;i++){evec[i] = 0.0;}

  double u=1.0, v=1.0, w=1.0;

  QuadratureRule g(nGauss,nDim,Type);
  Shape *sh = ShapeFactory::NewShape(Type);
  for(int ip=0;ip<g.nIntPoints;ip++)
  {
    if(nDim>0){u=g.xii[ip];}
    if(nDim>1){v=g.eti[ip];}
    if(nDim>2){w=g.zei[ip];}

    double det = jacobian(u,v,w);    
    double dv = det*g.wi[ip]; 

    (*sh).shape(u,v,w);
    for(int i=0;i<Npe;i++){
      double s = 0.0;
      s = (*sh).N[i]; 
      evec[i] += dv*s;
    }
  } 
  delete sh;
}



//--------------------------------------------------------------------
// Element vector
//--------------------------------------------------------------------
//void Element::elementVector()
//{
//  for(int i=0;i<Npe;i++){evec[i] = 0.0;}
//
//  double v=1.0, w=1.0;
//
//  QuadratureRule g(nGauss,nDim,Type);
//  Shape *sh = ShapeFactory::NewShape(Type);
//  for(int ip=0;ip<g.nIntPoints;ip++)
//  {
//    double nx = 0; //
//    double ny = 0; // components of normal vector
//    double nz = 0; //
//
//    if(nDim>1){v=g.eti[ip];}
//    //if(nDim>2){w=g.zei[ip];}
//
//    (*sh).shape(g.xii[ip],v,w);
//    (*sh).dshape(g.xii[ip],v,w);
//
//    double dxdxi=0, dydxi=0, dzdxi=0;
//    double dxdet=0, dydet=0, dzdet=0;
//    for(int j=0;j<Npe;j++){
//      dxdxi += (*sh).dN[0][j]*xpts[j];
//      dydxi += (*sh).dN[0][j]*ypts[j];
//      dzdxi += (*sh).dN[0][j]*zpts[j];
//      if(nDim==1){dzdet = 1;}
//      if(nDim==2){
//        dxdet += (*sh).dN[1][j]*xpts[j];
//        dydet += (*sh).dN[1][j]*ypts[j];
//        dzdet += (*sh).dN[1][j]*zpts[j];
//      }
//    }
//
//    nx = dydxi*dzdet-dzdxi*dydet;
//    ny = dzdxi*dxdet-dxdxi*dzdet;
//    nz = dxdxi*dydet-dydxi*dxdet;
//
//    double ds = sqrt(nx*nx + ny*ny + nz*nz);
//
//    for(int i=0;i<Npe;i++){
//      evec[i] += (*sh).N[i]*nx*g.wi[ip];
//      evec[i] += (*sh).N[i]*ny*g.wi[ip];
//      if(nDim==2){
//        evec[i] += (*sh).N[i]*nz*g.wi[ip];
//      }
//    }
//  }
//  delete sh;
//}



//--------------------------------------------------------------------
// find value on element at point (x,y,z)
//--------------------------------------------------------------------
// jacobian matrix
// e.g.   J = |x2-x1, x3-x1|
//            |y2-y1, y3-y1|
double Element::value(double x, double y, double z, double v1, double v2, double v3)
{
  double a = xpts[1] - xpts[0];
  double b = xpts[2] - xpts[0];
  double c = ypts[1] - ypts[0];
  double d = ypts[2] - ypts[0];

  double jac = a*d - b*c;
  double xi = (d*(x-xpts[0]) - b*(y-ypts[0]))/jac; 
  double et = (a*(y-ypts[0]) - c*(x-xpts[0]))/jac;
  double ze = 1.0;

  Shape *sh = ShapeFactory::NewShape(Type);

  (*sh).shape(xi,et,ze);

  //double val = v1*(*sh).N[0] + v2*(*sh).N[1] + v3*(*sh).N[2];
  double val = v1*(*sh).N[2] + v2*(*sh).N[0] + v3*(*sh).N[1];
  //double val = v2*(*sh).N[2] + v3*(*sh).N[0] + v1*(*sh).N[1];

  delete sh;
  return val;
}
