#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<string>
#include<time.h>
#include"IterSolvers.h"
using namespace std;

//ifstream fin("../tests/t2dal_e.mtx");          // works  
//int N = 4257; 
//int Nr = 4258;

//ifstream fin("../tests/sevenpoint1.txt");      // works 
//int N = 580;
//int Nr = 101;

//ifstream fin("../tests/sevenpoint2.txt");      // works 
//int N = 68800;
//int Nr = 10001;

//ifstream fin("../tests/sevenpoint3.txt");      // works 
//int N = 1744000;
//int Nr = 250001;

//ifstream fin("../tests/sevenpoint4.txt");      // works 
//int N = 3928500;
//int Nr = 562501;

//ifstream fin("../tests/mesh1em6.mtx");         // works 
//int N = 177;
//int Nr = 49;

//ifstream fin("../tests/mesh2em5.mtx");         // works 
//int N = 1162;
//int Nr = 307;

//ifstream fin("../tests/mesh3em5.mtx");         // works
//int N = 1089;
//int Nr = 290;

//ifstream fin("../tests/fv1.mtx");              // works
//int N = 47434; 
//int Nr = 9605;

//ifstream fin("../tests/fv2.mtx");              // works
//int N = 48413; 
//int Nr = 9802;

//ifstream fin("../tests/fv3.mtx");              // works
//int N = 48413; 
//int Nr = 9802;

//ifstream fin("../tests/crystm02.mtx");         // works  
//int N = 168435; 
//int Nr = 13966;

//ifstream fin("../tests/shallow_water1.mtx");   // works
//int N = 204800; 
//int Nr = 81921;

//ifstream fin("../tests/shallow_water2.mtx");   // works
//int N = 204800; 
//int Nr = 81921;

//ifstream fin("../tests/thermal1.mtx");         // works
//int N = 328556; 
//int Nr = 82655;

//ifstream fin("../tests/thermal2.mtx");         // works
//int N = 4904179;
//int Nr = 1228046;

//ifstream fin("../tests/thermomech_TC.mtx");    // works
//int N = 406858; 
//int Nr = 102159;
  
//ifstream fin("../tests/thermomech_TK.mtx");    // fails 
//int N = 406858; 
//int Nr = 102159;

//ifstream fin("../tests/thermomech_dM.mtx");    // works
//int N = 813716; 
//int Nr = 204317;

//ifstream fin("../tests/ted_B.mtx");            // works
//int N = 77592; 
//int Nr = 10606;

//ifstream fin("../tests/G2_circuit.mtx");       // works     
//int N = 438388; 
//int Nr = 150103;

//ifstream fin("../tests/G3_circuit.mtx");       // works    
//int N = 4623152; 
//int Nr = 1585479;

//ifstream fin("../tests/parabolic_fem.mtx");    // works   
//int N = 2100225; 
//int Nr = 525826;

//ifstream fin("../tests/apache2.mtx");    // works    ///////////////////////  
//int N = 2766523; 
//int Nr = 715177;

//ifstream fin("../tests/Pres_Poisson.mtx");  // slow convergence 
//int N = 365313; 
//int Nr = 14823;

//ifstream fin("../tests/cfd2.mtx");  // slow convergence
//int N = 1605669;
//int Nr = 123441;








//ifstream fin("../tests/nos2.mtx");        // appears to work  *    slow convegence
//int N = 2547;
//int Nr = 958;

//ifstream fin("../tests/nos3.mtx");        // appears to work  *    slow convegence
//int N = 8402;
//int Nr = 961;

//ifstream fin("../tests/nos5.mtx");        // appears to work  *    slow convegence
//int N = 2820;
//int Nr = 469;

//ifstream fin("../tests/nos6.mtx");        // appears to work  *    slow convegence
//int N = 1965;
//int Nr = 676;

//ifstream fin("../tests/nos7.mtx");        // appears to work  *    slow convegence
//int N = 2673;
//int Nr = 730;

//ifstream fin("../tests/bcsstk01.mtx");    // works (pcgamg)
//int N = 224; 
//int Nr = 49;

//ifstream fin("../tests/bcsstk10.mtx");    // works (pcgamg) 
//int N = 11578; 
//int Nr = 1087; 

//ifstream fin("../tests/bcsstk19.mtx");    // appears to work (g-s fails cond~10^11)
//int N = 3835; 
//int Nr = 818;

//ifstream fin("../tests/bcsstk28.mtx");    // works (pcgamg)  
//int N = 111717; 
//int Nr = 4411;

//ifstream fin("../tests/bcsstk36.mtx");    // appears to work 
//int N = 583096; 
//int Nr = 23053;

//ifstream fin("../tests/bcsstk38.mtx");    // appears to work (returns NaN in matlab)
//int N = 181746; 
//int Nr = 8033;

//ifstream fin("../tests/plbuckle.mtx");    // fails for pcgamg
//int N = 15963; 
//int Nr = 1283;

//ifstream fin("../tests/ex5.mtx");     //fails to converge (g-s fails cond~10^7)  *
//int N = 153;
//int Nr = 28;


int main()
{
  int currentLine = 0;
  int index = 0;
  int *rows = new int[N];
  int *columns = new int[N];
  double *values = new double[N];
  for(int i=0;i<N;i++){
    rows[i]=0;
    columns[i]=0;
    values[i]=0.0;
  }
  string percent("%");
  string space(" ");
  string token;

  //scan through file
  while(!fin.eof())
  {
    string line;
    getline(fin,line);

    if(index==N){break;}

    // parse the line
    if(line.substr(0,1).compare(percent)!=0){
      if(currentLine>0){
        token = line.substr(0,line.find(space));
        columns[index] = atoi(token.c_str())-1;
        line.erase(0,line.find(space)+space.length());
        token = line.substr(0,line.find(space));
        rows[index] = atoi(token.c_str())-1;
        line.erase(0,line.find(space)+space.length());
        token = line.substr(0,line.find(space));
        values[index] = strtod(token.c_str(),NULL);
        index++;
      }
      currentLine++;
    }
  }

  int m=rows[0];
  for(int i=0;i<N;i++){
    if(rows[i]!=m){
      m=rows[i];
      if(m!=columns[i]){
        cout<<"WARNING: Matrix does not contain a diagonal entry in every row/column"<<endl;
        return 0;
      }
    }
  }

  int Nt = 2*N-(Nr-1);  //number of entries in total sparse matrix
  int *row_total = new int[Nt];
  int *col_total = new int[Nt];
  double *val_total = new double[Nt];

  m = rows[0];
  index = 0;
  int i=0;
  while(i<N){
    if(rows[i]!=m){
      for(int j=0;j<i;j++){
        if(columns[j]==rows[i]){
          row_total[index] = rows[i];  
          col_total[index] = rows[j];
          val_total[index] = values[j];   
          index++;    
        }
      }
      m=rows[i];
    }
    else{
      row_total[index] = rows[i];
      col_total[index] = columns[i];
      val_total[index] = values[i];
      index++;
      i++;
    }
  }

  int *row_ptr = new int[Nr];
  int j=0;
  int count = 0;
  row_ptr[0] = 0;
  row_ptr[Nr-1] = Nt;
  for(int i=1;i<Nt;i++){
    if(row_total[i-1]==row_total[i]){
      count++;
    }
    else{
      count++;
      j++;
      row_ptr[j] = count;
    }    
  } 

  //for(int i=0;i<Nr;i++){
  //  cout<<row_ptr[i]<<endl;
  //}



  //begin test
  //cout<<"Begin test"<<endl;
  //srand(0);
  //double *x = new double[Nr-1];
  //double *b = new double[Nr-1];
  //for(int i=0;i<Nr-1;i++){
  //  x[i] = 2*(rand()%Nr)/(double)Nr-1;
  //  b[i] = 1.0;
  //  //if(i<(Nr-1)/2){b[i] = 10.0;}
  //  //else{b[i] = -5.0;}
  //}
  //int niter = IterSolvers::pcg(row_ptr,col_total,val_total,x,b,Nr-1,10e-8,100000);
  //cout<<""<<endl;
  //for(int i=0;i<10;i++){  //Nr-1
  //  cout<<x[i]<<endl;
  //}

  //cout<<"niter: "<<niter<<endl;


  //output b array
  //ofstream myfile("exact.txt");
  //if(myfile.is_open()){
  //  for(int i=0;i<Nr-1;i++){
  //    myfile<<x[i]<<endl;
  //  }
  //}
  //myfile.close();


  delete[] rows;
  delete[] columns;
  delete[] values;
  delete[] row_total;
  delete[] col_total;
  delete[] val_total;
  delete[] row_ptr;
  //delete[] x;
  //delete[] b;






























  //test 1  works
  int aar[9] = {0,6,11,16,23,31,37,43,50};
  int aac[50] = {0,2,3,4,6,7,1,3,4,5,7,0,2,3,4,6,0,1,2,3,4,5,7,0,1,2,3,4,5,6,7,1,3,4,5,6,7,0,2,
                 4,5,6,7,0,1,3,4,5,6,7};
  double aav[50] = {2,1,-0.75,0.1,0.2,0.6,4,0.2,0.3,0.4,0.5,1,3,1,-1,1.5,-0.75,0.2,1,4,0.6,0.8,
                    0.9,0.1,0.3,-1,0.6,2,-0.9,1,-1.1,0.4,0.8,-0.9,4,1.999,1,0.2,1.5,1,2,6,
                   -2.999,0.6,0.5,0.9,-1.1,1,-3,8};
  double bb[50] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                   1,1,1,1,1,1,1,1,1,1,1,1};
  int niter = IterSolvers::pcg(row_ptr,col_total,val_total,x,b,Nr-1,10,1000,10e-8);
  for(int i=0;i<8;i++){ 
    cout<<bb[i]<<endl;
  }

  //test 2 works
  //int aar[7] = {0,6,12,18,24,30,36};
  //int aac[36] = {0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5};
  //double aav[36] = {2,0.5310,0.4187,-0.7620,0.5025,0.0944,0.5310,2,0.5094,-0.0033,-0.4898,-0.7228,
  //                  0.4187,0.5094,2,0.9195,0.0119,-0.7014,-0.7620,-0.033,0.9195,2,0.3982,-0.4850,
  //                  0.5025,-0.4898,0.0119,0.3982,2,0.6814,0.0944,-0.7228,-0.7014,-0.4850,0.6814,2};
  //double bb[36] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
  //amg(aar,aac,aav,bb,6,1,0.5);
  //for(int i=0;i<8;i++){ 
  //  cout<<bb[i]<<endl;
  //}

  //test 3 works
  //int aar[6] = {0,2,5,8,11,13};
  //int aac[13] = {0,1,0,1,2,1,2,3,2,3,4,3,4};
  //double aav[13] = {-4,1,1,-4,1,1,-4,1,1,-4,1,1,-4};
  //double bb[5] = {1,1,1,1,1};
  //amg(aar,aac,aav,bb,5,1,0.5);
  //for(int i=0;i<5;i++){ 
  //  cout<<bb[i]<<endl;
  //}

  //test 4 works
  //int aar[6] = {0,4,9,13,18,21};
  //int aac[21] = {0,1,2,3,0,1,2,3,4,0,1,2,3,0,1,2,3,4,1,3,4};
  //double aav[21] = {2,-0.1,0.4,0.21,-0.1,3,0.51,0.2,0.7,0.4,0.51,2,1,0.21,0.2,1,4,0.52,0.7,0.52,2};
  //double bb[5] = {1,1,1,1,1};
  //amg(aar,aac,aav,bb,5,1,0.5);
  //for(int i=0;i<5;i++){ 
  //  cout<<bb[i]<<endl;
  //}

  //test 5 works
  //int aar[9] = {0,6,11,15,19,26,31,37,44};
  //int aac[44] = {0,1,3,4,6,7,0,1,2,4,7,1,2,5,7,0,3,4,6,0,1,3,4,5,6,7,2,4,5,6,7,0,3,4,5,6,7,0,1,2,4,5,6,7};
  //double aav[44] = {2,0.1,-0.56,-0.7,0.025,0.33,0.1,-4.2,0.8147,-0.746,0.82,0.8147,3.1,0.26,-0.8,-0.56,2.3,
  //                -0.44,0.09,-0.7,-0.746,-0.44,6,0.91,0.92,-0.68,0.26,0.91,7,0.94,0.91,0.025,0.09,0.92,
  //                 0.94,1.2,-0.02,0.33,0.82,-0.8,-0.68,0.91,-0.02,4};
  //double bb[8] = {1,1,1,1,1,1,1,1};
  //amg(aar,aac,aav,bb,8,0.5);
  //for(int i=0;i<5;i++){ 
  //  cout<<bb[i]<<endl;
  //}


  //test 6 works
  //int aar[11] = {0,1,2,3,4,5,6,7,8,9,10};
  //int aac[10] = {0,1,2,3,4,5,6,7,8,9};
  //double aav[10] = {2.3,1.1,4.2,3.9,6.6,3.7,1.5,2.2,4.3,5.5};
  //double bb[10] = {1,1,1,1,1,1,1,1,1,1};
  //amg(aar,aac,aav,bb,10,0.5);
  //for(int i=0;i<5;i++){
  //  cout<<bb[i]<<endl;
  //} 

  //test 7
  //int aar[13] = {0,1,12,20,28,32,40,49,57,66,75,83,90};
  //int aac[90] = {0,1,2,3,4,5,6,7,8,9,10,11,1,2,4,6,7,9,10,11,1,3,4,5,6,8,9,10,1,2,3,4,
  //               1,3,5,6,7,8,10,11,1,2,3,5,6,7,8,9,11,1,2,5,6,7,8,9,10,1,3,5,6,7,8,9,10,11,
  //               1,2,3,6,7,8,9,10,11,1,2,3,5,7,8,9,10,1,2,5,6,8,9,11};
  //double aav[90] = {4.2,2.2,0.629,0.811,-0.746,0.826,0.264,-0.804,-0.443,0.093,0.915,0.929,
  //                  0.629,6.4,0.631,-0.591,0.613,0.651,0.712,0.732,
  //                  0.811,3.3,-0.09,0.015,-0.111,-0.189,0.073,0.212,-0.746,0.631,-0.09,5.5,
  //                  0.826,0.015,7.1,1.12,0.913,0.211,0.437,0.516,
  //                  0.264,-0.591,-0.111,1.12,4.3,0.732,-0.324,0.159,-0.632,
  //                  -0.804,0.613,0.913,0.732,2.7,0.323,-0.217,0.409,
  //                  -0.443,-0.189,0.211,-0.324,0.323,2.9,0.737,-0.412,0.831,
  //                  0.093,0.651,0.073,0.159,-0.217,0.737,4.4,0.118,-0.516,
  //                  0.915,0.712,0.212,0.437,0.409,-0.412,0.118,5.1,
  //                  0.929,0.732,0.516,-0.632,0.831,-0.516,7.3};
  //double bb[12] = {1,1,1,1,1,1,1,1,1,1,1,1};
  //amg(aar,aac,aav,bb,12,0.5);
  //for(int i=0;i<5;i++){
  //  cout<<bb[i]<<endl;
  //}

  //test 6 works
  //int aar[8] = {0,1,2,3,10,11,12,13};
  //int aac[13] = {0,1,2,0,1,2,3,4,5,6,4,5,6};
  //double aav[13] = {1,1,1,-0.99999995,-0.99999995,3,4,3,-0.99999995,-0.99999995,1,1,1};
  //double bb[7] = {1,1,1,1,1,1,1};
  //amg(aar,aac,aav,bb,7,1,0.5);
  //for(int i=0;i<5;i++){
  //  cout<<bb[i]<<endl;
  //} 


