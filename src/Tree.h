//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef TREE_H
#define TREE_H

class Node
{
  public:
    int node;     //node number
    int group;    //group number
    int type;     //boundary condition type
                  // 0: no condition assigned yet
                  // 1: dirichlet
                  // 2: flux
    double value; //boundary condition value
    Node *left;
    Node *right;

    Node();
    Node(int n);
    Node(int n, int g);
    Node(int n, int g, int t);
    Node(int n, int g, int t, double v);
};



class Tree
{
  private:
    int nNodes;
    int index;
  	Node *root;


  public:
  	Tree();
  	~Tree();

    void insert(int n);
    void insert(int n, int g);
    void insert(int n, int g, int t);
    void insert(int n, int g, int t, double v);
    void toNodeArray(Node *array);
    void deleteTree();

    int getNumNodes();

  private:
    void insert(int n, Node *leaf);
    void insert(int n, int g, Node *leaf);
    void insert(int n, int g, int t, Node *leaf);
    void insert(int n, int g, int t, double v, Node *leaf);
    void toNodeArray(Node *array, Node *leaf);
    void deleteTree(Node *leaf);
};

#endif