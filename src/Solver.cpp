//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#include "Element.h"
#include "Solver.h"
#include "Mesh.h"
#include "FeModel.h"
#include "BoundaryData.h"
#include "IterSolvers.h"
#include "PostProcessing.h"

 

//********************************************************************************
//
//  Solvers for, poisson, heat and fluid equations
//
//********************************************************************************

  


//----------------------------------------------------------------------------
// Constructor for Solver
//----------------------------------------------------------------------------
Solver::Solver(Mesh *mesh1, FeModel *fem) : mesh1(mesh1), fem(fem)
{
  MAX_ITER = 100000;
  EPSILON = 1.0e-64;

  link1 = new int[10];        //link array not actually used. initialize for deletion later
  link2 = new int[10];        //link array not actually used. initialize for deletion later

  for(int i=0;i<10;i++){
    link1[i] = -1;
    link2[i] = -1;
  }     

  //determine upper bound on max number on non-zeros in rows of global matrices
  int *tmp = new int[mesh1->get_N()];
  for(int i=0;i<mesh1->get_N();i++){tmp[i] = 0;}
  for(int i=0;i<mesh1->get_Ne();i++){
    for(int j=0;j<mesh1->get_Npe();j++){
      tmp[mesh1->get_connect(i,j)-1]++;
    }
  }
  for(int i=0;i<mesh1->get_N();i++){tmp[i] = tmp[i]*mesh1->get_Npe();}
  MAX_NNZ = 0;
  for(int i=0;i<mesh1->get_N();i++){
    if(tmp[i]>MAX_NNZ){
      MAX_NNZ = tmp[i];
    }
  }
  delete [] tmp;
}



//----------------------------------------------------------------------------
// Constructor for Solver
//----------------------------------------------------------------------------
Solver::Solver(Mesh *mesh1, Mesh *mesh2, FeModel *fem) : mesh1(mesh1), mesh2(mesh2), fem(fem)
{
  MAX_ITER = 100000;
  EPSILON = 1.0e-64;

  link1 = new int[mesh1->get_N()];
  link2 = new int[mesh2->get_N()];       

  for(int i=0;i<mesh1->get_N();i++){link1[i] = -1;}
  for(int i=0;i<mesh2->get_N();i++){link2[i] = -1;}

  long count = 0;

  //fill in link1
  for(int i=0;i<mesh1->get_Ne();i++){
    for(int j=0;j<mesh1->get_Npe();j++){
      if(link1[mesh1->get_connect(i,j)-1]==-1){
        for(int k=0;k<mesh2->get_N();k++){
          count = count + 2;
          if(mesh2->get_xpoint(k)==mesh1->get_xpoint(mesh1->get_connect(i,j)-1) &&
             mesh2->get_ypoint(k)==mesh1->get_ypoint(mesh1->get_connect(i,j)-1) &&
             mesh2->get_zpoint(k)==mesh1->get_zpoint(mesh1->get_connect(i,j)-1)){
                link1[mesh1->get_connect(i,j)-1] = k+1;
                break;
          }
        }
      }
    }
  }

  //fill in link2
  for(int i=0;i<mesh2->get_Ne();i++){
    for(int j=0;j<mesh2->get_Npe();j++){
      if(link2[mesh2->get_connect(i,j)-1]==-1){
        for(int k=0;k<mesh1->get_N();k++){
          count = count + 2;
          if(mesh1->get_xpoint(k)==mesh2->get_xpoint(mesh2->get_connect(i,j)-1) &&
             mesh1->get_ypoint(k)==mesh2->get_ypoint(mesh2->get_connect(i,j)-1) && 
             mesh1->get_zpoint(k)==mesh2->get_zpoint(mesh2->get_connect(i,j)-1)){
                link2[mesh2->get_connect(i,j)-1] = k+1;
                break;
          }
        }
      }
    }
  }

  //determine upper bound on max number on non-zeros in rows of global matrices
  int *tmp = new int[mesh2->get_N()];
  for(int i=0;i<mesh2->get_N();i++){tmp[i] = 0;}
  for(int i=0;i<mesh2->get_Ne();i++){
    for(int j=0;j<mesh2->get_Npe();j++){
      tmp[mesh2->get_connect(i,j)-1]++;
    }
  }
  for(int i=0;i<mesh2->get_N();i++){tmp[i] = tmp[i]*mesh2->get_Npe();}
  MAX_NNZ = 0;
  for(int i=0;i<mesh2->get_N();i++){
    if(tmp[i]>MAX_NNZ){
      MAX_NNZ = tmp[i];
    }
  }
  delete [] tmp;

  std::cout<<"MAX_NNZ: "<<MAX_NNZ<<std::endl;
}



//----------------------------------------------------------------------------
// destructor for Solver
//----------------------------------------------------------------------------
Solver::~Solver()
{
  delete [] link1;
  delete [] link2;
}


















//********************************************************************************
//
//  Poisson equation solver
//
//  d^2u/dx^2 + d^2u/dy^2 + d^2u/dz^2 = f
//
//********************************************************************************


//----------------------------------------------------------------------------
// Constructor for poisson equation solver
//----------------------------------------------------------------------------
SolverPoisson::SolverPoisson(Mesh *mesh1, FeModel *fem) : Solver(mesh1,fem)
{
  filename = "poisson.msh";
  neq1 = mesh1->get_N();

  std::cout<<"MAX_NNZ: "<<MAX_NNZ<<std::endl;

  row1 = new int[neq1+1];
  col1 = new int[neq1*MAX_NNZ];
  A1 = new double[neq1*MAX_NNZ];

  u = new double[neq1];
  b = new double[neq1];

  for(int i=0;i<neq1+1;i++){row1[i] = 0;}
  for(int i=0;i<neq1*MAX_NNZ;i++){
    col1[i] = -1;
    A1[i] = 0.0;
  }
  for(int i=0;i<neq1;i++){
    u[i] = 0.0;
    b[i] = 1.0;
  }
}



//----------------------------------------------------------------------------
// assemble global poisson system
//----------------------------------------------------------------------------
void SolverPoisson::assemblePoisson(int *row, int *col, double *A, double *b, BoundaryData *data, Mesh *mesh)
{
  int n = mesh->get_N();
  int r=0,c=0;
  double v=0;

  for(int i=0;i<n+1;i++){row[i] = 0;}
  for(int i=0;i<n*MAX_NNZ;i++){
    col[i] = -1;
    A[i] = 0.0;
  }

  double *tmp = new double[n];
  for(int i=0;i<n;i++){tmp[i] = 0.0;}

  //loop through all elements
  for(int k=0;k<mesh->get_Ne();k++){
    Element e(mesh->get_Type());
    for(int l=0;l<e.get_Npe();l++){
      e.nodes[l] = mesh->get_connect(k,l);
      e.xpts[l] = mesh->get_xpoint(e.nodes[l]-1);
      e.ypts[l] = mesh->get_ypoint(e.nodes[l]-1);
      e.zpts[l] = mesh->get_zpoint(e.nodes[l]-1);
    }

    //determine which nodes in element are on a dirichlet boundary
    bool *bcFlags = new bool[e.get_Npe()];
    double *bcVals = new double[e.get_Npe()];
    for(int i=0;i<e.get_Npe();i++){
      bcFlags[i] = data->isDirichlet(e.nodes[i]-1);
      bcVals[i] = data->getDirichletValue(e.nodes[i]-1);
    }

    e.elementVector();
    e.stiffnessMatrix();

    //for(int i=0;i<e.Npe;i++){
    //  for(int j=0;j<e.Npe;j++){
    //    std::cout<<e.emat[i][j]<<"  ";   
    //  } 
    //  std::cout<<" "<<std::endl;
    //}
    //std::cout<<" "<<std::endl;

    //update col, A arrays
    for(int i=0;i<e.get_Npe();i++){
      r = e.nodes[i] - 1;
      tmp[r] += b[r]*e.evec[i];
      for(int j=0;j<e.get_Npe();j++){
        c = e.nodes[j] - 1;
        v = e.kmat[i][j];
        
        if(bcFlags[i]==true){          //
          tmp[r] = bcVals[i];          //
          v = 0.0;                     //
        }                              // enforce dirichlet 
        else if(bcFlags[j]==true){     // boundary conditions
          tmp[r] -= v*bcVals[j];       //
          v = 0.0;                     //
        }                              //

        for(int p=MAX_NNZ*r;p<MAX_NNZ*r+MAX_NNZ;p++){
          if(col[p]==-1){
            col[p] = c;
            A[p] = v;
            break;
          }
          else if(col[p]==c){
            A[p] += v;
            break;
          }
        }
      }
    }

    delete[] bcFlags;
    delete[] bcVals;
  }

  //update row array
  int jj = 0;
  for(int i=1;i<n+1;i++){
    for(int j=0;j<MAX_NNZ;j++){
      if(col[i*MAX_NNZ-MAX_NNZ+j]==-1){jj=j; break;}
    }
    row[i] = row[i-1] + jj;
  }

  //sort (insertion) col and A arrays
  for(int p=0;p<n;p++){
    int start = p*MAX_NNZ;
    for(int i=1;i<row[p+1]-row[p];i++){
      int entry = col[i+start];
      double entryA = A[i+start];
      int j = 0;
      for(j=i;j>0 && entry<col[j-1+start];j--){
        col[j+start] = col[j-1+start];
        A[j+start] = A[j-1+start];
      }
      col[j+start] = entry;
      A[j+start] = entryA;
    }
  }

  //compress col and A arrays
  int index=0;
  for(int i=0;i<n*MAX_NNZ;i++){
    if(col[i]==-1)
      continue;
    col[index] = col[i];
    A[index] = A[i];
    index++;
  }

  //set unused parts of arrays to 0 or -1
  for(int i=index;i<n*MAX_NNZ;i++){
    col[i] = -1;
    A[i] = 0.0;
  }

  for(int i=0;i<n;i++){
    for(int j=row[i];j<row[i+1];j++){
      if(col[j]==i && A[j]==0.0){
        A[j] = 1.0;
      }
    }
  }

  for(int i=0;i<n;i++){b[i] = tmp[i];}

  delete [] tmp;
}



//----------------------------------------------------------------------------
// return pointer to poisson variable array 
//----------------------------------------------------------------------------
double* SolverPoisson::getPoissonVariable()
{
  return u;
}



//-------------------------------------------------------------------
// Solve poisson equation
//-------------------------------------------------------------------
void SolverPoisson::solve()
{
  //assemble global stiffness
  assemblePoisson(row1,col1,A1,b,fem->tdata,mesh1);

  //solve
  IterSolvers::pcg(row1,col1,A1,u,b,neq1,MAX_ITER,EPSILON);

  //write solution vector to output file
  PostProcessing::write_results_gmsh_msh(u,filename,mesh1);
}



//----------------------------------------------------------------------------
// destructor for poisson solver
//----------------------------------------------------------------------------
SolverPoisson::~SolverPoisson()
{
  delete [] row1;
  delete [] col1;
  delete [] A1;

  delete [] u;
  delete [] b;
}

















//********************************************************************************
//
//  Heat equation solver
//
//  c*rho*dT/dt = k*(d^2T/dx^2 + d^2T/dy^2 + d^2T/dz^2) 
//
//********************************************************************************



//----------------------------------------------------------------------------
// Constructor for heat equation solver
//----------------------------------------------------------------------------
SolverHeat::SolverHeat(Mesh *mesh1, FeModel *fem) : Solver(mesh1,fem)
{
  filename = "heat.msh";
  neq1 = mesh1->get_N();

  row1 = new int[neq1+1];
  col1 = new int[neq1*MAX_NNZ];
  A1 = new double[neq1*MAX_NNZ];

  temp = new double[neq1];
  b = new double[neq1];

  for(int i=0;i<neq1+1;i++){row1[i] = 0;}
  for(int i=0;i<neq1*MAX_NNZ;i++){
    col1[i] = -1;
    A1[i] = 0.0;
  }
  for(int i=0;i<neq1;i++){
    temp[i] = 0.0;
    b[i] = 0.0;
  }
}



//----------------------------------------------------------------------------
// assemble global heat system
//----------------------------------------------------------------------------
void SolverHeat::assembleHeat(int *row, int *col, double *A, double *b, BoundaryData *data, Mesh *mesh)
{
  int n = mesh->get_N();
  int r=0,c=0;
  double v1=0,v2=0;

  for(int i=0;i<n+1;i++){row[i] = 0;}
  for(int i=0;i<n*MAX_NNZ;i++){
    col[i] = -1;
    A[i] = 0.0;
  }

  double *tmp = new double[n];
  for(int i=0;i<n;i++){tmp[i] = 0.0;}

  //loop through all elements
  for(int k=0;k<mesh->get_Ne();k++){
    Element e(mesh->get_Type());
    for(int l=0;l<e.get_Npe();l++){
      e.nodes[l] = mesh->get_connect(k,l);
      e.xpts[l] = mesh->get_xpoint(e.nodes[l]-1);
      e.ypts[l] = mesh->get_ypoint(e.nodes[l]-1);
      e.zpts[l] = mesh->get_zpoint(e.nodes[l]-1);
    }

    //determine which nodes in element are on a dirichlet boundary
    bool *bcFlags = new bool[e.get_Npe()];
    double *bcVals = new double[e.get_Npe()];
    for(int i=0;i<e.get_Npe();i++){
      bcFlags[i] = data->isDirichlet(e.nodes[i]-1);
      bcVals[i] = data->getDirichletValue(e.nodes[i]-1);
    }

    e.stiffnessMatrix();
    e.massMatrix();

    //for(int i=0;i<e.Npe;i++){
    //  for(int j=0;j<e.Npe;j++){
    //    std::cout<<e.emat[i][j]<<"  ";   
    //  } 
    //  std::cout<<" "<<std::endl;
    //}
    //std::cout<<" "<<std::endl;

    //update col, A arrays
    double theta = fem->get_theta();
    double kappa = fem->get_kappa();
    double dt = fem->get_dt();
    for(int i=0;i<e.get_Npe();i++){
      for(int j=0;j<e.get_Npe();j++){
        r = e.nodes[i] - 1;
        c = e.nodes[j] - 1;
        v1 = e.mmat[i][j] + theta/kappa*dt*e.kmat[i][j];
        v2 = e.mmat[i][j] - (1-theta)/kappa*dt*e.kmat[i][j];
        
        tmp[r] += v2*b[c];
        if(bcFlags[i]==true){           //
          tmp[r] = bcVals[i];           //
          v1 = 0.0;                     //
        }                               // enforce dirichlet 
        else if(bcFlags[j]==true){      // boundary conditions
          tmp[r] -= v1*bcVals[j];       //
          v1 = 0.0;                     //
        }                               //

        for(int p=MAX_NNZ*r;p<MAX_NNZ*r+MAX_NNZ;p++){
          if(col[p]==-1){
            col[p] = c;
            A[p] = v1;
            break;
          }
          else if(col[p]==c){
            A[p] += v1;
            break;
          }
        }
      }
    }

    delete[] bcFlags;
    delete[] bcVals;
  }

  //update row array
  int jj = 0;
  for(int i=1;i<n+1;i++){
    for(int j=0;j<MAX_NNZ;j++){
      if(col[i*MAX_NNZ-MAX_NNZ+j]==-1){jj=j; break;}
    }
    row[i] = row[i-1] + jj;
  }

  //sort (insertion) col and A arrays
  for(int p=0;p<n;p++){
    int start = p*MAX_NNZ;
    for(int i=1;i<row[p+1]-row[p];i++){
      int entry = col[i+start];
      double entryA = A[i+start];
      int j = 0;
      for(j=i;j>0 && entry<col[j-1+start];j--){
        col[j+start] = col[j-1+start];
        A[j+start] = A[j-1+start];
      }
      col[j+start] = entry;
      A[j+start] = entryA;
    }
  }

  //compress col and A arrays
  int index=0;
  for(int i=0;i<n*MAX_NNZ;i++){
    if(col[i]==-1)
      continue;
    col[index] = col[i];
    A[index] = A[i];
    index++;
  }

  //set unused parts of arrays to 0 or -1
  for(int i=index;i<n*MAX_NNZ;i++){
    col[i] = -1;
    A[i] = 0.0;
  }

  for(int i=0;i<n;i++){
    for(int j=row[i];j<row[i+1];j++){
      if(col[j]==i && A[j]==0.0){
        A[j] = 1.0;
      }
    }
  }

  for(int i=0;i<n;i++){b[i] = tmp[i];}

  delete [] tmp;
}



//----------------------------------------------------------------------------
// return pointer to temperature array 
//----------------------------------------------------------------------------
double* SolverHeat::getTemperature()
{
  return temp;
}



//-------------------------------------------------------------------
// theta method timestepping
//-------------------------------------------------------------------
void SolverHeat::theta_method()
{
  //update rhs vector to equal current temp
  for(int i=0;i<neq1;i++){b[i] = temp[i];}

  //assemble global stiffness
  assembleHeat(row1,col1,A1,b,fem->tdata,mesh1);

  IterSolvers::pcg(row1,col1,A1,temp,b,neq1,MAX_ITER,EPSILON);
}




//-------------------------------------------------------------------
// Solve heat equation
//-------------------------------------------------------------------
void SolverHeat::solve()
{
  //time loop
  for(int i=0;i<fem->get_steps();i++){
    std::cout<<"timestep: "<<i<<"temp[0]: "<<temp[0]<<std::endl;
    theta_method(); 
  }

  //write solution vector to output file
  PostProcessing::write_results_gmsh_msh(temp,filename,mesh1);
}



//----------------------------------------------------------------------------
// destructor for heat solver
//----------------------------------------------------------------------------
SolverHeat::~SolverHeat()
{
  delete [] row1;
  delete [] col1;
  delete [] A1;

  delete [] temp;
  delete [] b;
}



















//********************************************************************************
//
//  Fluid equations solver
//
//  du/dt + u*du/dx + v*du/dy + w*du/dz = -dp/dx + (1/Re)*(d^2u/dx^2 + d^2u/dy^2 d^2u/dz^2)
//  dv/dt + u*dv/dx + v*dv/dy + w*dv/dz = -dp/dy + (1/Re)*(d^2v/dx^2 + d^2v/dy^2 d^2v/dz^2)
//  dw/dt + u*dw/dx + v*dw/dy + w*dw/dz = -dp/dz + (1/Re)*(d^2w/dx^2 + d^2w/dy^2 d^2w/dz^2)
//  du/dx + dv/dy + dw/dz = 0
//
//********************************************************************************



//----------------------------------------------------------------------------
// Constructor for fluid (momentum and continuity equation) Solver
//----------------------------------------------------------------------------
SolverFluid::SolverFluid(Mesh *mesh1, Mesh *mesh2, FeModel *fem) : Solver(mesh1,mesh2,fem)
{
  ufilename = "fluid_u.msh";
  vfilename = "fluid_v.msh";
  wfilename = "fluid_w.msh";
  pfilename = "fluid_p.msh";

  neq1 = mesh1->get_N();
  neq2 = mesh2->get_N();

  row1 = new int[neq1+1];
  col1 = new int[neq1*MAX_NNZ];
  row2 = new int[neq2+1];
  col2 = new int[neq2*MAX_NNZ];

  A1 = new double[neq1*MAX_NNZ];
  A2 = new double[neq2*MAX_NNZ];

  u_vel = new double[neq1];
  v_vel = new double[neq1];
  w_vel = new double[neq1];
  pres1 = new double[neq1];
  pres2 = new double[neq2];
  div = new double[neq2];

  u_old = new double[neq1];
  v_old = new double[neq1];
  w_old = new double[neq1];
 
  b_u = new double[neq1];
  b_v = new double[neq1];
  b_w = new double[neq1];
  
  for(int i=0;i<neq1+1;i++){row1[i] = 0;}
  for(int i=0;i<neq2+1;i++){row2[i] = 0;}
  for(int i=0;i<neq1*MAX_NNZ;i++){
    col1[i] = -1;
    A1[i] = 0.0;
  }
  for(int i=0;i<neq2*MAX_NNZ;i++){
    col2[i] = -1;
    A2[i] = 0.0;
  }
  for(int i=0;i<neq1;i++){
    u_vel[i] = 0.0;
    v_vel[i] = 0.0;
    w_vel[i] = 0.0;
    u_old[i] = 0.0;
    v_old[i] = 0.0;
    w_old[i] = 0.0;
    pres1[i] = 0.0;
    b_u[i] = 0.0;
    b_v[i] = 0.0;
    b_w[i] = 0.0;
  }
  for(int i=0;i<neq2;i++){
    div[i] = 0.0;
    pres2[i] = 0.0;
  }
}



//----------------------------------------------------------------------------
// assemble global diffusion system
//----------------------------------------------------------------------------
void SolverFluid::assembleDiff(int *row, int *col, double *A, double *b, BoundaryData *data, Mesh *mesh)
{
  int n = mesh->get_N();
  int r=0,c=0;
  double v1=0,v2=0;

  for(int i=0;i<n+1;i++){row[i] = 0;}
  for(int i=0;i<n*MAX_NNZ;i++){
    col[i] = -1;
    A[i] = 0.0;
  }

  double *tmp = new double[n];
  for(int i=0;i<n;i++){tmp[i] = 0.0;}

  //loop through all elements
  for(int k=0;k<mesh->get_Ne();k++){
    Element e(mesh->get_Type());
    for(int l=0;l<e.get_Npe();l++){
      e.nodes[l] = mesh->get_connect(k,l);
      e.xpts[l] = mesh->get_xpoint(e.nodes[l]-1);
      e.ypts[l] = mesh->get_ypoint(e.nodes[l]-1);
      e.zpts[l] = mesh->get_zpoint(e.nodes[l]-1);
    }

    //determine which nodes in element are on a dirichlet boundary
    bool *bcFlags = new bool[e.get_Npe()];
    double *bcVals = new double[e.get_Npe()];
    for(int i=0;i<e.get_Npe();i++){
      bcFlags[i] = data->isDirichlet(e.nodes[i]-1);
      bcVals[i] = data->getDirichletValue(e.nodes[i]-1);
    }

    e.stiffnessMatrix();
    e.massMatrix();

    //for(int i=0;i<e.Npe;i++){
    //  for(int j=0;j<e.Npe;j++){
    //    std::cout<<e.emat[i][j]<<"  ";   
    //  } 
    //  std::cout<<" "<<std::endl;
    //}
    //std::cout<<" "<<std::endl;

    // //form K = M+theta*dt*K and M = M-(1-theta)*dt*K 
    // for(int i=0;i<row1[neq1];i++){
    //   K[i] = M_i+theta/fem->get_Re()*fem->get_dt()*K_i;
    //   M[i] = M_i-(1-theta)/fem->get_Re()*fem->get_dt()*K_i;
    // }

    //update col, A arrays
    double theta = fem->get_theta();
    double Re = fem->get_Re();
    double dt = fem->get_dt();
    for(int i=0;i<e.get_Npe();i++){
      for(int j=0;j<e.get_Npe();j++){
        r = e.nodes[i] - 1;
        c = e.nodes[j] - 1;
        v1 = e.mmat[i][j] + theta/Re*dt*e.kmat[i][j];
        v2 = e.mmat[i][j] - (1-theta)/Re*dt*e.kmat[i][j];
        
        tmp[r] += v2*b[c];
        if(bcFlags[i]==true){           //
          tmp[r] = bcVals[i];           //
          v1 = 0.0;                     //
        }                               // enforce dirichlet 
        else if(bcFlags[j]==true){      // boundary conditions
          tmp[r] -= v1*bcVals[j];       //
          v1 = 0.0;                     //
        }                               //

        for(int p=MAX_NNZ*r;p<MAX_NNZ*r+MAX_NNZ;p++){
          if(col[p]==-1){
            col[p] = c;
            A[p] = v1;
            break;
          }
          else if(col[p]==c){
            A[p] += v1;
            break;
          }
        }
      }
    }

    //std::cout<<" "<<std::endl;
    delete[] bcFlags;
    delete[] bcVals;
  }

  //update row array
  int jj = 0;
  for(int i=1;i<n+1;i++){
    for(int j=0;j<MAX_NNZ;j++){
      if(col[i*MAX_NNZ-MAX_NNZ+j]==-1){jj=j; break;}
    }
    row[i] = row[i-1] + jj;
  }

  //sort (insertion) col and A arrays
  for(int p=0;p<n;p++){
    int start = p*MAX_NNZ;
    for(int i=1;i<row[p+1]-row[p];i++){
      int entry = col[i+start];
      double entryA = A[i+start];
      int j = 0;
      for(j=i;j>0 && entry<col[j-1+start];j--){
        col[j+start] = col[j-1+start];
        A[j+start] = A[j-1+start];
      }
      col[j+start] = entry;
      A[j+start] = entryA;
    }
  }

  //compress col and A arrays
  int index=0;
  for(int i=0;i<n*MAX_NNZ;i++){
    if(col[i]==-1)
      continue;
    col[index] = col[i];
    A[index] = A[i];
    index++;
  }

  //set unused parts of arrays to 0 or -1
  for(int i=index;i<n*MAX_NNZ;i++){
    col[i] = -1;
    A[i] = 0.0;
  }

  for(int i=0;i<n;i++){
    for(int j=row[i];j<row[i+1];j++){
      if(col[j]==i && A[j]==0.0){
        A[j] = 1.0;
      }
    }
  }

  for(int i=0;i<n;i++){b[i] = tmp[i];}

  delete [] tmp;
}



//----------------------------------------------------------------------------
// assemble global pressure system
//----------------------------------------------------------------------------
void SolverFluid::assemblePres(int *row, int *col, double *A, double *b, Mesh *mesh)
{
  int n = mesh->get_N();
  int r=0,c=0;
  double v=0;

  for(int i=0;i<n;i++){b[i] = 0.0;}
  for(int i=0;i<n+1;i++){row[i] = 0;}
  for(int i=0;i<n*MAX_NNZ;i++){
    col[i] = -1;
    A[i] = 0.0;
  }

  //loop through all elements
  for(int k=0;k<mesh->get_Ne();k++){
    Element e(mesh->get_Type());
    for(int l=0;l<e.get_Npe();l++){
      e.nodes[l] = mesh->get_connect(k,l);
      e.xpts[l] = mesh->get_xpoint(e.nodes[l]-1);
      e.ypts[l] = mesh->get_ypoint(e.nodes[l]-1);
      e.zpts[l] = mesh->get_zpoint(e.nodes[l]-1);
    }

    e.stiffnessMatrix();
    e.advecXMatrix();
    e.advecYMatrix();

    //update col, A arrays
    double theta = fem->get_theta();
    double dt = fem->get_dt();
    for(int i=0;i<e.get_Npe();i++){
      for(int j=0;j<e.get_Npe();j++){
        r = e.nodes[i] - 1;
        c = e.nodes[j] - 1;
        v = e.kmat[i][j];
        
        b[r] -= (e.axmat[i][j]*u_vel[link2[c]-1] + e.aymat[i][j]*v_vel[link2[c]-1])/(dt*theta);
        for(int p=MAX_NNZ*r;p<MAX_NNZ*r+MAX_NNZ;p++){
          if(col[p]==-1){
            col[p] = c;
            A[p] = v;
            break;
          }
          else if(col[p]==c){
            A[p] += v;
            break;
          }
        }
      }
    }
  }

  //update row array
  int jj = 0;
  for(int i=1;i<n+1;i++){
    for(int j=0;j<MAX_NNZ;j++){
      if(col[i*MAX_NNZ-MAX_NNZ+j]==-1){jj=j; break;}
    }
    row[i] = row[i-1] + jj;
  }

  //sort (insertion) col and A arrays
  for(int p=0;p<n;p++){
    int start = p*MAX_NNZ;
    for(int i=1;i<row[p+1]-row[p];i++){
      int entry = col[i+start];
      double entryA = A[i+start];
      int j = 0;
      for(j=i;j>0 && entry<col[j-1+start];j--){
        col[j+start] = col[j-1+start];
        A[j+start] = A[j-1+start];
      }
      col[j+start] = entry;
      A[j+start] = entryA;
    }
  }

  //compress col and A arrays
  int index=0;
  for(int i=0;i<n*MAX_NNZ;i++){
    if(col[i]==-1)
      continue;
    col[index] = col[i];
    A[index] = A[i];
    index++;
  }

  //set unused parts of arrays to 0 or -1
  for(int i=index;i<n*MAX_NNZ;i++){
    col[i] = -1;
    A[i] = 0.0;
  }

  //apply dirichlet condition at node 0 for pressure stiffness matrix
  for(int i=row[0];i<row[1];i++){A[i] = 0.0;}
  //b[0] = 1.0; A[0] = 1.0;
  A[0] = 1.0; b[0] = 1.0;
  for(int i=1;i<n;i++){
    for(int j=row[i];j<row[i+1];j++){
      if(col[j]==0){
        b[i] = b[i] - b[0]*A[j]; 
        A[j] = 0.0; 
        break;
      }
    }
  }
}



//----------------------------------------------------------------------------
// assemble global pressure update system
//----------------------------------------------------------------------------
void SolverFluid::assemblePresUpd(int *row, int *col, double *A, double *b, BoundaryData *data, Mesh *mesh, int comp)
{
  int n = mesh->get_N();
  int r=0,c=0;
  double v=0;

  for(int i=0;i<n;i++){b[i] = 0.0;}
  for(int i=0;i<n+1;i++){row[i] = 0;}
  for(int i=0;i<n*MAX_NNZ;i++){
    col[i] = -1;
    A[i] = 0.0;
  }

  double *tmp = new double[n];
  for(int i=0;i<n;i++){tmp[i] = 0.0;}

  //loop through all elements
  for(int k=0;k<mesh->get_Ne();k++){
    Element e(mesh->get_Type());
    for(int l=0;l<e.get_Npe();l++){
      e.nodes[l] = mesh->get_connect(k,l);
      e.xpts[l] = mesh->get_xpoint(e.nodes[l]-1);
      e.ypts[l] = mesh->get_ypoint(e.nodes[l]-1);
      e.zpts[l] = mesh->get_zpoint(e.nodes[l]-1);
    }

    //determine which nodes in element are on a dirichlet boundary
    bool *bcFlags = new bool[e.get_Npe()];
    double *bcVals = new double[e.get_Npe()];
    for(int i=0;i<e.get_Npe();i++){
      bcFlags[i] = data->isDirichlet(e.nodes[i]-1);
      bcVals[i] = data->getDirichletValue(e.nodes[i]-1);
    }

    e.stiffnessMatrix();
    e.massMatrix();
    switch(comp)
    {
    case 1:
      e.advecXMatrix();
      break;
    case 2:
      e.advecYMatrix();
      break;
    }

  //update velocities to include pressure
  // assembleGVEC(row1,col1,K,u_vel,b_u,mesh1);
  // assembleGVEC(row1,col1,K,v_vel,b_v,mesh1);
  // for(int i=0;i<neq1;i++){
  //   for(int j=row1[i];j<row1[i+1];j++){
  //     b_u[i] -= fem->get_dt()*theta*Hx[j]*pres[col1[j]];
  //     b_v[i] -= fem->get_dt()*theta*Hy[j]*pres[col1[j]];
  //   }
  // }

  //K[i] = M_i+theta/fem->get_Re()*fem->get_dt()*K_i;
  // applyBCGVEC(b_u,fem->udata,mesh1);
  // applyBCGVEC(b_v,fem->vdata,mesh1);
  // iter = IterSolvers::pcg(row1,col1,K,u_old,b_u,neq1,MAX_ITER,EPSILON);
  // iter = IterSolvers::pcg(row1,col1,K,v_old,b_v,neq1,MAX_ITER,EPSILON);

    //update col, A arrays
    double theta = fem->get_theta();
    double Re = fem->get_Re();
    double dt = fem->get_dt();
    for(int i=0;i<e.get_Npe();i++){
      for(int j=0;j<e.get_Npe();j++){
        r = e.nodes[i] - 1;
        c = e.nodes[j] - 1;
        v = e.mmat[i][j] + theta/Re*dt*e.kmat[i][j];
        
        switch(comp)
        {
        case 1:
          tmp[r] += v*u_vel[c] - dt*theta*e.axmat[i][j]*pres1[c];
          break;
        case 2:
          tmp[r] += v*v_vel[c] - dt*theta*e.aymat[i][j]*pres1[c];
          break;
        }

        if(bcFlags[i]==true){          //
          tmp[r] = bcVals[i];          //
          v = 0.0;                     //
        }                              // enforce dirichlet 
        else if(bcFlags[j]==true){     // boundary conditions
          tmp[r] -= v*bcVals[j];       //
          v = 0.0;                     //
        }                              //

        for(int p=MAX_NNZ*r;p<MAX_NNZ*r+MAX_NNZ;p++){
          if(col[p]==-1){
            col[p] = c;
            A[p] = v;
            break;
          }
          else if(col[p]==c){
            A[p] += v;
            break;
          }
        }
      }
    }

    delete[] bcFlags;
    delete[] bcVals;
  }

  //update row array
  int jj = 0;
  for(int i=1;i<n+1;i++){
    for(int j=0;j<MAX_NNZ;j++){
      if(col[i*MAX_NNZ-MAX_NNZ+j]==-1){jj=j; break;}
    }
    row[i] = row[i-1] + jj;
  }

  //sort (insertion) col and A arrays
  for(int p=0;p<n;p++){
    int start = p*MAX_NNZ;
    for(int i=1;i<row[p+1]-row[p];i++){
      int entry = col[i+start];
      double entryA = A[i+start];
      int j = 0;
      for(j=i;j>0 && entry<col[j-1+start];j--){
        col[j+start] = col[j-1+start];
        A[j+start] = A[j-1+start];
      }
      col[j+start] = entry;
      A[j+start] = entryA;
    }
  }

  //compress col and A arrays
  int index=0;
  for(int i=0;i<n*MAX_NNZ;i++){
    if(col[i]==-1)
      continue;
    col[index] = col[i];
    A[index] = A[i];
    index++;
  }

  //set unused parts of arrays to 0 or -1
  for(int i=index;i<n*MAX_NNZ;i++){
    col[i] = -1;
    A[i] = 0.0;
  }

  for(int i=0;i<n;i++){
    for(int j=row[i];j<row[i+1];j++){
      if(col[j]==i && A[j]==0.0){
        A[j] = 1.0;
      }
    }
  }

  for(int i=0;i<n;i++){b[i] = tmp[i];}

  delete [] tmp;
}



//----------------------------------------------------------------------------
// interpolate pres2 to pres1 array
//----------------------------------------------------------------------------
void SolverFluid::interpolatePres()
{
  for(int i=0;i<mesh1->get_Ne();i++){
    Element e(mesh2->get_Type());
    for(int j=0;j<e.get_Npe();j++){
      e.nodes[j] = mesh1->get_connect(i,j);
      e.xpts[j] = mesh1->get_xpoint(e.nodes[j]-1);
      e.ypts[j] = mesh1->get_ypoint(e.nodes[j]-1);
      e.zpts[j] = mesh1->get_zpoint(e.nodes[j]-1);
    }
    
    for(int j=0;j<mesh1->get_Npe();j++){
      if(link1[mesh1->get_connect(i,j)-1]==-1){
        double xpt = mesh1->get_xpoint(mesh1->get_connect(i,j)-1);
        double ypt = mesh1->get_ypoint(mesh1->get_connect(i,j)-1);
        double zpt = mesh1->get_zpoint(mesh1->get_connect(i,j)-1);
        double val1 = pres2[link1[mesh1->get_connect(i,0)-1]-1];
        double val2 = pres2[link1[mesh1->get_connect(i,1)-1]-1];
        double val3 = pres2[link1[mesh1->get_connect(i,2)-1]-1];
        pres1[mesh1->get_connect(i,j)-1] = e.value(xpt,ypt,zpt,val1,val2,val3); 
      }
      else{
        pres1[mesh1->get_connect(i,j)-1] = pres2[link1[mesh1->get_connect(i,j)-1]-1];
      }
    }
  }
}



//----------------------------------------------------------------------------
// return pointer to u, v, w velocities and pressure for fluid solver
//----------------------------------------------------------------------------
double* SolverFluid::getUVelocity()
{
  return u_vel;
}


double* SolverFluid::getVVelocity()
{
  return v_vel;
}


double* SolverFluid::getWVelocity()
{
  return w_vel;
}


double* SolverFluid::getPressure()
{
  return pres1;
}



//----------------------------------------------------------------------------
// theta method time stepping (2D)
// (see Finite element for incompressible Navier-Stokes equations by Ir. A. Segal 2015)
//----------------------------------------------------------------------------
void SolverFluid::theta_method2D()
{
  //update rhs vectors to equal current velocity components
  for(int i=0;i<neq1;i++){
    b_u[i] = u_old[i];
    b_v[i] = v_old[i];
  }

  //assemble and solve global diffusion system for each velocity component
  assembleDiff(row1,col1,A1,b_u,fem->udata,mesh1);
  IterSolvers::pcg(row1,col1,A1,u_vel,b_u,neq1,MAX_ITER,EPSILON);

  assembleDiff(row1,col1,A1,b_v,fem->vdata,mesh1);
  IterSolvers::pcg(row1,col1,A1,v_vel,b_v,neq1,MAX_ITER,EPSILON);

  //assemble and solve pressure system -Kp*(p^n-p^n-1) = div/(2*dt) on mesh2
  assemblePres(row2,col2,A2,div,mesh2);
  IterSolvers::pcg(row2,col2,A2,pres2,div,neq2,MAX_ITER,EPSILON);

  //interpolate pres2 array on mesh2 to pres1 array on mesh1
  interpolatePres();

  //assemble and solve pressure update system for each velocity component
  //assemblePresUpd(row1,col1,A1,b_u,fem->udata,mesh1,1);
  //IterSolvers::pcg(row1,col1,A1,u_old,b_u,neq1,MAX_ITER,EPSILON);

  //assemblePresUpd(row1,col1,A1,b_v,fem->vdata,mesh1,2);
  //IterSolvers::pcg(row1,col1,A1,v_old,b_v,neq1,MAX_ITER,EPSILON);
}




//----------------------------------------------------------------------------
// Solve Fluid (momentum and continuity) equations
//----------------------------------------------------------------------------
void SolverFluid::solve()
{
  void (SolverFluid::*timestepper)(void) = NULL;

  //set time stepper and solve
  switch(mesh1->get_Dim())
  {
    case 2:
      timestepper = &SolverFluid::theta_method2D;
      break;
    case 3:
      //timestepper = &SolverFluid::theta_method3D;
      break;
  }

  for(int i=0;i<fem->get_steps();i++){
    std::cout<<"timestep: "<<i<<"u_vel[0]: "<<u_vel[0]<<std::endl;
    (*this.*timestepper)(); // ugly c++...
  }


  //write solution vector to output file
  PostProcessing::write_results_gmsh_msh(u_vel,ufilename,mesh1);
  PostProcessing::write_results_gmsh_msh(v_vel,vfilename,mesh1);
  PostProcessing::write_results_gmsh_msh(pres1,pfilename,mesh1);

  if(mesh1->get_Dim()==3){
    PostProcessing::write_results_gmsh_msh(w_vel,wfilename,mesh1);
  }
}




//----------------------------------------------------------------------------
// destructor for fluid solver
//----------------------------------------------------------------------------
SolverFluid::~SolverFluid()
{
  delete [] row1;
  delete [] col1;
  delete [] row2;
  delete [] col2;

  delete [] A1;
  delete [] A2;

  delete [] u_vel;
  delete [] v_vel;
  delete [] w_vel;
  delete [] pres1;
  delete [] pres2;
  delete [] div;

  delete [] u_old;
  delete [] v_old;
  delete [] w_old;

  delete [] b_u;
  delete [] b_v;
  delete [] b_w;
}







//----------------------------------------------------------------------------
// Global Force Vector f
//----------------------------------------------------------------------------
// void Solver::assembleGFV(double *fvec, Mesh *mesh)
// {
//   int n = mesh->N;

//   double *temp = new double[n];
//   for(int i=0;i<n;i++){temp[i] = 0.0;}

//   //for(int i=0;i<n;i++){fvec[i] = 0.0;}

//   //compute internal heat generation vector fvec=Q
//   for(int k=0;k<mesh->Ne;k++){
//     Element e(mesh->Type);
//     for(int l=0;l<e.Npe;l++){
//       e.nodes[l] = mesh->connect[k][l];
//       e.xpts[l] = mesh->xpoints[e.nodes[l]-1];
//       e.ypts[l] = mesh->ypoints[e.nodes[l]-1];
//       e.zpts[l] = mesh->zpoints[e.nodes[l]-1];
//     }

//     e.elementVector();
//     for(int i=0;i<e.Npe;i++){
//       //fvec[e.nodes[i]-1] += fem->get_Q()*e.evec[i];
//       temp[e.nodes[i]-1] += fvec[e.nodes[i]-1]*e.evec[i];
//     }
//   }

//   for(int i=0;i<n;i++){fvec[i] = temp[i];}

//   delete [] temp;
// }