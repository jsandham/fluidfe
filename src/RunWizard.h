//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef RUNWIZARD_H
#define RUNWIZARD_H

#include <QtGui>
#include <QWizard>
#include "Mesh.h"
#include "FeModel.h"

class QLabel;
class QLineEdit;
class QRadioButton;
class QComboBox;


//--------------------------------------------------------------------------------
// RunWizard class
//--------------------------------------------------------------------------------
class RunWizard : public QWizard
{
     Q_OBJECT

 private:
     Mesh *mesh;
     BoundaryData *bdata;
     FeModel *fem;

 public:
     RunWizard(Mesh *msh, QWidget *parent = 0);
     ~RunWizard();

     void accept();
};



//--------------------------------------------------------------------------------
// Intro page class for RunWizard
//--------------------------------------------------------------------------------
class IntroPage : public QWizardPage
{
     Q_OBJECT

 public:
     IntroPage(QWidget *parent = 0);
     ~IntroPage();

 private:
     QVBoxLayout *layout;
     QLabel *label;
};



//--------------------------------------------------------------------------------
// Time page class for RunWizard
//--------------------------------------------------------------------------------
class TimePage : public QWizardPage
{
     Q_OBJECT

 public:
     TimePage(QWidget *parent = 0);
     ~TimePage();

 private:
     QVBoxLayout *layout;
     QHBoxLayout *layout_top;
     QHBoxLayout *layout_bot;
     QVBoxLayout *layout_bot_left;
     QVBoxLayout *layout_bot_right;
     QHBoxLayout *layout1;
     QHBoxLayout *layout2;

     QRadioButton *stationaryRadioButton;
     QRadioButton *transientRadioButton;
     QLineEdit *dtLineEdit;
     QLineEdit *ntLineEdit;
     QLabel *dtLabel;
     QLabel *ntLabel;

 protected:
     virtual bool isComplete()const;

 private slots:
     void stationary();
     void transient();
};



//--------------------------------------------------------------------------------
// Multi physics page class for RunWizard
//--------------------------------------------------------------------------------
class MultiPhysicsPage : public QWizardPage
{
     Q_OBJECT

 public:
     MultiPhysicsPage(QWidget *parent = 0);
     ~MultiPhysicsPage();     

 private:
     QHBoxLayout *layout;
     QRadioButton *heatRadioButton;
     QRadioButton *fluidRadioButton;
};



//--------------------------------------------------------------------------------
// Material page class for RunWizard
//--------------------------------------------------------------------------------
class MaterialPage : public QWizardPage
{
     Q_OBJECT

 public:
     MaterialPage(QWidget *parent = 0);
     ~MaterialPage();

 private:
     QHBoxLayout *layout;
     QLabel *rhoLabel;
     QLabel *cLabel;
     QLabel *qLabel;
     QLabel *kLabel;
     QLineEdit *rhoLineEdit;
     QLineEdit *cLineEdit;
     QLineEdit *qLineEdit;
     QLineEdit *kLineEdit;
};



//--------------------------------------------------------------------------------
// Boundary page class for RunWizard
//--------------------------------------------------------------------------------
class BoundaryPage : public QWizardPage
{
     Q_OBJECT

 public:
     BoundaryData *bdata;

     BoundaryPage(BoundaryData *bd, QWidget *parent = 0);
     ~BoundaryPage();

 private:
     QVBoxLayout *layout;
     QHBoxLayout *title_layout;
     QLabel *grp_title;
     QLabel *val_title;
     QLabel *bc_title;
     QHBoxLayout **hlayouts;
     QLabel **labels;
     QLineEdit **lineEdits;
     QComboBox **comboBoxes;
     QComboBox *grpbox;
     QComboBox *condbox;
     QLabel *vLabel;
     QLineEdit *vLineEdit;
     QPushButton *saveButton;

 private slots:
     void save();
};



//--------------------------------------------------------------------------------
// Launch page class for RunWizard
//--------------------------------------------------------------------------------
class LaunchPage : public QWizardPage
{
     Q_OBJECT

 public:
     LaunchPage(QWidget *parent = 0);
     ~LaunchPage();

 private:
     QLabel *label;
};

#endif
