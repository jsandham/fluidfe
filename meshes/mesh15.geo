Point(1) = {0, 0, 0, 1.0};
Point(2) = {1, 0, 0, 1.0};
Point(3) = {-1, 0, 0, 1.0};
Point(4) = {0, 1, 0, 1.0};
Point(5) = {0, -1, 0, 1.0};
Point(6) = {2, 2, 0, 1.0};
Point(7) = {2, -2, 0, 1.0};
Point(8) = {-2, -2, 0, 1.0};
Point(9) = {-2, 2, 0, 1.0};
Line(1) = {8, 7};
Line(2) = {7, 6};
Line(3) = {6, 9};
Line(4) = {9, 8};
Circle(5) = {5, 1, 2};
Circle(6) = {2, 1, 4};
Circle(7) = {4, 1, 3};
Circle(8) = {3, 1, 5};
Line Loop(9) = {5, 6, 7, 8};
Line Loop(10) = {2, 3, 4, 1};
Plane Surface(11) = {9, 10};
Extrude {0, 0, 4} {
  Surface{11};
}
Surface Loop(54) = {28, 11, 24, 53, 32, 36, 40, 44, 48, 52};
Volume(55) = {54};
Physical Surface(56) = {40, 53, 11, 36, 24, 28, 32, 48};
Physical Surface(57) = {44};
Physical Surface(58) = {52};
Physical Volume(59) = {55};
