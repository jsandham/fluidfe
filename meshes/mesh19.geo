Point(1) = {0, 0, 0, 1.0};
Point(2) = {10, 0, 0, 1.0};
Point(3) = {10, 2, 0, 1.0};
Point(4) = {6, 2, 0, 1.0};
Point(5) = {4, 2, 0, 1.0};
Point(6) = {5, 1, 0, 1.0};
Point(7) = {0, 2, 0, 1.0};
Point(8) = {9, 0, 0, 1.0};
Point(9) = {7, 0, 0, 1.0};
Point(10) = {8, 1, 0, 1.0};
Point(11) = {1, 0, 0, 1.0};
Point(12) = {3, 0, 0, 1.0};
Point(13) = {2, 1, 0, 1.0};
Point(14) = {2, 0, 0, 1.0};
Point(15) = {8, 0, 0, 1.0};
Point(16) = {5, 2, 0, 1.0};
Line(1) = {1, 11};
Line(2) = {12, 9};
Line(3) = {8, 2};
Line(4) = {2, 3};
Line(5) = {3, 4};
Line(6) = {5, 7};
Line(7) = {7, 1};
Circle(8) = {11, 14, 13};
Circle(9) = {13, 14, 12};
Circle(10) = {9, 15, 10};
Circle(11) = {10, 15, 8};
Circle(12) = {4, 16, 6};
Circle(13) = {6, 16, 5};
Line Loop(14) = {2, 10, 11, 3, 4, 5, 12, 13, 6, 7, 1, 8, 9};
Plane Surface(15) = {14};
Physical Line(16) = {7};
Physical Line(17) = {1, 8, 9, 2, 10, 11, 3, 5, 12, 13, 6};
Physical Line(18) = {4};
Physical Surface(19) = {15};
