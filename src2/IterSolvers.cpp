//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#include<iostream>
#include"IterSolvers.h"
#include"math.h"


//********************************************************************************
//
//  Collection of Iterative Solvers
//
//********************************************************************************



//----------------------------------------------------------------------------
// constructor for Iterative Solvers
//----------------------------------------------------------------------------
IterSolvers::IterSolvers()
{

}


//----------------------------------------------------------------------------
// destructor for Iterative Solvers
//----------------------------------------------------------------------------
IterSolvers::~IterSolvers()
{

}


//----------------------------------------------------------------------------
// preconditioned conjugte gradient solver
//----------------------------------------------------------------------------
int IterSolvers::pcg(int row[], int col[], double A[], double x[], double b[], int n, int maxIter, double tol)
{
  double *r = new double[n];
  double *w = new double[n];
  double *p = new double[n];
  double *md = new double[n];

  for(int i=0;i<n;i++){
    r[i] = 0.0;
    w[i] = 0.0;
    p[i] = 0.0;
    md[i] = 0.0;
  }

  //find diagonal of A matrix
  diagonalPreconditioner(row,col,A,md,n);

  //calculate initial x
  for(int i=0;i<n;i++){
    x[i] = b[i]*md[i];
  }

  //r = b-A*x and initial error
  matrixVectorProduct(row,col,A,x,r,n);
  double check = 0;
  for(int i=0;i<n;i++){
    r[i] = b[i] - r[i];
    check += r[i];
  }

  double gamma0 = 1;
  double gammai = 1;
  int iter = 0;
  if(fabs(check)<tol){
    iter = 1;
  }
  else{
    for(iter=0; iter<maxIter; iter++){
      //w = (M^-1)*r
      for(int i=0;i<n;i++){
        w[i] = md[i]*r[i];
      }

      //gam = (r,w)
      double gammai1 = gammai;
      gammai = 0;
      for(int i=0;i<n;i++){
        gammai += r[i]*w[i];
      }
      if(iter==0){                          //
        gamma0 = gammai;                    //
        for(int i=0;i<n;i++)                // To Do: remove if statement
          p[i] = w[i];                      //
      }                                     //
      else{                                 //
        double rg = gammai/gammai1;         //
        for(int i=0;i<n;i++)                //
          p[i] = w[i] + rg*p[i];            //
      }                                     //

      //w = A*p
      matrixVectorProduct(row,col,A,p,w,n);
      double beta = 0;
      for(int i=0;i<n;i++)
        beta += p[i]*w[i];
      double alpha = gammai/beta;

      //update x and r, calculate error
      for(int i=0;i<n;i++){
        x[i] += alpha*p[i];
        r[i] -= alpha*w[i];
      }
      double err = sqrt(gammai/gamma0);
      //std::cout<<"error: "<<err<<std::endl;
      if(err<tol)
        break;
    }
  }

  delete[] r;
  delete[] w;
  delete[] p;
  delete[] md;

  return iter;
}



//----------------------------------------------------------------------------
// generalised minimum residual solver
//----------------------------------------------------------------------------
int IterSolvers::gmres(int row[], int col[], double A[], double x[], double b[], int n, int restart, int maxIter, double tol)
{
  //res = b-A*x 
  double residual = 0.0;
  double *res = new double[n];
  matrixVectorProduct(row,col,A,x,res,n);
  for(int i=0;i<n;i++){residual += (b[i]-res[i])*(b[i]-res[i]);}
  if(residual<tol){return 1;}

  //create q and v array
  double *q = new double[n];
  double *v = new double[n];

  //create H and Q matrices (which are dense and stored as vectors columnwise) 
  double *H = new double[(restart+1)*restart];
  double *Q = new double[n*(restart+1)];

  //gmres 
  int iter = 0, k = 0;
  while(iter<maxIter && sqrt(residual)>tol){
    iter++;

    double *y = new double[restart];

    //initialize H and Q matrices to zero
    for(int i=0;i<(restart+1)*restart;i++){H[i] = 0.0;}
    for(int i=0;i<n*(restart+1);i++){Q[i] = 0.0;}

    //calculate residual using initial x0 approximation
    double *x0 = new double[n];
    for(int i=0;i<n;i++){x0[i] = x[i];}
    matrixVectorProduct(row,col,A,x0,res,n);
    for(int i=0;i<n;i++){res[i] = b[i] - res[i];}

    //double rr = dotProduct(res,res,n);
    double rr = 0.0;
    for(int i=0;i<n;i++){rr += res[i]*res[i];}
    for(int i=0;i<n;i++){q[i] = res[i]/sqrt(rr);}
    for(int i=0;i<n;i++){Q[i] = q[i];}

    //restart
    k = 0;
    while(k<restart){
      k++;
    
      //Arnoldi iteration
      matrixVectorProduct(row,col,A,q,v,n);
      for(int i=0;i<k;i++){
        //H[i+(k-1)*(restart+1)] = dotProduct(&Q[i*n],v,n);
        for(int j=0;j<n;j++){
          H[i+(k-1)*(restart+1)] += Q[i*n+j]*v[j];
        }
        for(int j=0;j<n;j++){
          v[j] = v[j] - H[i+(k-1)*(restart+1)]*Q[j+i*n];
        }
      }

      //double vv = dotProduct(v,v,n);
      double vv = 0.0;
      for(int i=0;i<n;i++){vv += v[i]*v[i];}
      
      H[k+(k-1)*(restart+1)] = sqrt(vv);
      if(sqrt(vv)<10e-12){
        for(int i=0;i<n;i++){q[i] = 0.0;}
      }
      else{
        for(int i=0;i<n;i++){q[i] = v[i]/H[k+(k-1)*(restart+1)];}
      }
      for(int i=0;i<n;i++){Q[k*n+i] = q[i];}

      //print H
      //std::cout<<""<<std::endl;
      //for(int i=0;i<(restart+1)*restart;i++){
      //  std::cout<<H[i]<<" ";
      //}
      //std::cout<<""<<std::endl;

      //solve least squares problem h(1:iter+1,1:iter)*y=sqrt(b'*b)*eye(iter+1,1)
      //since H is hessenberg, use givens rotations
      double *R = new double[(k+1)*k];
      for(int i=0;i<k;i++){
        for(int j=0;j<k+1;j++){
          R[j+(k+1)*i] = H[j+(restart+1)*i];
        }
      }
      for(int i=0;i<k+1;i++){x[i] = 0.0;}
      x[0] = sqrt(rr);

      for(int i=0;i<k;i++){
        //Givens 2 by 2 rotation matrix: 
        //  G = [g11,g12
        //       g21,g22]
        double g11=0.0,g12=0.0,g21=0.0,g22=0.0;
        double xi = R[i+i*(k+1)], xj = R[i+1+i*(k+1)];
        double c,s;
        if(xi<10e-16 && xj<10e-16){
          c = 0.0; s = 0.0;
        }
        else{
          c = xi/sqrt(xi*xi+xj*xj);
          s = -xj/sqrt(xi*xi+xj*xj);
        }
        g11 = c; g22 = c; g12 = -s; g21 = s;
        int ind1 = i, ind2 = i+1;
        for(int j=0;j<k;j++){                                                    
          double R1 = R[ind1+(k+1)*j], R2 = R[ind2+(k+1)*j];                           
          R[ind1+(k+1)*j] = g11*R1 + g12*R2;   
          R[ind2+(k+1)*j] = g21*R1 + g22*R2;                              
        }
        double x1 = x[ind1], x2 = x[ind2];
        x[ind1] = g11*x1 + g12*x2;
        x[ind2] = g21*x1 + g22*x2;
      }

      //print R
      std::cout<<""<<std::endl;
      for(int i=0;i<(k+1)*k;i++){
        std::cout<<R[i]<<" ";
      }
      std::cout<<""<<std::endl;
  
      //backward solve
      for(int i=k-1;i>-1;i--){
        if(i+1>k-1){
        y[i] = x[i]/R[i+(k+1)*i];
        }
        else{
          y[i] = x[i];
          for(int j=i+1;j<k;j++){
            y[i] -=R[i+(k+1)*j]*y[j];
          }
          y[i] = y[i]/R[i+(k+1)*i];
        }
      }
   
      //print y
      //std::cout<<""<<std::endl;
      //for(int i=0;i<n;i++){
      //  std::cout<<y[i]<<" ";
      //}
      //std::cout<<""<<std::endl;
 
      delete[] R;
    }
    
    //x = Q[:,1:k]*y
    for(int i=0;i<n;i++){
      double qy = 0.0;
      for(int j=0;j<k;j++){
        qy += Q[i+n*j]*y[j];
      }
      x[i] = x0[i] + qy;
      //x[i] = qy;
    }

    delete[] y;

    residual = 0.0;
    matrixVectorProduct(row,col,A,x,res,n);
    for(int i=0;i<n;i++){residual += (b[i]-res[i])*(b[i]-res[i]);}

    std::cout<<"residual: "<<residual<<std::endl;
  }

  delete[] res;
  delete[] q;
  delete[] v;
  delete[] H;
  delete[] Q;

  return iter;
}



//----------------------------------------------------------------------------
// find diagonal of matrix solver
//----------------------------------------------------------------------------
void IterSolvers::diagonalPreconditioner(int row[], int col[], double A[], double md[], int n)
{
  for(int j=0;j<n;j++){
    int i = 0;
    for(i=row[j];i<row[j+1];i++)
      if(col[i]==j) break;
    md[j] = 1.0/A[i];
    if(A[i]==0){std::cout<<"i: "<<i<<std::endl;}
  }
}



//----------------------------------------------------------------------------
// sparse matrix vector product
//----------------------------------------------------------------------------
void IterSolvers::matrixVectorProduct(int row[], int col[], double A[], double x[], double y[], int n)
{
  for(int j=0;j<n;j++){
    double s = 0.0;
    for(int i=row[j];i<row[j+1];i++)
      s += A[i]*x[col[i]];
    y[j] = s;
  }
}  

