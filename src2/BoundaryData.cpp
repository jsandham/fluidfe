//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************


#include "BoundaryData.h"


//----------------------------------------------------------------------------
// constructor for boundary data class
//----------------------------------------------------------------------------
BoundaryData::BoundaryData(int n)
{
  numGrps = n;
  numDirichGrps = 0;
  numFluxGrps = 0;

  dirichGrps = new int[numGrps];
  fluxGrps = new int[numGrps];

  dirichVals = new double[numGrps];
  fluxVals = new double[numGrps];

  for(int i=0;i<numGrps;i++){
    dirichGrps[i] = -1;
    fluxGrps[i] = -1;
  } 

  for(int i=0;i<numGrps;i++){
    dirichVals[i] = 0.0;
    fluxVals[i] = 0.0;
  }
}


//----------------------------------------------------------------------------
// destructor for boundary data class
//----------------------------------------------------------------------------
BoundaryData::~BoundaryData()
{
  delete[] dirichGrps;
  delete[] fluxGrps;
  delete[] dirichVals;
  delete[] fluxVals;
}



//----------------------------------------------------------------------------
// get numGrps, numDirichGrps, and numFluxGrps
//----------------------------------------------------------------------------
int BoundaryData::getNumGrps()
{
  return numGrps;
}


int BoundaryData::getNumDirichletGrps()
{
  return numDirichGrps;
}


int BoundaryData::getNumFluxGrps()
{
  return numFluxGrps;
}



//----------------------------------------------------------------------------
// add a dirichlet/flux group 
//----------------------------------------------------------------------------
void BoundaryData::addDirichlet(int grp, double val)
{
  if(numDirichGrps<numGrps){
    dirichGrps[numDirichGrps] = grp;
    dirichVals[numDirichGrps] = val;
    numDirichGrps++;
  }
}
    

void BoundaryData::addFlux(int grp, double val)
{
  if(numFluxGrps<numGrps){
    fluxGrps[numFluxGrps] = grp;
    fluxVals[numFluxGrps] = val;
    numFluxGrps++;
  }
}



//----------------------------------------------------------------------------
// remove a dirichlet/flux group
//----------------------------------------------------------------------------
void BoundaryData::removeDirichlet(int grp)
{
  for(int i=0;i<numDirichGrps;i++){
    if(dirichGrps[i]==grp){
      dirichGrps[i] = -1;
      break;
    }
  }

  int ind = 0;
  for(int i=0;i<numDirichGrps;i++){
    if(dirichGrps[i]!=-1){
      dirichGrps[ind] = dirichGrps[i];
      dirichVals[ind] = dirichVals[i];
      ind++;
    }
  }
  numDirichGrps--;
  for(int i=numDirichGrps;i<numGrps;i++){
    dirichGrps[i] = -1;
    dirichVals[i] = 0.0;
  }
}


void BoundaryData::removeFlux(int grp)
{
  for(int i=0;i<numFluxGrps;i++){
    if(fluxGrps[i]==grp){
      fluxGrps[i] = -1;
    }
  }

  int ind = 0;
  for(int i=0;i<numFluxGrps;i++){
    if(fluxGrps[i]!=-1){
      fluxGrps[ind] = fluxGrps[i];
      fluxVals[ind] = fluxVals[i];
      ind++;
    }
  }
  numFluxGrps--;
  for(int i=numFluxGrps;i<numGrps;i++){
    fluxGrps[i] = -1;
    fluxVals[i] = 0.0;
  }
}



//----------------------------------------------------------------------------
// determine whether an input group is a dirichlet/flux group
//----------------------------------------------------------------------------
bool BoundaryData::isDirichlet(int grp)
{
  bool flag = false;
  for(int i=0;i<numDirichGrps;i++){
    if(dirichGrps[i]==grp){
      flag = true;
      break;
    }
  }
  return flag;
}



bool BoundaryData::isFlux(int grp)
{
  bool flag = false;
  for(int i=0;i<numFluxGrps;i++){
    if(fluxGrps[i]==grp){
      flag = true;
      break;
    }
  }
  return flag;
}



//----------------------------------------------------------------------------
// return dirichlet/flux value corresponding to input group
//----------------------------------------------------------------------------
double BoundaryData::getDirichletValue(int grp)
{
  double val = 0.0;
  for(int i=0;i<numDirichGrps;i++){
    if(dirichGrps[i]==grp){
      val = dirichVals[i];
      break;
    }
  }
  return val;
}


double BoundaryData::getFluxValue(int grp)
{
  double val = 0.0;
  for(int i=0;i<numFluxGrps;i++){
    if(fluxGrps[i]==grp){
      val = fluxVals[i];
      break;
    }
  }
  return val;
}