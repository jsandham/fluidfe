//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef POSTPROCESSING_H
#define POSTPROCESSING_H
#include<iostream>
#include<string>
#include"Mesh.h"

using namespace std;


class PostProcessing
{
  public:
    PostProcessing();
    ~PostProcessing();

    static double minimum(const double *vec, Mesh *mesh);
    static double maximum(const double *vec, Mesh *mesh);
    static double average(const double *vec, Mesh *mesh);

    static void write_results_gmsh_pos(const double *vec, string filename, Mesh *mesh);
    static void write_results_gmsh_msh(const double *vec, string filename, Mesh *mesh);
    static void write_results_matlab(const double *vec, string filename, Mesh *mesh);
};



#endif
