//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef ETREE_H
#define ETREE_H
#include"Element.h"



class Node
{
  public:
    int nodeNum,numChildren;
    Element *elem;  //the node data
    Node *children; //node children

    Node();
    Node(Element *e, int nNum, int nChild);
    ~Node();

    void setNode(Element *e, int nNum, int nChild);
    //void setChildren(Element *e, int *nNum, int *nChild);
};



class ETree
{
  public:
    static int totalNumNodes;  //total number of Nodes (elements) across all trees in mesh 
    static int maxNodeNum;     //maximum node number across all trees in mesh

    ETree();
    //ETree(Element *e, );
    ~ETree();

  private:
    Node *root;

    void refine_element(Node *n);    //adds left and right children to node n
    void unrefine_element(Node *n);  //removes left and right children of node n
};




#endif
