//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#include<fstream>
#include"Mesh.h"
#include"PostProcessing.h"


//********************************************************************************
//
//  Post Processing
//
//********************************************************************************



//----------------------------------------------------------------------------
// constructor for Post_Processing
//----------------------------------------------------------------------------
PostProcessing::PostProcessing()
{
  
}



//----------------------------------------------------------------------------
// destructor for Post_Processing
//----------------------------------------------------------------------------
PostProcessing::~PostProcessing()
{
  
}



//----------------------------------------------------------------------
// minimum, maximum, and average values of a vector
//----------------------------------------------------------------------
double minimum(const double *vec, Mesh *mesh)
{
  double min = vec[0];
  for(int i=1;i<mesh->N;i++){
    if(vec[i]<min){
      min = vec[i];
    }
  }

  return min;
}


double maximum(const double *vec, Mesh *mesh)
{
  double max = vec[0];
  for(int i=1;i<mesh->N;i++){
    if(vec[i]>max){
      max = vec[i];
    }
  }

  return max;
}


double average(const double *vec, Mesh *mesh)
{
  double avg = vec[0];
  for(int i=1;i<mesh->N;i++){
    avg += vec[i];
  }

  return avg/mesh->N;
}




//----------------------------------------------------------------------
// write results to output file (.pos format)
//----------------------------------------------------------------------
void PostProcessing::write_results_gmsh_pos(const double *vec, string filename, Mesh *mesh)
{
  std::ofstream myfile;
  myfile.open(filename.c_str());

  char typ[3] = {' ',' ',' '};
  int npe = 0;

  switch(mesh->Type)
  {
    case 1:
      typ[0] = 'S'; typ[1] = 'L'; typ[2] = ' '; npe=2;
      break;
    case 2:
      typ[0] = 'S'; typ[1] = 'T'; typ[2] = ' '; npe=3;
      break;
    case 3:
      typ[0] = 'S'; typ[1] = 'Q'; typ[2] = ' '; npe=4;
      break;
    case 4:
      typ[0] = 'S'; typ[1] = 'S'; typ[2] = ' '; npe=4;
      break;
    case 8:
      typ[0] = 'S'; typ[1] = 'L'; typ[2] = '2'; npe=3;
      break;
    case 9:
      typ[0] = 'S'; typ[1] = 'T'; typ[2] = '2'; npe=6;
      break;
    case 10:
      typ[0] = 'S'; typ[1] = 'Q'; typ[2] = '2'; npe=8;
      break;
    case 11:
      typ[0] = 'S'; typ[1] = 'S'; typ[2] = '2'; npe=10;
      break;
  }

  myfile<<"View \"Temperature\" {\n";
  for(int i=0;i<mesh->Ne;i++){
    myfile<<typ[0]<<typ[1]<<typ[2]<<" (";
    for(int j=0;j<npe;j++){
      myfile<<mesh->xpoints[mesh->connect[i][j]-1]<<","
            <<mesh->ypoints[mesh->connect[i][j]-1]<<","
            <<mesh->zpoints[mesh->connect[i][j]-1];
      if(j<npe-1){
        myfile<<",";
      }
      else{
        myfile<<"){";
      }
    }
  
    for(int j=0;j<npe;j++){
      myfile<<vec[mesh->connect[i][j]-1];
      if(j<npe-1){
        myfile<<",";
      }
      else{
        myfile<<"};\n";
      }
    }
  }
  myfile<<"};\n";
  myfile.close();
}



//----------------------------------------------------------------------
// write results to output file (.msh format)
//----------------------------------------------------------------------
void PostProcessing::write_results_gmsh_msh(const double *vec, string filename, Mesh *mesh)
{
  std::ofstream myfile;
  myfile.open(filename.c_str());

  myfile<<"$MeshFormat"<<std::endl;
  myfile<<"2.2 0 8"<<std::endl;
  myfile<<"$EndMeshFormat"<<std::endl;

  myfile<<"$Nodes"<<std::endl;
  myfile<<mesh->N<<std::endl;
  for(int i=0;i<mesh->N;i++){
    myfile<<i+1<<" "<<mesh->xpoints[i]<<" "<<mesh->ypoints[i]<<" "<<mesh->zpoints[i]<<std::endl;
  }
  myfile<<"$EndNodes"<<std::endl;

  myfile<<"$Elements"<<std::endl;
  myfile<<mesh->Ne<<std::endl;
  for(int i=0;i<mesh->Ne;i++){
    myfile<<i+1<<" "<<mesh->Type<<" "<<0<<" ";
    for(int j=0;j<mesh->Npe;j++){
      myfile<<mesh->connect[i][j]<<" ";
    }
    myfile<<""<<std::endl;
  }
  myfile<<"$EndElements"<<std::endl;

  myfile<<"$NodeData"<<std::endl;
  myfile<<1<<std::endl;                 //number of string tags
  myfile<<"A scalar view"<<std::endl;   //string tag
  myfile<<1<<std::endl;                 //number of real tags
  myfile<<1.0<<std::endl;               //real tag
  myfile<<3<<std::endl;                 //number of int tags
  myfile<<0<<std::endl;
  myfile<<1<<std::endl;
  myfile<<mesh->N<<std::endl;
  for(int i=0;i<mesh->N;i++){
    myfile<<i+1<<" "<<vec[i]<<std::endl;
  }
  myfile<<"$EndNodeData"<<std::endl;

  myfile.close();
}



//----------------------------------------------------------------------
// write results to output file (matlab format)
//----------------------------------------------------------------------
void PostProcessing::write_results_matlab(const double *vec, string filename, Mesh *mesh)
{
  std::ofstream myfile;
  myfile.open(filename.c_str());

  for(int i=0;i<mesh->N;i++){
    myfile<<vec[i]<<std::endl;
  }
}




