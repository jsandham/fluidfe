//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef POSTPROCESSING_H
#define POSTPROCESSING_H
#include"Mesh.h"



class Post_Processing
{
  public:
    Post_Processing();
    ~Post_Processing();

    static double minimum(const double *vec, Mesh *mesh);
    static double maximum(const double *vec, Mesh *mesh);
    static double average(const double *vec, Mesh *mesh);

    static void write_results_gmsh_pos(const double *vec, const char *filename, const char *label, Mesh *mesh);
    static void write_results_gmsh_msh(const double *vec, const char *filename, const char *label, Mesh *mesh);
    static void write_results_matlab(const double *vec, const char *filename, const char *label, Mesh *mesh);
};



#endif
