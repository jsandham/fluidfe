#ifndef FECONST_H
#define FECONST_H

const int MAX_NNZ = 130;
const int MAX_ITER = 50000;
const double EPSILON = 1.0e-22;
const double LARGE_NUM = 1.0e64;

static const int MAX_E = 3*10;  //maximum number of nodes per element
static const int MAX_S = 10;    //maximum number of shape functions per element
#endif
