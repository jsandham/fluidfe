//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include<QMainWindow>
#include<QDialog>
#include<QtGui>
#include "GLWidget.h"
#include "RunWizard.h"
#include "Mesh.h"


class QAction;
class QMenu;
class QMenuBar;
class QLabel;
class QLineEdit;



//----------------------------------------------------------------------------
// constructor for main window
//----------------------------------------------------------------------------
class MainWindow : public QMainWindow
{
  Q_OBJECT;

  public:
    Mesh *input_mesh;

    MainWindow();
    ~MainWindow();

  private:
    GLWidget *openglWindow;  
    RunWizard *runWindow;  

    QMenuBar *menuBar;
    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *helpMenu;
    QAction *clearAct;
    QAction *openAct;
    QAction *runAct;
    QAction *exitAct;
    QAction *aboutAct;
    QAction *aboutQtAct;

  private slots:
    void clear();
    void open();
    void run();
    void about();
    void aboutQt();
    void loadFile(const char *fileName);

  private:
    void createToolBars();
    void createStatusBar();
    void createActions();
    void createMenus();
    void keyPressEvent(QKeyEvent* e);
};


#endif
