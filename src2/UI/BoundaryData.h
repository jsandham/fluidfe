//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef BOUNDARYDATA_H
#define BOUNDARYDATA_H


//----------------------------------------------------------------------------
// constructor for boundary data class
//----------------------------------------------------------------------------
class BoundaryData
{
  public:
    int numOfGrps;
    int *groups;
    int *types;
    double *values;

  public:
    BoundaryData(int n);
    ~BoundaryData();
};


#endif
