//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#include<iostream>
#include<fstream>
#include"Element.h"
#include"Solver.h"
#include"Mesh.h"
#include"FeModel.h"
#include"FeConst.h"
#include"math.h"
#include"AMG.h"


//********************************************************************************
//
//  Solvers for heat and fluid equations
//
//********************************************************************************

  


//----------------------------------------------------------------------------
// Constructor for Solver
//----------------------------------------------------------------------------
Solver::Solver(Mesh mesh, FeModel fem) : mesh(mesh), fem(fem)
{
  
}




//----------------------------------------------------------------------------
// preconditioned conjugte gradient solver
//----------------------------------------------------------------------------
int Solver::pcg(int row[], int col[], double A[], double x[], double b[], int n)
{
  double *r = new double[n];
  double *w = new double[n];
  double *p = new double[n];
  double *md = new double[n];

  //find diagonal of A matrix
  diagonalPreconditioner(row,col,A,md,n);

  //calculate initial x
  for(int i=0;i<n;i++){
    x[i] = b[i]*md[i];
  }

  //r = b-A*x and initial error
  matrixVectorProduct(row,col,A,x,r,n);
  double check = 0;
  for(int i=0;i<n;i++){
    r[i] = b[i] - r[i];
    check += r[i];
  }
  if(fabs(check)<EPSILON){return 1;}
  double gamma0 = 1;
  double gammai = 1;
  int iter;
  //double omega=1.1;
  //double omega2=2-omega;
  for(iter=0; iter<MAX_ITER; iter++){
    //w = (M^-1)*r
    for(int i=0;i<n;i++){
      w[i] = md[i]*r[i];
    }

    //gam = (r,w)
    double gammai1 = gammai;
    gammai = 0;
    for(int i=0;i<n;i++){
      gammai += r[i]*w[i];
    }
    if(iter==0){
      gamma0 = gammai;
      for(int i=0;i<n;i++)
        p[i] = w[i];
    }
    else{
      double rg = gammai/gammai1;
      for(int i=0;i<n;i++)
        p[i] = w[i] + rg*p[i];
    }

    //w = A*p
    matrixVectorProduct(row,col,A,p,w,n);
    double beta = 0;
    for(int i=0;i<n;i++)
      beta += p[i]*w[i];
    double alpha = gammai/beta;

    //update x and r, calculate error
    for(int i=0;i<n;i++){
      x[i] += alpha*p[i];
      r[i] -= alpha*w[i];
    }
    double err = sqrt(gammai/gamma0);
    std::cout<<"error: "<<err<<std::endl;
    if(err<EPSILON)
      break;
    }
 
  delete[] r;
  delete[] w;
  delete[] p;
  delete[] md;

  return iter;
}



//----------------------------------------------------------------------------
// find diagonal of matrix solver
//----------------------------------------------------------------------------
void Solver::diagonalPreconditioner(int row[], int col[], double A[], double md[], int n)
{
  for(int j=0;j<n;j++){
    int i;
    for(i=row[j];i<row[j+1];i++)
      if(col[i]==j) break;
    md[j] = 1.0/A[i];
    if(A[i]==0){std::cout<<"i: "<<i<<std::endl;}
  }
}



//----------------------------------------------------------------------------
// sparse matrix vector product
//----------------------------------------------------------------------------
void Solver::matrixVectorProduct(int row[], int col[], double A[], double x[], double y[], int n)
{
  for(int j=0;j<n;j++){
    double s = 0;
    for(int i=row[j];i<row[j+1];i++)
      s += A[i]*x[col[i]];
    y[j] = s;
  }
}



//----------------------------------------------------------------------------
// assemble global sparse matrix
//----------------------------------------------------------------------------
void Solver::assembleGSM(int *row, int *col, double *A, int type, int n)
{
  int r=0,c=0;
  double v=0;

  for(int k=0;k<mesh.Ne;k++){
    Element e(mesh.Type);
    for(int l=0;l<e.Npe;l++){
      e.nodes[l] = mesh.connect[k][l];
      e.xpts[l] = mesh.xpoints[e.nodes[l]-1];
      e.ypts[l] = mesh.ypoints[e.nodes[l]-1];
      e.zpts[l] = mesh.zpoints[e.nodes[l]-1];
    }

    switch(type)
    {
    case 0:
      e.stiffnessMatrix();
      break;
    case 1:
      e.massMatrix();
      break;
    case 2:
      e.advecXMatrix();
      break;
    case 3:
      e.advecYMatrix();
      break;
    case 4:
      e.advecZMatrix();
      break;
    case 5:
      e.pstiffnessMatrix();
      break;
    }

    //for(int i=0;i<e.Npe;i++){
    //  for(int j=0;j<e.Npe;j++){
    //    std::cout<<e.kmat[i][j]<<"  ";   
    //  } 
    //  std::cout<<" "<<std::endl;
    //}
    //std::cout<<" "<<std::endl;

    //update col, A arrays
    for(int i=0;i<e.Npe;i++){
      for(int j=0;j<e.Npe;j++){
        r = e.nodes[i];
        c = e.nodes[j];
        v = e.emat[i][j];
        for(int p=MAX_NNZ*r-MAX_NNZ;p<MAX_NNZ*r;p++){
          if(col[p]==-1){
            col[p] = c-1;
            A[p] = v;
            break;
          }
          else if(col[p]==c-1){
            A[p] += v;
            break;
          }
        }
      }
    }
  }

  //update row array
  int jj = 0;
  for(int i=1;i<n+1;i++){
    for(int j=0;j<MAX_NNZ;j++){
      if(col[i*MAX_NNZ-MAX_NNZ+j]==-1){jj=j; break;}
    }
    row[i] = row[i-1] + jj;
  }

  //sort (insertion) col and A arrays
  for(int p=0;p<n;p++){
    for(int i=0;i<row[p+1]-row[p];i++){
      int entry = col[i+p*MAX_NNZ];
      double entryA = A[i+p*MAX_NNZ];
      int index = i+p*MAX_NNZ;
      for(int j=i-1;j>=0;j--){
        if(entry<col[j+p*MAX_NNZ]){
          int a = col[j+p*MAX_NNZ];
          double b = A[j+p*MAX_NNZ];
          col[j+p*MAX_NNZ] = entry;
          A[j+p*MAX_NNZ] = entryA;
          col[index] = a;
          A[index] = b;
          index = j+p*MAX_NNZ;
        }
      }
    }
  }

  //apply dirichlet boundary conditions
  //for(int i=0;i<mesh.Ne_b;i++){
  //  int grp = mesh.connect_b[i][0];
  //  for(int j=0;j<Ng;j++){
  //    if(grp==groups[j] && types[j]==1){
  //      int l=mesh.connect_b[i][1]-1;
  //      //for(int k=0;k<neq*MAX_NNZ;k++){
  //      //  if(col[k]==l){col[k]=-1;}
  //      //}
  //      for(int k=MAX_NNZ*l;k<MAX_NNZ*l+MAX_NNZ;k++){
  //        col[k] = -1;
  //      }
  //      col[MAX_NNZ*l] = l;
  //      A[MAX_NNZ*l] = 1.0;
  //    }
  //  }
  //}

  //update row array
  //jj = 0;
  //for(int i=1;i<n+1;i++){
  //  for(int j=0;j<MAX_NNZ;j++){
  //    if(col[i*MAX_NNZ-MAX_NNZ+j]!=-1){jj++;}
  //  }
  //  row[i] = jj;
  //}

  //compress col and A arrays
  int index=0;
  for(int i=0;i<n*MAX_NNZ;i++){
    if(col[i]==-1)
      continue;
    col[index] = col[i];
    A[index] = A[i];
    index++;
  }

  //set unused parts of arrays to 0 or -1
  for(int i=index;i<n*MAX_NNZ;i++){
    col[i] = -1;
    A[i] = 0.0;
  }
}



//----------------------------------------------------------------------------
// Assemble Global Lumped Matrix
//----------------------------------------------------------------------------
void Solver::assembleGLM(int *row, double *A, double *L, int n)
{
  for(int i=0;i<n;i++){
    for(int j=row[i];j<row[i+1];j++){
      L[i] += A[j];
    }
  }
}



//----------------------------------------------------------------------------
// Assemble Global RHS Vector
//----------------------------------------------------------------------------
void Solver::assembleGRHSV(int *row, int *col, double *A, double *rhs, double *vec, int n)
{
  for(int i=0;i<n;i++){rhs[i] = 0.0;}

  //set RHS = M*T^n
  for(int i=0;i<n;i++){
    for(int j=row[i];j<row[i+1];j++){
      rhs[i] += A[j]*vec[col[j]];
    }
  }

  //add internal heat generation vector Q
  //for(int i=0;i<n;i++){rhs[i] += f[i];}
}




//----------------------------------------------------------------------------
// Global Force Vector f
//----------------------------------------------------------------------------
void Solver::assembleGFV(double *fvec, int n)
{
  for(int i=0;i<n;i++){fvec[i] = 0.0;}

  //compute internal heat generation vector fvec=Q
  for(int k=0;k<mesh.Ne;k++){
    Element e(mesh.Type);
    for(int l=0;l<e.Npe;l++){
      e.nodes[l] = mesh.connect[k][l];
      e.xpts[l] = mesh.xpoints[e.nodes[l]-1];
      e.ypts[l] = mesh.ypoints[e.nodes[l]-1];
      e.zpts[l] = mesh.zpoints[e.nodes[l]-1];
    }

    e.elementVector();
    for(int i=0;i<e.Npe;i++){
      fvec[e.nodes[i]-1] += fem.get_Q()*e.evec[i];
    }
  }
}




//----------------------------------------------------------------------------
// apply dirichlet boundary conditions to global sparse matrix
//----------------------------------------------------------------------------
void Solver::dirichletGSM(int *row, int *col, double *A, int *groups, int *types, int Ng)
{
  //apply dirichlet BC to A using method of large numbers
  for(int i=0;i<mesh.Ne_b;i++){
    int grp = mesh.connect_b[i][0];
    for(int j=0;j<Ng;j++){
      if(grp==groups[j] && types[j]==1){
        int l=mesh.connect_b[i][1]-1;
        for(int p=row[l];p<row[l+1];p++){
          if(col[p]==l){A[p] = LARGE_NUM;}
        }
        break;
      }
    }
  }
}



//----------------------------------------------------------------------------
// apply dirichlet boundary conditions to global right hand side vector
//----------------------------------------------------------------------------
void Solver::dirichletGRHSV(double *rhs, int *groups, int *types, double *values, int Ng)
{
  //add boundary conditions to global RHS vector
  for(int i=0;i<mesh.Ne_b;i++){
    int grp = mesh.connect_b[i][0];
    for(int j=0;j<Ng;j++){

      //surface heat flux boundary condition
      if(grp==groups[j] && types[j]==2){
        Element e(mesh.Type_b);
        for(int k=0;k<e.Npe;k++){
          e.nodes[k] = mesh.connect_b[i][k+1];
          e.xpts[k] = mesh.xpoints[e.nodes[k]-1];
          e.ypts[k] = mesh.ypoints[e.nodes[k]-1];
          e.zpts[k] = mesh.zpoints[e.nodes[k]-1];
        }

        e.elementVector();
        for(int k=0;k<e.Npe;k++){
          rhs[e.nodes[k]-1] += values[j]*e.evec[k];
        }
      }

      //specified temperature boundary condition
      if(grp==groups[j] && types[j]==1){
        int l=mesh.connect_b[i][1]-1;
        rhs[l] = values[j]*LARGE_NUM;
        break;
      }
    }
  }
}














//********************************************************************************
//
//  Heat equation solver
//
//********************************************************************************



//----------------------------------------------------------------------------
// Constructor for heat equation solver
//----------------------------------------------------------------------------
SolverHeat::SolverHeat(Mesh mesh, FeModel fem) : Solver(mesh,fem)
{
  neq = mesh.N;
  krow = new int[neq+1];
  kcol = new int[neq*MAX_NNZ];
  K = new double[neq*MAX_NNZ];

  mrow = new int[neq+1];
  mcol = new int[neq*MAX_NNZ];
  M = new double[neq*MAX_NNZ];

  temp = new double[neq];
  b = new double[neq];
  f = new double[neq];

  for(int i=0;i<neq+1;i++){
    krow[i] = 0;
    mrow[i] = 0;
  }
  for(int i=0;i<neq*MAX_NNZ;i++){
    kcol[i] = -1;
    mcol[i] = -1;
    K[i] = 0.0;
    M[i] = 0.0;
  }
  for(int i=0;i<neq;i++){
    temp[i] = 0.0;
    b[i] = 0.0;
    f[i] = 0.0;
  }
}




//----------------------------------------------------------------------------
// return pointer to temperature array for heat solver
//----------------------------------------------------------------------------
double* SolverHeat::getTemperature()
{
  return temp;
}




//-------------------------------------------------------------------
// Diffusion of temperature
//-------------------------------------------------------------------
void SolverHeat::diffusion()
{
  int iter = 0;

  //assemble global RHS vector
  assembleGRHSV(mrow,mcol,M,b,temp,neq);

  //apply boundary conditions to RHS vector
  dirichletGRHSV(b,(fem.bdata)->groups,(fem.bdata)->types,(fem.bdata)->values,mesh.Ng);

  //solve using preconditioned conjugate gradient
  iter = pcg(krow,kcol,K,temp,b,neq);
}





//-------------------------------------------------------------------
// Solve heat equation
//-------------------------------------------------------------------
int SolverHeat::solve()
{
  int iter = 0;
  
  //assemble global force vector
  assembleGFV(f,neq);
  
  if(fem.get_time()){    
    //assemble global stiffness and global mass matrices
    assembleGSM(krow,kcol,K,0,neq);
    assembleGSM(mrow,mcol,M,1,neq);

    //form K = M+dt*K
    for(int i=0;i<krow[neq];i++){K[i] = M[i]+0.02*fem.get_dt()*K[i];}

    //enforce dirichlet boundary conditions
    dirichletGSM(krow,kcol,K,(fem.bdata)->groups,(fem.bdata)->types,mesh.Ng);

    //time loop
    for(int i=0;i<fem.get_steps();i++){
      diffusion();
    }
  }
  else{
    //assemble global stiffness
    assembleGSM(krow,kcol,K,0,neq);

    //enforce dirichlet boundary conditions
    dirichletGSM(krow,kcol,K,(fem.bdata)->groups,(fem.bdata)->types,mesh.Ng);

    diffusion();
  }

  //write solution vector to output file
  writeTemperature();

  return iter;
}



//----------------------------------------------------------------------------
// destructor for heat solver
//----------------------------------------------------------------------------
SolverHeat::~SolverHeat()
{
  delete [] krow;
  delete [] kcol;
  delete [] K;

  delete [] mrow;
  delete [] mcol;
  delete [] M;

  delete [] temp;
  delete [] b;
  delete [] f;
}














//********************************************************************************
//
//  Fluid equations solver
//
//********************************************************************************



//----------------------------------------------------------------------------
// Constructor for fluid (momentum and continuity equation) Solver
//----------------------------------------------------------------------------
SolverFluid::SolverFluid(Mesh mesh, FeModel fem) : Solver(mesh,fem)
{
  neq = mesh.N;
  krow = new int[neq+1];
  kcol = new int[neq*MAX_NNZ];
  K = new double[neq*MAX_NNZ];

  mrow = new int[neq+1];
  mcol = new int[neq*MAX_NNZ];
  M = new double[neq*MAX_NNZ];

  hxrow = new int[neq+1];
  hxcol = new int[neq*MAX_NNZ];
  Hx = new double[neq*MAX_NNZ];

  hyrow = new int[neq+1];
  hycol = new int[neq*MAX_NNZ];
  Hy = new double[neq*MAX_NNZ];

  hzrow = new int[neq+1];
  hzcol = new int[neq*MAX_NNZ];
  Hz = new double[neq*MAX_NNZ];

  kprow = new int[neq+1];
  kpcol = new int[neq*MAX_NNZ];
  Kp = new double[neq*MAX_NNZ];

  LM = new double[neq];

  u_vel = new double[neq];
  v_vel = new double[neq];
  w_vel = new double[neq];
  pres = new double[neq];

  u_old = new double[neq];
  v_old = new double[neq];
  w_old = new double[neq];
  pres_old = new double[neq];
 
  b_u = new double[neq];
  b_v = new double[neq];
  b_w = new double[neq];

  for(int i=0;i<neq+1;i++){
    krow[i] = 0;
    mrow[i] = 0;
    hxrow[i] = 0;
    hyrow[i] = 0;
    hzrow[i] = 0;
    kprow[i] = 0;
  }
  for(int i=0;i<neq*MAX_NNZ;i++){
    kcol[i] = -1;
    mcol[i] = -1;
    hxcol[i] = -1;
    hycol[i] = -1;
    hzcol[i] = -1;
    kpcol[i] = -1;
    K[i] = 0.0;
    M[i] = 0.0;
    Hx[i] = 0.0;
    Hy[i] = 0.0;
    Hz[i] = 0.0;
    Kp[i] = 0.0;
  }
  for(int i=0;i<neq;i++){
    LM[i] = 0.0;
    u_vel[i] = 0.0;
    v_vel[i] = 0.0;
    w_vel[i] = 0.0;
    div[i] = 0.0;
    pres[i] = 0.0;
    u_old[i] = 0.0;
    v_old[i] = 0.0;
    w_old[i] = 0.0;
    pres_old[i] = 0.0;
    b_u[i] = 0.0;
    b_v[i] = 0.0;
    b_w[i] = 0.0;
  }
}



//----------------------------------------------------------------------------
// return pointer to u, v, w velocities and pressure for fluid solver
//----------------------------------------------------------------------------
double* SolverFluid::getUVelocity()
{
  return u_vel;
}


double* SolverFluid::getVVelocity()
{
  return v_vel;
}


double* SolverFluid::getWVelocity()
{
  return w_vel;
}


double* SolverFluid::getPressure()
{
  return pres;
}




//----------------------------------------------------------------------------
// nonlinear advection f function
//----------------------------------------------------------------------------
void SolverFluid::fadvec(double *u_in, double *v_in, double *fu_out, double *fv_out)
{
  //update u velocity for nonlinear advection
  for(int i=0;i<neq;i++){
    for(int j=hxrow[i];j<hxrow[i+1];j++){
      //fu_out[i] -= Hx[j]*u_in[hxcol[j]]*u_in[hxcol[j]];
      fu_out[i] -= 2*Hx[j]*u_in[hxcol[j]];
    }
  }

  for(int i=0;i<neq;i++){
    for(int j=hyrow[i];j<hyrow[i+1];j++){
      //fu_out[i] -= Hy[j]*u_in[hycol[j]]*v_in[hycol[j]];
      //fu_out[i] -= 3*Hy[j]*v_in[hycol[j]];
    }
  }

  //update v velocity for nonlinear advection
  for(int i=0;i<neq;i++){
    for(int j=hxrow[i];j<hxrow[i+1];j++){
      //fv_out[i] -= Hx[j]*v_in[hxcol[j]]*u_in[hxcol[j]];
      //fv_out[i] -= 4*Hx[j]*u_in[hxcol[j]];
    }
  }

  for(int i=0;i<neq;i++){
    for(int j=hyrow[i];j<hyrow[i+1];j++){
      //fv_out[i] -= Hy[j]*v_in[hycol[j]]*v_in[hycol[j]];
      //fv_out[i] -= 4*Hy[j]*v_in[hycol[j]];
    }
  }
}



//----------------------------------------------------------------------------
// explicit rk1 timestepping
//----------------------------------------------------------------------------
void SolverFluid::rk1()
{
  int iter = 0;
  double *k1_u = new double[neq];
  double *k1_v = new double[neq];

  for(int i=0;i<neq;i++){
    k1_u[i] = 0.0;
    k1_v[i] = 0.0;
  }

  fadvec(u_old,v_old,k1_u,k1_v);
  
  iter = pcg(mrow,mcol,M,u_vel,k1_u,neq);
  iter = pcg(mrow,mcol,M,v_vel,k1_v,neq);

  //update velocities 
  for(int i=0;i<neq;i++){
    u_vel[i] = u_old[i] + fem.get_dt()*u_vel[i];
    v_vel[i] = v_old[i] + fem.get_dt()*v_vel[i];
  }

  delete [] k1_u;
  delete [] k1_v;
}



//----------------------------------------------------------------------------
// explicit rk2 timestepping
//----------------------------------------------------------------------------
void SolverFluid::rk2()
{
  double *k1_u = new double[neq];
  double *k2_u = new double[neq];
  double *k1_v = new double[neq];
  double *k2_v = new double[neq];

  for(int i=0;i<neq;i++){
    k1_u[i] = 0.0;
    k1_v[i] = 0.0;
    k2_u[i] = 0.0;
    k2_v[i] = 0.0;
  }

  fadvec(u_old,v_old,k1_u,k1_v);

  //update velocities 
  for(int i=0;i<neq;i++){
    u_vel[i] = u_old[i] + fem.get_dt()*k1_u[i]/LM[i];
    v_vel[i] = v_old[i] + fem.get_dt()*k1_v[i]/LM[i];
  }

  fadvec(u_vel,v_vel,k2_u,k2_v);
 
  for(int i=0;i<neq;i++){
    u_vel[i] = (1.0/2)*(k1_u[i]+k2_u[i])/LM[i];
    v_vel[i] = (1.0/2)*(k1_v[i]+k2_v[i])/LM[i];
  }

  //update velocities 
  for(int i=0;i<neq;i++){
    u_vel[i] = u_old[i] + fem.get_dt()*u_vel[i];
    v_vel[i] = v_old[i] + fem.get_dt()*v_vel[i];
  }

  delete [] k1_u;
  delete [] k2_u;
  delete [] k1_v;
  delete [] k2_v;
}



//----------------------------------------------------------------------------
// explicit rk4 timestepping
//----------------------------------------------------------------------------
void SolverFluid::rk4()
{
  double *k1_u = new double[neq];
  double *k2_u = new double[neq];
  double *k3_u = new double[neq];
  double *k4_u = new double[neq];
  double *k1_v = new double[neq];
  double *k2_v = new double[neq];
  double *k3_v = new double[neq];
  double *k4_v = new double[neq];

  for(int i=0;i<neq;i++){
    k1_u[i] = 0.0;
    k1_v[i] = 0.0;
    k2_u[i] = 0.0;
    k2_v[i] = 0.0;
    k3_u[i] = 0.0;
    k3_v[i] = 0.0;
    k4_u[i] = 0.0;
    k4_v[i] = 0.0;
  }

  fadvec(u_old,v_old,k1_u,k1_v);

  //update velocities 
  for(int i=0;i<neq;i++){
    u_vel[i] = u_old[i] + 0.5*fem.get_dt()*k1_u[i]/LM[i];
    v_vel[i] = v_old[i] + 0.5*fem.get_dt()*k1_v[i]/LM[i];
  }

  fadvec(u_vel,v_vel,k2_u,k2_v);

  //update velocities 
  for(int i=0;i<neq;i++){
    u_vel[i] = u_old[i] + 0.5*fem.get_dt()*k2_u[i]/LM[i];
    v_vel[i] = v_old[i] + 0.5*fem.get_dt()*k2_v[i]/LM[i];
  }

  fadvec(u_vel,v_vel,k3_u,k3_v);

  //update velocities 
  for(int i=0;i<neq;i++){
    u_vel[i] = u_old[i] + fem.get_dt()*k3_u[i]/LM[i];
    v_vel[i] = v_old[i] + fem.get_dt()*k3_v[i]/LM[i];
  }

  fadvec(u_old,v_old,k4_u,k4_v);

  for(int i=0;i<neq;i++){
    u_vel[i] = (1.0/6)*(k1_u[i]+2*k2_u[i]+2*k3_u[i]+k4_u[i])/LM[i];
    v_vel[i] = (1.0/6)*(k1_v[i]+2*k2_v[i]+2*k3_v[i]+k4_v[i])/LM[i];
  }

  //update velocities 
  for(int i=0;i<neq;i++){
    u_vel[i] = u_old[i] + fem.get_dt()*u_vel[i];
    v_vel[i] = v_old[i] + fem.get_dt()*v_vel[i];
  }

  delete [] k1_u;
  delete [] k2_u;
  delete [] k3_u;
  delete [] k4_u;
  delete [] k1_v;
  delete [] k2_v;
  delete [] k3_v;
  delete [] k4_v;
}




//----------------------------------------------------------------------------
// Nonlinear Advection of Global velocities
//----------------------------------------------------------------------------
void SolverFluid::advection()
{
  int iter= 0;
  for(int i=0;i<neq;i++){
    u_old[i] = u_vel[i];
    v_old[i] = v_vel[i];
    w_old[i] = w_vel[i];
  }

  rk1();
  //rk2();
  //rk4();
}




//----------------------------------------------------------------------------
// Diffusion of Global velocities
//----------------------------------------------------------------------------
void SolverFluid::diffusion()
{
  int iter = 0;
 
  //assemble global RHS vector
  assembleGRHSV(mrow,mcol,M,b_u,u_vel,neq);
  assembleGRHSV(mrow,mcol,M,b_v,v_vel,neq);
  assembleGRHSV(mrow,mcol,M,b_w,w_vel,neq);

  dirichletGRHSV(b_u,(fem.bdata)->groups,(fem.bdata)->types,(fem.bdata)->values,mesh.Ng);
  dirichletGRHSV(b_v,(fem.bdata)->groups,(fem.bdata)->types,(fem.bdata)->values,mesh.Ng);
  dirichletGRHSV(b_w,(fem.bdata)->groups,(fem.bdata)->types,(fem.bdata)->values,mesh.Ng);

  //solve using preconditioned conjugate gradient
  iter = pcg(krow,kcol,K,u_vel,b_u,neq);
  iter = pcg(krow,kcol,K,v_vel,b_v,neq);
  iter = pcg(krow,kcol,K,w_vel,b_w,neq);
}



//----------------------------------------------------------------------------
// divergence
//----------------------------------------------------------------------------
void SolverFluid::divergence()
{
  for(int i=0;i<neq;i++){
    div[i] = 0.0;
  }

  //find divergence
  for(int i=0;i<neq;i++){
    for(int j=hxrow[i];j<hxrow[i+1];j++){
      div[i] += Hx[j]*u_vel[hxcol[j]] + Hx[j]*v_vel[hxcol[j]];
    }
  }
}



//----------------------------------------------------------------------------
// Pressure correction
//----------------------------------------------------------------------------
void SolverFluid::pressure()
{
 
}



//----------------------------------------------------------------------------
// Solve Fluid (momentum and continuity) equations
//----------------------------------------------------------------------------
int SolverFluid::solve()
{
  double theta = 0.5;

  assembleGSM(krow,kcol,K,0,neq);
  assembleGSM(mrow,mcol,M,1,neq);
  assembleGSM(hxrow,hxcol,Hx,2,neq);
  assembleGSM(hyrow,hycol,Hy,3,neq);
  assembleGSM(hzrow,hzcol,Hz,4,neq);
  //assembleGSM(kprow,kpcol,Kp,5,neq); 

  //form K = M+theta*dt*K and M = M-(1-theta)*dt*K 
  for(int i=0;i<krow[neq];i++){
    double K_i = K[i];
    double M_i = M[i];
    K[i] = M_i+theta*0.02*fem.get_dt()*K_i;
    M[i] = M_i-(1-theta)*0.02*fem.get_dt()*K_i;
  }

  //enforce dirichlet boundary conditions
  dirichletGSM(krow,kcol,K,(fem.bdata)->groups,(fem.bdata)->types,mesh.Ng);
  //dirichletGSM(kprow,kpcol,Kp,(fem.bdata)->groups,(fem.bdata)->types,mesh.Ng);

  for(int i=0;i<fem.get_steps();i++){
    advection();
    diffusion();
    pressure();
  }

  //write solution vector to output file
  writeUVelocity();
  writeVVelocity();
  writeWVelocity();
}



//----------------------------------------------------------------------------
// destructor for solver
//----------------------------------------------------------------------------
SolverFluid::~SolverFluid()
{
  delete [] krow;
  delete [] kcol;
  delete [] K;

  delete [] mrow;
  delete [] mcol;
  delete [] M;

  delete [] hxrow;
  delete [] hxcol;
  delete [] Hx;

  delete [] hyrow;
  delete [] hycol;
  delete [] Hy;

  delete [] hzrow;
  delete [] hzcol;
  delete [] Hz;

  delete [] kprow;
  delete [] kpcol;
  delete [] Kp;

  delete [] LM;

  delete [] u_vel;
  delete [] v_vel;
  delete [] w_vel;
  delete [] div;
  delete [] pres;

  delete [] u_old;
  delete [] v_old;
  delete [] w_old;
  delete [] pres_old;

  delete [] b_u;
  delete [] b_v;
  delete [] b_w;
}




















//----------------------------------------------------------------------
// Write Temperature Results to Output File
//----------------------------------------------------------------------
void SolverHeat::writeTemperature()
{
  std::ofstream myfile;
  myfile.open("output.pos");

  char typ[3] = {' ',' ',' '};
  int npe = 0;

  switch(mesh.Type)
  {
    case 1:
      typ[0] = 'S'; typ[1] = 'L'; typ[2] = ' '; npe=2;
      break;
    case 2:
      typ[0] = 'S'; typ[1] = 'T'; typ[2] = ' '; npe=3;
      break;
    case 3:
      typ[0] = 'S'; typ[1] = 'Q'; typ[2] = ' '; npe=4;
      break;
    case 4:
      typ[0] = 'S'; typ[1] = 'S'; typ[2] = ' '; npe=4;
      break;
    case 8:
      typ[0] = 'S'; typ[1] = 'L'; typ[2] = '2'; npe=3;
      break;
    case 9:
      typ[0] = 'S'; typ[1] = 'T'; typ[2] = '2'; npe=6;
      break;
    case 10:
      typ[0] = 'S'; typ[1] = 'Q'; typ[2] = '2'; npe=8;
      break;
    case 11:
      typ[0] = 'S'; typ[1] = 'S'; typ[2] = '2'; npe=10;
      break;
  }

  myfile<<"View \"Temperature\" {\n";
  for(int i=0;i<mesh.Ne;i++){
    myfile<<typ[0]<<typ[1]<<typ[2]<<" (";
    for(int j=0;j<npe;j++){
      myfile<<mesh.xpoints[mesh.connect[i][j]-1]<<","
            <<mesh.ypoints[mesh.connect[i][j]-1]<<","
            <<mesh.zpoints[mesh.connect[i][j]-1];
      if(j<npe-1){
        myfile<<",";
      }
      else{
        myfile<<"){";
      }
    }

    for(int j=0;j<npe;j++){ 
      myfile<<temp[mesh.connect[i][j]-1];
      if(j<npe-1){
        myfile<<",";
      }
      else{
        myfile<<"};\n";
      }
    }
  }
  myfile<<"};\n";
  myfile.close();
}



//----------------------------------------------------------------------
// Write Velocity Results to Output File
//----------------------------------------------------------------------
void SolverFluid::writeUVelocity()
{
  std::ofstream myfile;
  myfile.open("output_u.pos");

  char typ[3] = {' ',' ',' '};
  int npe = 0;

  switch(mesh.Type)
  {
    case 1:
      typ[0] = 'S'; typ[1] = 'L'; typ[2] = ' '; npe=2;
      break;
    case 2:
      typ[0] = 'S'; typ[1] = 'T'; typ[2] = ' '; npe=3;
      break;
    case 3:
      typ[0] = 'S'; typ[1] = 'Q'; typ[2] = ' '; npe=4;
      break;
    case 4:
      typ[0] = 'S'; typ[1] = 'S'; typ[2] = ' '; npe=4;
      break;
    case 8:
      typ[0] = 'S'; typ[1] = 'L'; typ[2] = '2'; npe=3;
      break;
    case 9:
      typ[0] = 'S'; typ[1] = 'T'; typ[2] = '2'; npe=6;
      break;
    case 10:
      typ[0] = 'S'; typ[1] = 'Q'; typ[2] = '2'; npe=8;
      break;
    case 11:
      typ[0] = 'S'; typ[1] = 'S'; typ[2] = '2'; npe=10;
      break;
  }

  myfile<<"View \"U Velocity\" {\n";
  for(int i=0;i<mesh.Ne;i++){
    myfile<<typ[0]<<typ[1]<<typ[2]<<" (";
    for(int j=0;j<npe;j++){
      myfile<<mesh.xpoints[mesh.connect[i][j]-1]<<","
            <<mesh.ypoints[mesh.connect[i][j]-1]<<","
            <<mesh.zpoints[mesh.connect[i][j]-1];
      if(j<npe-1){
        myfile<<",";
      }
      else{
        myfile<<"){";
      }
    }

    for(int j=0;j<npe;j++){
      myfile<<u_vel[mesh.connect[i][j]-1];
      if(j<npe-1){
        myfile<<",";
      }
      else{
        myfile<<"};\n";
      }
    }
  }
  myfile<<"};\n";
  myfile.close();
}



void SolverFluid::writeVVelocity()
{
  std::ofstream myfile;
  myfile.open("output_v.pos");

  char typ[3] = {' ',' ',' '};
  int npe = 0;

  switch(mesh.Type)
  {
    case 1:
      typ[0] = 'S'; typ[1] = 'L'; typ[2] = ' '; npe=2;
      break;
    case 2:
      typ[0] = 'S'; typ[1] = 'T'; typ[2] = ' '; npe=3;
      break;
    case 3:
      typ[0] = 'S'; typ[1] = 'Q'; typ[2] = ' '; npe=4;
      break;
    case 4:
      typ[0] = 'S'; typ[1] = 'S'; typ[2] = ' '; npe=4;
      break;
    case 8:
      typ[0] = 'S'; typ[1] = 'L'; typ[2] = '2'; npe=3;
      break;
    case 9:
      typ[0] = 'S'; typ[1] = 'T'; typ[2] = '2'; npe=6;
      break;
    case 10:
      typ[0] = 'S'; typ[1] = 'Q'; typ[2] = '2'; npe=8;
      break;
    case 11:
      typ[0] = 'S'; typ[1] = 'S'; typ[2] = '2'; npe=10;
      break;
  }

  myfile<<"View \"V Velocity\" {\n";
  for(int i=0;i<mesh.Ne;i++){
    myfile<<typ[0]<<typ[1]<<typ[2]<<" (";
    for(int j=0;j<npe;j++){
      myfile<<mesh.xpoints[mesh.connect[i][j]-1]<<","
            <<mesh.ypoints[mesh.connect[i][j]-1]<<","
            <<mesh.zpoints[mesh.connect[i][j]-1];
      if(j<npe-1){
        myfile<<",";
      }
      else{
        myfile<<"){";
      }
    }

    for(int j=0;j<npe;j++){
      myfile<<v_vel[mesh.connect[i][j]-1];
      if(j<npe-1){
        myfile<<",";
      }
      else{
        myfile<<"};\n";
      }
    }
  }
  myfile<<"};\n";
  myfile.close();
}



void SolverFluid::writeWVelocity()
{
  std::ofstream myfile;
  myfile.open("output_w.pos");

  char typ[3] = {' ',' ',' '};
  int npe = 0;

  switch(mesh.Type)
  {
    case 1:
      typ[0] = 'S'; typ[1] = 'L'; typ[2] = ' '; npe=2;
      break;
    case 2:
      typ[0] = 'S'; typ[1] = 'T'; typ[2] = ' '; npe=3;
      break;
    case 3:
      typ[0] = 'S'; typ[1] = 'Q'; typ[2] = ' '; npe=4;
      break;
    case 4:
      typ[0] = 'S'; typ[1] = 'S'; typ[2] = ' '; npe=4;
      break;
    case 8:
      typ[0] = 'S'; typ[1] = 'L'; typ[2] = '2'; npe=3;
      break;
    case 9:
      typ[0] = 'S'; typ[1] = 'T'; typ[2] = '2'; npe=6;
      break;
    case 10:
      typ[0] = 'S'; typ[1] = 'Q'; typ[2] = '2'; npe=8;
      break;
    case 11:
      typ[0] = 'S'; typ[1] = 'S'; typ[2] = '2'; npe=10;
      break;
  }

  myfile<<"View \"W Velocity\" {\n";
  for(int i=0;i<mesh.Ne;i++){
    myfile<<typ[0]<<typ[1]<<typ[2]<<" (";
    for(int j=0;j<npe;j++){
      myfile<<mesh.xpoints[mesh.connect[i][j]-1]<<","
            <<mesh.ypoints[mesh.connect[i][j]-1]<<","
            <<mesh.zpoints[mesh.connect[i][j]-1];
      if(j<npe-1){
        myfile<<",";
      }
      else{
        myfile<<"){";
      }
    }

    for(int j=0;j<npe;j++){
      myfile<<w_vel[mesh.connect[i][j]-1];
      if(j<npe-1){
        myfile<<",";
      }
      else{
        myfile<<"};\n";
      }
    }
  }
  myfile<<"};\n";
  myfile.close();
}
