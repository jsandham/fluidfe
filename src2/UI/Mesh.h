//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef MESH_H
#define MESH_H


class Mesh
{
  public:
    int Dim;                //dimension of mesh (1, 2, or 3) 
    int Ng;                 //number of element groups
    int N;                  //total number of nodes                      
    int Nte;                //total number of elements (Nte=Ne+Ne_b)       
    int Ne;                 //number of interior elements                
    int Ne_b;               //number of boundary elements                                 
    int Npe;                //number of points per interior element      
    int Npe_b;              //number of points per boundary element      
    int Type;               //interior element type                      
    int Type_b;             //boundary element type
    int *groups;            //element groups                      
    double *xpoints;        //x-points of nodes                          
    double *ypoints;        //y-points of nodes                          
    double *zpoints;        //z-points of nodes                          
    int **connect;          //connectivity for interior elements         
    int **connect_b;        //connectivity for boundary elements         


  public:
    Mesh(const char* file);

    void deleteMesh();    
};


#endif
