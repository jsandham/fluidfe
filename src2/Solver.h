//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef SOLVER_H
#define SOLVER_H
#include<iostream>
#include<string>
#include"Mesh.h"
#include"FeModel.h"
#include"BoundaryData.h"

using namespace std;



class Solver
{
  public:
    Mesh *mesh1;      //finite element mesh (poisson, temperature, velocities)
    Mesh *mesh2;      //finite element mesh (pressure)
    FeModel *fem;     //finite element model

    int MAX_NNZ;      //maximum number of non-zeros in a row of a global matrix
    int MAX_ITER;     //maximum number of iterations used in all iterative method
    double EPSILON;   //accuracy iterative methods solve to
    double LARGE_NUM; //lare number used in apply dirichlet boundary conditions

    int *link1;       //array to link mesh1 and mesh2
    int *link2;       //array to link mesh2 and mesh1

  public:
    Solver(Mesh *mesh1, FeModel *fem);
    Solver(Mesh *mesh1, Mesh *mesh2, FeModel *fem);
    ~Solver();

    virtual int solve() = 0;   //solve linear system
 
    void assembleGSM(int *row, int *col, double *A, int type, Mesh *mesh);
    void assembleGVEC(int *row, int *col, double *A, double *x, double *y, Mesh *mesh);
    void assembleGLV(double *fvec, Mesh *mesh);
    
    void applyBCGSYS(int *row, int *col, double *A, double *b, BoundaryData *data, Mesh *mesh);
    void applyBCGSM(int *row, int *col, double *A, BoundaryData *data, Mesh *mesh);
    void applyBCGVEC(double *b, BoundaryData *data, Mesh *mesh);
};




class SolverPoisson : public Solver
{
  private:
    int neq1;          //number of equations (size of global matrices for mesh1)

    int* row1;         //pointers to stiffness matrix rows
    int* col1;         //column numbers for nonzero values in K

    double* K;         //nonzero values of the global stiffness matrix by row

    double *u, *f;     //poisson variable and RHS

    string filename;     //output filename

  public:
    SolverPoisson(Mesh *mesh1, FeModel *fem);
    ~SolverPoisson();

    double* getPoissonVariable();

    virtual int solve();
};




class SolverHeat : public Solver
{
  private:
    int neq1;          //number of equations (size of global matrices for mesh1)

    int* row1;         //pointers to stiffness matrix rows
    int* col1;         //column numbers for nonzero values in K

    double* K;         //nonzero values of the global stiffness matrix by row
    double* M;         //nonzero values of the global mass matrix by row

    double *temp, *b, *f;  //temperature and RHS

    string filename;     //output filename

  public:
    SolverHeat(Mesh *mesh1, FeModel *fem);
    ~SolverHeat();

    double* getTemperature(); 

    virtual int solve();

  private:
    void theta_method();
};




class SolverFluid : public Solver
{
  private:
    int neq1;        //number of equations (size of global matrices for mesh1)
    int neq2;        //number of equations (size of global matrices for mesh2)

    int *row1;       //pointers to matrix rows (mesh1)
    int *col1;       //column numbers for nonzero values in matrix (mesh1)
    int *row2;       //pointers to matrix rows (mesh2)
    int *col2;       //column numbers for nonzero values in matrix (mesh2)

    double* K;       //nonzero values of the global stiffness matrix by row
    double* M;       //nonzero values of the global mass matrix by row
    double* Hx;      //nonzero values of the global x-advection matrix by row
    double* Hy;      //nonzero values of the global y-advection matrix by row
    double* Hz;      //nonzero values of the global z-advection matrix by row
    double* Kp;      //nonzero values of the global pressure stiffness matrix by row (mesh2)
    double* Hxp;     //nonzero values of the global x-advection matrix by row (mesh2)
    double* Hyp;     //nonzero values of the global y-advection matrix by row (mesh2)
    double* Hzp;     //nonzero values of the global z-advection matrix by row (mesh2)

    double *u_vel, *u_old;     //u velocity 
    double *v_vel, *v_old;     //v velocity
    double *w_vel, *w_old;     //w velocity
    double *pres, *pres_old;   //pressure
    double *div;               //divergence

    double *b_u;       //RHS vector b_u
    double *b_v;       //RHS vector b_v
    double *b_w;       //RHS vector b_w

    string ufilename;   //output filename for u-velocity
    string vfilename;   //output filename for v-velocity
    string wfilename;   //output filename for w-velocity
    string pfilename;   //output filename for pressure

  public: 
    SolverFluid(Mesh *mesh1, Mesh *mesh2, FeModel *fem);
    ~SolverFluid();

    double* getUVelocity();
    double* getVVelocity();
    double* getWVelocity();
    double* getPressure();
    
    virtual int solve();

  private:
    void theta_method2D(double theta);
    void theta_method3D(double theta);
};


#endif
