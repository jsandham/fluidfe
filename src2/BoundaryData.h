//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef BOUNDARYDATA_H
#define BOUNDARYDATA_H



class BoundaryData
{
  private:
    int numGrps;        //maximum number of groups
    int numDirichGrps;  //number of dirichlet groups
    int numFluxGrps;    //number of flux groups

    int *dirichGrps;
    int *fluxGrps;

    double *dirichVals;
    double *fluxVals;

  public:
    BoundaryData(int n);
    ~BoundaryData();

  	int getNumGrps();
  	int getNumDirichletGrps();
  	int getNumFluxGrps();

  	void addDirichlet(int grp, double val);
  	void addFlux(int grp, double val);
  	void removeDirichlet(int grp);
  	void removeFlux(int grp);

  	bool isDirichlet(int grp);
  	bool isFlux(int grp);

  	double getDirichletValue(int grp);
  	double getFluxValue(int grp);    
};

#endif
