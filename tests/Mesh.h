//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef MESH_H
#define MESH_H

#include "Element.h"



class ElementNode
{
  public:
  	Element *element;
  	ElementNode *left;
  	ElementNode *right;

  public:
  	ElementNode(int typ);
    ~ElementNode();

};



class Mesh
{
  private:
    int N;               //number of nodes
    int Ne;              //number of initial elements
  	ElementNode **roots; //array of element node trees

  public:
  	Mesh(const char *file);
    ~Mesh();

    void get_N();
    void get_Ne();


};


#endif