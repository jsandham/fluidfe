//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef BOUNDARYDATA_H
#define BOUNDARYDATA_H


class BoundaryData
{
  private:
    int dirichletGroups[10];
    int fluxGroups[10];
    double dirichletValues[10];
    double fluxValues[10];

  public:
    BoundaryData();
    ~BoundaryData();

    void clearDirichlet();
    void clearFlux();
    void addDirichletGroup(int grp, double val);
    void addFluxGroup(int grp, double val);
    bool isGroupDirichlet(int grp);
    bool isGroupFlux(int grp);
    double getDirichletValue(int grp);
    double getFluxValue(int grp);
};





//#include"Mesh.h"
//#include"Tree.h"

// class BoundaryData
// {
//   private:
//     Tree *boundaryTree;  //contains boundary data as a tree of nodes
//     Node *boundaryNodes; //contains boundary data as an array of nodes
    
//   public:
//     BoundaryData(Mesh *mesh);
//     ~BoundaryData();

//     int getNumNodes();
//     void addDirichlet(int grp, double val);
//     void addFlux(int grp, double val);
//     void removeDirichlet(int grp);
//     void removeFlux(int grp);
//     bool isDirichlet(int node);
//     bool isFlux(int node);
//     double getDirichletValue(int node);
//     double getFluxValue(int node);
// };

#endif
