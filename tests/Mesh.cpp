//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#include <iostream>
#include <stdlib.h>
#include "Mesh.h"
#include "Element.h"
#include "GmshReader.h"



//--------------------------------------------------------------------
// element node constructor 
//--------------------------------------------------------------------
ElementNode::ElementNode(int typ)
{
  element = new Element(typ);
  left = NULL;
  right = NULL;
}



//--------------------------------------------------------------------
// element node destructor 
//--------------------------------------------------------------------
ElementNode::~ElementNode()
{
  delete element;
}



//--------------------------------------------------------------------
// mesh2 constructor 
//--------------------------------------------------------------------
Mesh::Mesh(const char *file)
{
  //read gmsh file
  GmshReader *input = new GmshReader(file);

  //number of initial coarse elements in mesh
  N = input->get_N();
  Ne = input->get_Ne();

  //create array of element node pointers
  roots = new ElementNode*[Ne];

  //fill in array of element nodes with initial elements in coarse mesh
  for(int i=0;i<Ne;i++){
    roots[i] = new ElementNode(input->get_Type());

    for(int j=0;j<input->get_Npe();j++){
      //std::cout<<input->get_group(input->get_connect(i,j)-1)<<" ";
      (roots[i]->element)->groups[j] = input->get_group(input->get_connect(i,j)-1);
      (roots[i]->element)->nodes[j] = input->get_connect(i,j)-1;
      (roots[i]->element)->xpts[j] = input->get_xpoint((roots[i]->element)->nodes[j]);
      (roots[i]->element)->ypts[j] = input->get_ypoint((roots[i]->element)->nodes[j]);
      (roots[i]->element)->zpts[j] = input->get_zpoint((roots[i]->element)->nodes[j]);
    }
    //std::cout<<""<<std::endl;

    (roots[i]->element)->stiffnessMatrix();
    (roots[i]->element)->massMatrix();
    (roots[i]->element)->advecXMatrix();
    (roots[i]->element)->advecYMatrix();
    (roots[i]->element)->advecZMatrix();
    (roots[i]->element)->elementVector();
  }

  //for(int i=0;i<input->get_Ng();i++){
  //  std::cout<<input->get_group(0)<<" ";
  //}
  //std::cout<<""<<std::endl;
  //std::cout<<"Node: "<<" Group: "<<input->get_group(255)<<std::endl;

  delete input;
}




//--------------------------------------------------------------------
// mesh2 destructor 
//--------------------------------------------------------------------
Mesh::~Mesh()
{
  for(int i=0;i<Ne;i++){
    delete roots[i];
  }
  delete [] roots;
}