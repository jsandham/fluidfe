#include <iostream>
#include "Mesh.h"
#include "BoundaryData.h"
#include "FeModel.h"
using namespace std;



int main(int argc, char *argv[])
{
  //Mesh objects
  Mesh *mesh = new Mesh(argv[1]);   
  
  //define boundary data objects
  BoundaryData *tdata = new BoundaryData();
  BoundaryData *udata = new BoundaryData();
  BoundaryData *vdata = new BoundaryData();
  BoundaryData *wdata = new BoundaryData();

  //define finite element model
  FeModel *fem = new FeModel(tdata);

  fem->set_time(true);
  fem->set_dt(0.025);
  fem->set_steps(50);
  fem->set_theta(1);
  fem->set_kappa(10);
  fem->set_Re(10);

  //solve 



  delete fem;
  delete tdata;
  delete udata;
  delete vdata;
  delete wdata;
  delete mesh;
}