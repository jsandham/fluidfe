//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#include <iostream>
#include "BoundaryData.h"



//----------------------------------------------------------------------------
// constructor for boundary data class
//----------------------------------------------------------------------------
BoundaryData::BoundaryData()
{
  for(int i=0;i<10;i++){
    dirichletGroups[i] = 0;
    fluxGroups[i] = 0;
    dirichletValues[i] = 0.0;
    fluxValues[i] = 0.0;
  }
}


//----------------------------------------------------------------------------
// destructor for boundary data class
//----------------------------------------------------------------------------
BoundaryData::~BoundaryData()
{

}



//----------------------------------------------------------------------------
// clear dirichlet group and value arrays
//----------------------------------------------------------------------------
void BoundaryData::clearDirichlet()
{
  for(int i=0;i<10;i++){
    dirichletGroups[i] = 0;
    dirichletValues[i] = 0.0;
  }
}


//----------------------------------------------------------------------------
// clear flux group and value arrays
//----------------------------------------------------------------------------
void BoundaryData::clearFlux()
{
  for(int i=0;i<10;i++){
    fluxGroups[i] = 0;
    fluxValues[i] = 0.0;
  }
}



//----------------------------------------------------------------------------
// add a dirichlet group 
//----------------------------------------------------------------------------
void BoundaryData::addDirichletGroup(int grp, double val)
{
  for(int i=0;i<10;i++){
    if(dirichletGroups[i]==0){
      dirichletGroups[i] = grp;
      dirichletValues[i] = val;
      break;
    }
  }
}



//----------------------------------------------------------------------------
// add a flux group 
//----------------------------------------------------------------------------
void BoundaryData::addFluxGroup(int grp, double val)
{
  for(int i=0;i<10;i++){
    if(fluxGroups[i]==0){
      fluxGroups[i] = grp;
      fluxValues[i] = val;
      break;
    }
  }
}



//----------------------------------------------------------------------------
// is input group a dirichlet group?
//----------------------------------------------------------------------------
bool BoundaryData::isGroupDirichlet(int grp)
{
  bool flag = false;
  for(int i=0;i<10;i++){
    if(dirichletGroups[i]==grp){
      flag = true;
      break;
    }
  }
  return flag;
}



//----------------------------------------------------------------------------
// is input group a flux group?
//----------------------------------------------------------------------------
bool BoundaryData::isGroupFlux(int grp)
{
  bool flag = false;
  for(int i=0;i<10;i++){
    if(fluxGroups[i]==grp){
      flag = true;
      break;
    }
  }
  return flag;
}



//----------------------------------------------------------------------------
// return dirichlet value corresponding to input group
//----------------------------------------------------------------------------
double BoundaryData::getDirichletValue(int grp)
{
  double val = 0.0;
  for(int i=0;i<10;i++){
    if(dirichletGroups[i]==grp){
      val = dirichletValues[i];
      break;
    }
  }
  return val;
}



//----------------------------------------------------------------------------
// return flux value corresponding to input group
//----------------------------------------------------------------------------
double BoundaryData::getFluxValue(int grp)
{
  double val = 0.0;
  for(int i=0;i<10;i++){
    if(fluxGroups[i]==grp){
      val = fluxValues[i];
      break;
    }
  }
  return val;
}


























//#include "Tree.h"


// //----------------------------------------------------------------------------
// // constructor for boundary data class
// //----------------------------------------------------------------------------
// BoundaryData::BoundaryData(Mesh *mesh)
// {
//   boundaryTree = new Tree();
//   for(int i=0;i<mesh->get_Ne_b();i++){
//     for(int j=1;j<mesh->get_Npe_b()+1;j++){
//       boundaryTree->insert(mesh->get_connect_b(i,1)-1,mesh->get_connect_b(i,0));
//     }
//   }                                                                         

//   boundaryNodes = new Node[boundaryTree->getNumNodes()];
//   boundaryTree->toNodeArray(boundaryNodes);
// }


// //----------------------------------------------------------------------------
// // destructor for boundary data class
// //----------------------------------------------------------------------------
// BoundaryData::~BoundaryData()
// {
//   delete boundaryTree;
//   delete[] boundaryNodes;
// }



// // //----------------------------------------------------------------------------
// // // get number of boundary nodes
// // //----------------------------------------------------------------------------
// int BoundaryData::getNumNodes()
// {
//   return boundaryTree->getNumNodes();
// }



// //----------------------------------------------------------------------------
// // add a dirichlet/flux group 
// //----------------------------------------------------------------------------
// void BoundaryData::addDirichlet(int grp, double val)
// {
//   for(int i=0;i<boundaryTree->getNumNodes();i++){
//     if(boundaryNodes[i].group==grp){
//       boundaryNodes[i].type = 1;  
//       boundaryNodes[i].value = val;
//     }
//   }
// }


// void BoundaryData::addFlux(int grp, double val)
// {
//   for(int i=0;i<boundaryTree->getNumNodes();i++){
//     if(boundaryNodes[i].group==grp){
//       boundaryNodes[i].type = 2;
//       boundaryNodes[i].value = val;
//     }
//   }
// }


// void BoundaryData::removeDirichlet(int grp)
// {
//   for(int i=0;i<boundaryTree->getNumNodes();i++){
//     if(boundaryNodes[i].group==grp){
//       boundaryNodes[i].type = 0;
//       boundaryNodes[i].value = 0.0;
//     }
//   }
// }


// void BoundaryData::removeFlux(int grp)
// {
//   for(int i=0;i<boundaryTree->getNumNodes();i++){
//     if(boundaryNodes[i].group==grp){
//       boundaryNodes[i].type = 0;
//       boundaryNodes[i].value = 0.0;
//     }
//   }
// }



// //----------------------------------------------------------------------------
// // determine whether an input group is a dirichlet/flux group
// //----------------------------------------------------------------------------
// bool BoundaryData::isDirichlet(int node)
// {
//   //perform a binary search of the boundaryNodes array
//   int min = 0;
//   int max = boundaryTree->getNumNodes()-1;

//   //continue searching while [min,max] is not empty
//   while (min <= max){
//     //calculate the midpoint for roughly equal partition
//     int mid = (min+max)/2;
//     if(boundaryNodes[mid].node == node)
//       if(boundaryNodes[mid].type == 1)
//         return true;  //node found (and is a dirichlet type) at index mid
//       else
//         return false; //node found (but is not dirichlet type) at index mid
//     //determine which subarray to search
//     else if (boundaryNodes[mid].node < node)
//       min = mid + 1;  //change min index to search upper subarray
//     else         
//       max = mid - 1;  //change max index to search lower subarray
//   }
//   //node was not found
//   return false;
// }


// bool BoundaryData::isFlux(int node)
// {
//   //perform a binary search of the boundaryNodes array
//   int min = 0;
//   int max = boundaryTree->getNumNodes()-1;

//   //continue searching while [min,max] is not empty
//   while (min <= max){
//     //calculate the midpoint for roughly equal partition
//     int mid = (min+max)/2;
//     if(boundaryNodes[mid].node == node)
//       if(boundaryNodes[mid].type == 2)
//         return true;   //node found (and is a flux type) at index mid
//       else
//         return false;  //node found (but is not flux type) at index mid
//     else if (boundaryNodes[mid].node < node)
//       min = mid + 1;  //change min index to search upper subarray
//     else         
//       max = mid - 1;  //change max index to search lower subarray
//   }
//   //node was not found
//   return false;
// }



// //----------------------------------------------------------------------------
// // return dirichlet/flux value corresponding to input group
// //----------------------------------------------------------------------------
// double BoundaryData::getDirichletValue(int node)
// {
//   //perform a binary search of the boundaryNodes array
//   int min = 0;
//   int max = boundaryTree->getNumNodes()-1;

//   //continue searching while [min,max] is not empty
//   while (min <= max){
//     //calculate the midpoint for roughly equal partition
//     int mid = (min+max)/2;
//     if(boundaryNodes[mid].node == node)
//       return boundaryNodes[mid].value;
//     //determine which subarray to search
//     else if (boundaryNodes[mid].node < node)
//       min = mid + 1;  //change min index to search upper subarray
//     else         
//       max = mid - 1;  //change max index to search lower subarray
//   }
//   //node was not found
//   return 0;
// }


// double BoundaryData::getFluxValue(int node)
// {
//   //perform a binary search of the boundaryNodes array
//   int min = 0;
//   int max = boundaryTree->getNumNodes()-1;

//   //continue searching while [min,max] is not empty
//   while (min <= max){
//     //calculate the midpoint for roughly equal partition
//     int mid = (min+max)/2;
//     if(boundaryNodes[mid].node == node)
//       return boundaryNodes[mid].value;
//     //determine which subarray to search
//     else if (boundaryNodes[mid].node < node)
//       min = mid + 1;  //change min index to search upper subarray
//     else         
//       max = mid - 1;  //change max index to search lower subarray
//   }
//   //node was not found
//   return 0;
// }