//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef GMSHREADER_H
#define GMSHREADER_H

class GmshReader
{
  private:
    int Dim;                //dimension of mesh (1, 2, or 3) 
    int Ng;                 //number of element groups
    int N;                  //total number of nodes                      
    int Nte;                //total number of elements (Nte=Ne+Ne_b)       
    int Ne;                 //number of interior elements                
    int Ne_b;               //number of boundary elements                                 
    int Npe;                //number of points per interior element      
    int Npe_b;              //number of points per boundary element      
    int Type;               //interior element type                      
    int Type_b;             //boundary element type
    int *groups;            //element groups       
    double *xpoints;        //x-points of nodes                          
    double *ypoints;        //y-points of nodes                          
    double *zpoints;        //z-points of nodes                          
    int **connect;          //connectivity for interior elements         
    int **connect_b;        //connectivity for boundary elements         


  public:
    GmshReader(const char* file);
    ~GmshReader();

    int get_Dim();
    int get_Ng();
    int get_N();
    int get_Nte();
    int get_Ne();
    int get_Ne_b();
    int get_Npe();
    int get_Npe_b();
    int get_Type();
    int get_Type_b();

    int get_group(int node);
    double get_xpoint(int node);
    double get_ypoint(int node);
    double get_zpoint(int node);
    int get_connect(int elem, int node);
    int get_connect_b(int elem, int node);
};


#endif
