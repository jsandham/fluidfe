//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef ITERSOLVERS_H
#define ITERSOLVERS_H

class IterSolvers
{
  public:
    IterSolvers();
    ~IterSolvers();

    static void pcg(int row[], int col[], double A[], double x[], double b[], int n, int maxIter, double tol);
    static void gmres(int row[], int col[], double A[], double x[], double b[], int n, int restart, int maxIter, double tol);

    static void diagonalPreconditioner(int row[], int col[], double A[], double md[], int n);
    static void matrixVectorProduct(int row[], int col[], double A[], double x[], double y[], int n);    
};


#endif
