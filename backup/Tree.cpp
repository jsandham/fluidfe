//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#include <stddef.h>
#include"Tree.h"



//----------------------------------------------------------------------------
// constructor for Node
//----------------------------------------------------------------------------
Node::Node()
{
  left = NULL;
  right = NULL;
}


Node::Node(int n)
{
  node = n;
  group = 0;
  type = 0;
  value = 0;
  left = NULL;
  right = NULL;
}


Node::Node(int n, int g)
{
  node = n;
  group = g;
  type = 0;
  value = 0;
  left = NULL;
  right = NULL;
}


Node::Node(int n, int g, int t)
{
  node = n;
  group = g;
  type = t;
  value = 0;
  left = NULL;
  right = NULL;
}


Node::Node(int n, int g, int t, double v)
{
  node = n;
  group = g;
  type = t;
  value = v;
  left = NULL;
  right = NULL;
}




//----------------------------------------------------------------------------
// constructor for Tree
//----------------------------------------------------------------------------
Tree::Tree()
{
  nNodes = 0;
  index = 0;
  root = NULL;
}


Tree::~Tree()
{
  deleteTree();
}


//----------------------------------------------------------------------------
// insert methods for tree
//----------------------------------------------------------------------------
void Tree::insert(int n)
{
  if(root!=NULL){
    insert(n,root);
  }
  else{
    root = new Node(n);
    nNodes++;
  }
}


void Tree::insert(int n, int g)
{
  if(root!=NULL){
    insert(n,g,root);
  }
  else{
    root = new Node(n,g);
    nNodes++;
  }
}


void Tree::insert(int n, int g, int t)
{
  if(root!=NULL){
    insert(n,g,t,root);
  }
  else{
    root = new Node(n,g,t);
    nNodes++;
  }
}


void Tree::insert(int n, int g, int t, double v)
{
  if(root!=NULL){
    insert(n,g,t,v,root);
  }
  else{
    root = new Node(n,g,t,v);
    nNodes++;
  }
}



void Tree::insert(int n, Node *leaf)
{
  if(n<leaf->node){
    if(leaf->left!=NULL){
      insert(n,leaf->left);
    }
    else{
  	  leaf->left = new Node(n);
      nNodes++;
  	}
  }
  else if(n>leaf->node){
    if(leaf->right!=NULL){
      insert(n,leaf->right);
    }
    else{
  	  leaf->right = new Node(n);
      nNodes++;
  	}
  }
}


void Tree::insert(int n, int g, Node *leaf)
{
  if(n<leaf->node){
    if(leaf->left!=NULL){
      insert(n,g,leaf->left);
    }
    else{
      leaf->left = new Node(n,g);
      nNodes++;
    }
  }
  else if(n>leaf->node){
    if(leaf->right!=NULL){
      insert(n,g,leaf->right);
    }
    else{
      leaf->right = new Node(n,g);
      nNodes++;
    }
  }
  else{
    leaf->group = g;
  }
}


void Tree::insert(int n, int g, int t, Node *leaf)
{
  if(n<leaf->node){
    if(leaf->left!=NULL){
      insert(n,g,t,leaf->left);
    }
    else{
      leaf->left = new Node(n,g,t);
      nNodes++;
    }
  }
  else if(n>leaf->node){
    if(leaf->right!=NULL){
      insert(n,g,t,leaf->right);
    }
    else{
      leaf->right = new Node(n,g,t);
      nNodes++;
    }
  }
  else{
    leaf->group = g;
    leaf->type = t;
  }
}


void Tree::insert(int n, int g, int t, double v, Node *leaf)
{
  if(n<leaf->node){
    if(leaf->left!=NULL){
      insert(n,g,t,v,leaf->left);
    }
    else{
      leaf->left = new Node(n,g,t,v);
      nNodes++;
    }
  }
  else if(n>leaf->node){
    if(leaf->right!=NULL){
      insert(n,g,t,v,leaf->right);
    }
    else{
      leaf->right = new Node(n,g,t,v);
      nNodes++;
    }
  }
  else{
    leaf->group = g;
    leaf->type = t;
    leaf->value = v;
  }
}



//----------------------------------------------------------------------------
// write Tree to array
//----------------------------------------------------------------------------
void Tree::toNodeArray(Node *array)
{
	if(root!=NULL){
    toNodeArray(array,root);
  }
}


void Tree::toNodeArray(Node *array, Node *leaf)
{
  if(leaf->left!=NULL){
    toNodeArray(array,leaf->left);
  }
  
  array[index] = *leaf;
  index++;

  if(leaf->right!=NULL){
    toNodeArray(array,leaf->right);
  }
}



//----------------------------------------------------------------------------
// delete Tree
//----------------------------------------------------------------------------
void Tree::deleteTree()
{
  if(root!=NULL){
    deleteTree(root);
    delete root;
  }
}


void Tree::deleteTree(Node *leaf)
{
  if(leaf->left!=NULL){
    deleteTree(leaf->left);
    delete leaf->left;
  }

  if(leaf->right!=NULL){
    deleteTree(leaf->right);
    delete leaf->right;
  }
}



//----------------------------------------------------------------------------
// return number of nodes in tree
//----------------------------------------------------------------------------
int Tree::getNumNodes()
{
  return nNodes;
}