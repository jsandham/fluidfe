//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************
#include<iostream>
#include"Mesh.h"
#include"BoundaryData.h"
#include"FeModel.h"
#include"Solver.h"

#include"IterSolvers.h"
#include"Tree.h" 




//********************************************************************************
//
// Main Program 
//
//********************************************************************************
int main(int argc, char *argv[])
{
  //Mesh objects
  Mesh *vmesh = new Mesh(argv[1]);   //velocity mesh
  Mesh *pmesh = new Mesh(argv[2]);   //pressure mesh

  //define boundary data
  BoundaryData *tdata = new BoundaryData(vmesh);  //boundary conditions for temperature
  BoundaryData *udata = new BoundaryData(vmesh);  //boundary conditions for u-velocity
  BoundaryData *vdata = new BoundaryData(vmesh);  //boundary conditions for v-velocity
  BoundaryData *wdata = new BoundaryData(vmesh);  //boundary conditions for w-velocity

  tdata->addDirichlet(vmesh->get_group(0),0.0);
  tdata->addDirichlet(vmesh->get_group(1),1.0);
  udata->addDirichlet(vmesh->get_group(0),0.0);
  udata->addDirichlet(vmesh->get_group(1),1.0);
  vdata->addDirichlet(vmesh->get_group(0),0.0);
  vdata->addDirichlet(vmesh->get_group(1),0.0);

  //define finite element model
  FeModel *fem = new FeModel(tdata,udata,vdata,wdata);

  fem->set_time(true);
  fem->set_dt(0.0025);
  fem->set_steps(20);
  fem->set_rho(1);
  fem->set_c(2);
  fem->set_Q(1);
  fem->set_k(3);
  fem->set_Re(10);

  //create solver objects and solve
  //SolverPoisson solver1(vmesh,fem);        //create solver object and pass model to constructor
  //SolverHeat solver2(vmesh,fem);           //create solver object and pass model to constructor
  SolverFluid solver3(vmesh,pmesh,fem);  //create solver object and pass model to constructor

  //int itr1 = solver1.solve();     //solve Global system
  //int itr2 = solver2.solve();     //solve Global system 
  int itr3 = solver3.solve();     //solve Global system

  std::cout<<"Ne: "<<vmesh->get_Ne()<<std::endl;
  //std::cout<<"neq2: "<<pmesh->N<<std::endl;
  //std::cout<<"Number of dirichlet boundary groups: "<<tdata->getNumDirichletGrps()<<std::endl;
  //std::cout<<"Number of flux boundary groups: "<<tdata->getNumFluxGrps()<<std::endl;


  delete vmesh;
  delete pmesh;
  delete tdata;
  delete udata;
  delete vdata;
  delete wdata;
  delete fem; 



















// test Tree
// int A[21] = {4,7,2,9,8,1,0,5,6,3,2,2,8,3,1,6,5,8,7,9,5};
// int B[21] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

// Tree myTree;
// //myTree.insert(2);
// //myTree.insert(1);
// //myTree.insert(3);
// for(int i=0;i<21;i++){
//   myTree.insert(A[i]);
// }  

// myTree.toArray(B);

// for(int i=0;i<21;i++){
//   std::cout<<B[i]<<std::endl;
// }

// std::cout<<"Number of nodes in tree: "<<myTree.getNumNodes()<<std::endl;
// myTree.deleteTree();






// test iterSolvers

  //test 1  works
  // int N = 8;
  // int aar[9] = {0,6,11,16,23,31,37,43,50};
  // int aac[50] = {0,2,3,4,6,7,1,3,4,5,7,0,2,3,4,6,0,1,2,3,4,5,7,0,1,2,3,4,5,6,7,1,3,4,5,6,7,0,2,
  //               4,5,6,7,0,1,3,4,5,6,7};
  // double aav[50] = {2,1,-0.75,0.1,0.2,0.6,4,0.2,0.3,0.4,0.5,1,3,1,-1,1.5,-0.75,0.2,1,4,0.6,0.8,
  //                  0.9,0.1,0.3,-1,0.6,2,-0.9,1,-1.1,0.4,0.8,-0.9,4,1.999,1,0.2,1.5,1,2,6,
  //                 -2.999,0.6,0.5,0.9,-1.1,1,-3,8};
  // double bb[50] = {1,1,1,1,1,1,1,1};
  // double x[50] = {1,1,1,1,1,1,1,1};
  // int niter1 = IterSolvers::pcg(aar,aac,aav,x,bb,N,1000,10e-8);
  // std::cout<<"iter1: "<<niter1<<std::endl;
  // for(int i=0;i<5;i++){
  //  std::cout<<x[i]<<std::endl;
  // }
  // for(int i=0;i<6;i++){x[i] = 1;}
  // std::cout<<""<<std::endl;
  // int niter2 = IterSolvers::gmres(aar,aac,aav,x,bb,N,3,1000,10e-8);
  // std::cout<<"iter2: "<<niter2<<std::endl;
  // for(int i=0;i<5;i++){
  //  std::cout<<x[i]<<std::endl;
  // }


  //test 2 works
  //int N = 6;
  //int aar[7] = {0,6,12,18,24,30,36};
  //int aac[36] = {0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5,0,1,2,3,4,5};
  //double aav[36] = {2,0.5310,0.4187,-0.7620,0.5025,0.0944,0.5310,2,0.5094,-0.0033,-0.4898,-0.7228,
  //                  0.4187,0.5094,2,0.9195,0.0119,-0.7014,-0.7620,-0.033,0.9195,2,0.3982,-0.4850,
  //                  0.5025,-0.4898,0.0119,0.3982,2,0.6814,0.0944,-0.7228,-0.7014,-0.4850,0.6814,2};
  //double bb[6] = {1,1,1,1,1,1};
  //double x[6] = {1,1,1,1,1,1};
  //int niter1 = IterSolvers::pcg(aar,aac,aav,x,bb,N,1000,10e-8);
  //std::cout<<"iter1: "<<niter1<<std::endl;
  //for(int i=0;i<5;i++){
  //  std::cout<<x[i]<<std::endl;
  //}
  //for(int i=0;i<6;i++){x[i] = 0;}
  //std::cout<<""<<std::endl;
  //int niter2 = IterSolvers::gmres(aar,aac,aav,x,bb,N,5,100,10e-8);
  //std::cout<<"iter2: "<<niter2<<std::endl;
  //for(int i=0;i<5;i++){
  //  std::cout<<x[i]<<std::endl;
  //}


  //test 5 works
  // int N = 8;
  // int aar[9] = {0,6,11,15,19,26,31,37,44};
  // int aac[44] = {0,1,3,4,6,7,0,1,2,4,7,1,2,5,7,0,3,4,6,0,1,3,4,5,6,7,2,4,5,6,7,0,3,4,5,6,7,0,1,2,4,5,6,7};
  // double aav[44] = {2,0.1,-0.56,-0.7,0.025,0.33,0.1,-4.2,0.8147,-0.746,0.82,0.8147,3.1,0.26,-0.8,-0.56,2.3,
  //                -0.44,0.09,-0.7,-0.746,-0.44,6,0.91,0.92,-0.68,0.26,0.91,7,0.94,0.91,0.025,0.09,0.92,
  //                 0.94,1.2,-0.02,0.33,0.82,-0.8,-0.68,0.91,-0.02,4};
  // double x[8] = {1,1,1,1,1,1,1,1};
  // double bb[8] = {1,1,1,1,1,1,1,1};
  // int niter1 = IterSolvers::pcg(aar,aac,aav,x,bb,N,1000,10e-8);
  // std::cout<<"iter1: "<<niter1<<std::endl;
  // for(int i=0;i<5;i++){
  //  std::cout<<x[i]<<std::endl;
  // }
  // for(int i=0;i<8;i++){x[i] = 1;}
  // std::cout<<""<<std::endl;
  // int niter2 = IterSolvers::gmres(aar,aac,aav,x,bb,N,7,100,10e-8);
  // std::cout<<"iter2: "<<niter2<<std::endl;
  // for(int i=0;i<5;i++){
  //  std::cout<<x[i]<<std::endl;
  // }



  //test 7
  // int N = 12;
  // int aar[13] = {0,1,12,20,28,32,40,49,57,66,75,83,90};
  // int aac[90] = {0,1,2,3,4,5,6,7,8,9,10,11,1,2,4,6,7,9,10,11,1,3,4,5,6,8,9,10,1,2,3,4,
  //               1,3,5,6,7,8,10,11,1,2,3,5,6,7,8,9,11,1,2,5,6,7,8,9,10,1,3,5,6,7,8,9,10,11,
  //               1,2,3,6,7,8,9,10,11,1,2,3,5,7,8,9,10,1,2,5,6,8,9,11};
  // double aav[90] = {4.2,2.2,0.629,0.811,-0.746,0.826,0.264,-0.804,-0.443,0.093,0.915,0.929,
  //                  0.629,6.4,0.631,-0.591,0.613,0.651,0.712,0.732,
  //                  0.811,3.3,-0.09,0.015,-0.111,-0.189,0.073,0.212,-0.746,0.631,-0.09,5.5,
  //                  0.826,0.015,7.1,1.12,0.913,0.211,0.437,0.516,
  //                  0.264,-0.591,-0.111,1.12,4.3,0.732,-0.324,0.159,-0.632,
  //                  -0.804,0.613,0.913,0.732,2.7,0.323,-0.217,0.409,
  //                  -0.443,-0.189,0.211,-0.324,0.323,2.9,0.737,-0.412,0.831,
  //                  0.093,0.651,0.073,0.159,-0.217,0.737,4.4,0.118,-0.516,
  //                  0.915,0.712,0.212,0.437,0.409,-0.412,0.118,5.1,
  //                  0.929,0.732,0.516,-0.632,0.831,-0.516,7.3};
  // double bb[12] = {1,2,3,4,5,6,7,8,9,10,11,12};
  // double x[12] = {1,2,3,4,5,6,7,8,9,10,11,12};
  // int niter1 = IterSolvers::pcg(aar,aac,aav,x,bb,N,1000,10e-8);
  // std::cout<<"iter1: "<<niter1<<std::endl;
  // for(int i=0;i<5;i++){
  //  std::cout<<x[i]<<std::endl;
  // }
  // for(int i=0;i<12;i++){x[i] = i+1;}
  // std::cout<<""<<std::endl;
  // int niter2 = IterSolvers::gmres(aar,aac,aav,x,bb,N,10,100,10e-8);
  // std::cout<<"iter2: "<<niter2<<std::endl;
  // for(int i=0;i<5;i++){
  //  std::cout<<x[i]<<std::endl;
  // }
}

