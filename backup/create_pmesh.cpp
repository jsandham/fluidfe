#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<cstring>
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;

const int MAX_NUM_GROUPS = 20;
const int MAX_NUM_ELEM_TYP = 20;
const int MAX_CHARS_PER_LINE = 512;
const int MAX_TOKENS_PER_LINE = 20;
const char* DELIMITER = " ";

int main()
{
  const char *file = "vmesh1.msh";

  ifstream fin;
  fin.open(file); 

  ofstream fout("pmesh1.msh");

  int flag=0,index=0,typ=0;

  //scan through input file (first pass)
  while(!fin.eof())
  {
    char buf[MAX_CHARS_PER_LINE];
    fin.getline(buf,MAX_CHARS_PER_LINE);

    const char* token[MAX_TOKENS_PER_LINE]={};

    int n = 0;

    // parse the line
    token[0] = strtok(buf,DELIMITER);
    if(token[0]){
      if(strcmp(token[0],"$Elements")==0) {flag=2; index=-2;}
      if(strcmp(token[0],"$EndElements")==0) {flag=0; index=-2;}
      for(n=1;n<MAX_TOKENS_PER_LINE;n++){
        token[n] = strtok(0,DELIMITER);
        if(!token[n]) break;
      }
    }

    if(flag==2 && index>=0){
      int typ = atoi(token[1]);
      switch(typ)
      {
        case 1:     //linear 1D lines
          //do nothing
          break;
        case 2:     //linear 2D triangles
          //do nothing
          break;
        case 3:     //linear 2D squares
          //do nothing
          break;
        case 4:     //linear 3D tetrahedra
          //do nothing
          break;
        case 8:     //quadratic 1D lines
          token[1] = "1";
          n = n - 1;
          break;
        case 9:     //quadratic 2D triangles
          token[1] = "2";
          n = n - 3;
          break;
        case 10:    //quadratic 2D squares
          token[1] = "3";
          n = n - 4;
          break;
        case 11:    //quadratic 3D tetrahedra
          token[1] = "4";
          n = n - 6;
          break;
        case 15:    //0D point
          //do nothing
          break;
      }
    }

    for(int i=0;i<n;i++){
      fout<<token[i]<<" ";
    }
    fout<<""<<endl;
    
    //for(int i=0;i<n;i++){cout<<token[i]<<" ";} 
    //cout<<""<<endl;
    index++;
  }
}
