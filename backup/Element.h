//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef ELEMENT_H
#define ELEMENT_H

class Element
{
  public:
    int nodes[30];           // global node numbers for element
    double xpts[30];         // global x-points for element
    double ypts[30];         // global y-points for element
    double zpts[30];         // global z-points for element
    double evec[30];         // element load vector
    double emat[30][30];     // element matrix    

  private:
    int Npe;                 // nodes per element
    int Type;                // element type
    int nDim;                // dimension of element
    int nGauss;              // number of Gauss points to use
    double jmat[3][3];       // jacobian matrix
    double imat[3][3];       // inverse jacobian matrix
    double nmat[30];         // temperature matrix
    double bmat[3][30];      // temperature differentiation matrix    
   
  public: 
    Element(int typ); 

    int get_Npe();
    int get_Type();
    int get_nDim();
    int get_nGauss();

    void stiffnessMatrix();
    void massMatrix();
    void advecXMatrix();
    void advecYMatrix();
    void advecZMatrix();   
    void elementVector();

    double value(double x, double y, double z, double v1, double v2, double v3);

  private:
    double jacobian(double xi, double et, double ze);
    double tempMatrix(double xi, double et, double ze);
    double tempDiffMatrix(double xi, double et, double ze);
};

#endif
