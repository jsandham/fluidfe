//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#include"Element.h"
#include"Solver.h"
#include"Mesh.h"
#include"FeModel.h"
#include"BoundaryData.h"
#include"IterSolvers.h"
#include"PostProcessing.h"

 

//********************************************************************************
//
//  Solvers for, poisson, heat and fluid equations
//
//********************************************************************************

  


//----------------------------------------------------------------------------
// Constructor for Solver
//----------------------------------------------------------------------------
Solver::Solver(Mesh *mesh1, FeModel *fem) : mesh1(mesh1), fem(fem)
{
  MAX_ITER = 10000;
  EPSILON = 1.0e-16;
  LARGE_NUM = 1.0e64;

  link1 = new int[10];        //link array not actually used. initialize for deletion later
  link2 = new int[10];        //link array not actually used. initialize for deletion later

  for(int i=0;i<10;i++){
    link1[i] = -1;
    link2[i] = -1;
  }     

  //determine upper bound on max number on non-zeros in rows of global matrices
  int *tmp = new int[mesh1->get_N()];
  for(int i=0;i<mesh1->get_N();i++){tmp[i] = 0;}
  for(int i=0;i<mesh1->get_Ne();i++){
    for(int j=0;j<mesh1->get_Npe();j++){
      tmp[mesh1->get_connect(i,j)-1]++;
    }
  }
  for(int i=0;i<mesh1->get_N();i++){tmp[i] = tmp[i]*mesh1->get_Npe();}
  MAX_NNZ = 0;
  for(int i=0;i<mesh1->get_N();i++){
    if(tmp[i]>MAX_NNZ){
      MAX_NNZ = tmp[i];
    }
  }
  delete [] tmp;
}



//----------------------------------------------------------------------------
// Constructor for Solver
//----------------------------------------------------------------------------
Solver::Solver(Mesh *mesh1, Mesh *mesh2, FeModel *fem) : mesh1(mesh1), mesh2(mesh2), fem(fem)
{
  MAX_ITER = 10000;
  EPSILON = 1.0e-16;
  LARGE_NUM = 1.0e64;

  link1 = new int[mesh1->get_N()];
  link2 = new int[mesh2->get_N()];       

  for(int i=0;i<mesh1->get_N();i++){link1[i] = -1;}
  for(int i=0;i<mesh2->get_N();i++){link2[i] = -1;}

  long count = 0;

  //fill in link1
  for(int i=0;i<mesh1->get_Ne();i++){
    for(int j=0;j<mesh1->get_Npe();j++){
      if(link1[mesh1->get_connect(i,j)-1]==-1){
        for(int k=0;k<mesh2->get_N();k++){
          count = count + 2;
          if(mesh2->get_xpoint(k)==mesh1->get_xpoint(mesh1->get_connect(i,j)-1) &&
             mesh2->get_ypoint(k)==mesh1->get_ypoint(mesh1->get_connect(i,j)-1) &&
             mesh2->get_zpoint(k)==mesh1->get_zpoint(mesh1->get_connect(i,j)-1)){
                link1[mesh1->get_connect(i,j)-1] = k+1;
                break;
          }
        }
      }
    }
  }

  //fill in link2
  for(int i=0;i<mesh2->get_Ne();i++){
    for(int j=0;j<mesh2->get_Npe();j++){
      if(link2[mesh2->get_connect(i,j)-1]==-1){
        for(int k=0;k<mesh1->get_N();k++){
          count = count + 2;
          if(mesh1->get_xpoint(k)==mesh2->get_xpoint(mesh2->get_connect(i,j)-1) &&
             mesh1->get_ypoint(k)==mesh2->get_ypoint(mesh2->get_connect(i,j)-1) && 
             mesh1->get_zpoint(k)==mesh2->get_zpoint(mesh2->get_connect(i,j)-1)){
                link2[mesh2->get_connect(i,j)-1] = k+1;
                break;
          }
        }
      }
    }
  }

  std::cout<<"count: "<<count<<std::endl;

  //determine upper bound on max number on non-zeros in rows of global matrices
  int *tmp = new int[mesh2->get_N()];
  for(int i=0;i<mesh2->get_N();i++){tmp[i] = 0;}
  for(int i=0;i<mesh2->get_Ne();i++){
    for(int j=0;j<mesh2->get_Npe();j++){
      tmp[mesh2->get_connect(i,j)-1]++;
    }
  }
  for(int i=0;i<mesh2->get_N();i++){tmp[i] = tmp[i]*mesh2->get_Npe();}
  MAX_NNZ = 0;
  for(int i=0;i<mesh2->get_N();i++){
    if(tmp[i]>MAX_NNZ){
      MAX_NNZ = tmp[i];
    }
  }
  delete [] tmp;

  std::cout<<"MAX_NNZ: "<<MAX_NNZ<<std::endl;
}



//----------------------------------------------------------------------------
// destructor for Solver
//----------------------------------------------------------------------------
Solver::~Solver()
{
  delete [] link1;
  delete [] link2;
}


//----------------------------------------------------------------------------
// assemble global sparse matrix
//----------------------------------------------------------------------------
void Solver::assembleGSM(int *row, int *col, double *A, int type, Mesh *mesh)
{
  int n = mesh->get_N();
  int r=0,c=0;
  double v=0;

  for(int i=0;i<n+1;i++){row[i] = 0;}
  for(int i=0;i<n*MAX_NNZ;i++){
    col[i] = -1;
    A[i] = 0.0;
  }

  for(int k=0;k<mesh->get_Ne();k++){
    Element e(mesh->get_Type());
    for(int l=0;l<e.get_Npe();l++){
      e.nodes[l] = mesh->get_connect(k,l);
      e.xpts[l] = mesh->get_xpoint(e.nodes[l]-1);
      e.ypts[l] = mesh->get_ypoint(e.nodes[l]-1);
      e.zpts[l] = mesh->get_zpoint(e.nodes[l]-1);
    }

    switch(type)
    {
    case 0:
      e.stiffnessMatrix();
      break;
    case 1:
      e.massMatrix();
      break;
    case 2:
      e.advecXMatrix();
      break;
    case 3:
      e.advecYMatrix();
      break;
    case 4:
      e.advecZMatrix();
      break;
    }

    //for(int i=0;i<e.Npe;i++){
    //  for(int j=0;j<e.Npe;j++){
    //    std::cout<<e.emat[i][j]<<"  ";   
    //  } 
    //  std::cout<<" "<<std::endl;
    //}
    //std::cout<<" "<<std::endl;

    //update col, A arrays
    for(int i=0;i<e.get_Npe();i++){
      for(int j=0;j<e.get_Npe();j++){
        r = e.nodes[i];
        c = e.nodes[j];
        v = e.emat[i][j];
        for(int p=MAX_NNZ*r-MAX_NNZ;p<MAX_NNZ*r;p++){
          if(col[p]==-1){
            col[p] = c-1;
            A[p] = v;
            break;
          }
          else if(col[p]==c-1){
            A[p] += v;
            break;
          }
        }
      }
    }
  }

  //update row array
  int jj = 0;
  for(int i=1;i<n+1;i++){
    for(int j=0;j<MAX_NNZ;j++){
      if(col[i*MAX_NNZ-MAX_NNZ+j]==-1){jj=j; break;}
    }
    row[i] = row[i-1] + jj;
  }

  //sort (insertion) col and A arrays
  for(int p=0;p<n;p++){
    for(int i=0;i<row[p+1]-row[p];i++){
      int entry = col[i+p*MAX_NNZ];
      double entryA = A[i+p*MAX_NNZ];
      int index = i+p*MAX_NNZ;
      for(int j=i-1;j>=0;j--){
        if(entry<col[j+p*MAX_NNZ]){
          int a = col[j+p*MAX_NNZ];
          double b = A[j+p*MAX_NNZ];
          col[j+p*MAX_NNZ] = entry;
          A[j+p*MAX_NNZ] = entryA;
          col[index] = a;
          A[index] = b;
          index = j+p*MAX_NNZ;
        }
      }
    }
  }

  //compress col and A arrays
  int index=0;
  for(int i=0;i<n*MAX_NNZ;i++){
    if(col[i]==-1)
      continue;
    col[index] = col[i];
    A[index] = A[i];
    index++;
  }

  //set unused parts of arrays to 0 or -1
  for(int i=index;i<n*MAX_NNZ;i++){
    col[i] = -1;
    A[i] = 0.0;
  }
}



//----------------------------------------------------------------------------
// Assemble Global Vector (y = A*x)
//----------------------------------------------------------------------------
void Solver::assembleGVEC(int *row, int *col, double *A, double *x, double *y, Mesh *mesh)
{
  int n = mesh->get_N();

  //set y = A*x
  for(int i=0;i<n;i++){
    y[i] = 0.0;
    for(int j=row[i];j<row[i+1];j++){
      y[i] += A[j]*x[col[j]];
    }
  }
}



//----------------------------------------------------------------------------
// Global Load Vector f
//----------------------------------------------------------------------------
void Solver::assembleGLV(double *fvec, Mesh *mesh)
{
  int n = mesh->get_N();

  double *tmp = new double[n];
  for(int i=0;i<n;i++){tmp[i] = 0.0;}

  //compute global load vector, i.e. internal heat generation vector fvec=Q
  for(int k=0;k<mesh->get_Ne();k++){
    Element e(mesh->get_Type());
    for(int l=0;l<e.get_Npe();l++){
      e.nodes[l] = mesh->get_connect(k,l);
      e.xpts[l] = mesh->get_xpoint(e.nodes[l]-1);
      e.ypts[l] = mesh->get_ypoint(e.nodes[l]-1);
      e.zpts[l] = mesh->get_zpoint(e.nodes[l]-1);
    }

    e.elementVector();
    for(int i=0;i<e.get_Npe();i++){
      //fvec[e.nodes[i]-1] += fem->get_Q()*e.evec[i];
      tmp[e.nodes[i]-1] += fvec[e.nodes[i]-1]*e.evec[i];
    }
  }

  for(int i=0;i<n;i++){fvec[i] = tmp[i];}

  delete [] tmp;
}




//----------------------------------------------------------------------------
// apply boundary conditions to global matrix system Ax = b
//----------------------------------------------------------------------------
void Solver::applyBCGSYS(int *row, int *col, double *A, double *b, BoundaryData *data, Mesh *mesh)
{
  // //apply dirichlet BC to A and b
  // for(int i=0;i<mesh->Ne_b;i++){
  //   int grp = mesh->connect_b[i][0];
  //   int node = mesh->connect_b[i][1]-1;
  //   if(data->isDirichlet(grp)){
  //     double val = data->getDirichletValue(grp);
  //     for(int j=0;j<mesh->N;j++){
  //       for(int k=row[j];k<row[j+1];k++){
  //         if(col[k]==node){
  //           b[j] = b[j] - A[k]*val;
  //           A[k] = 0.0;
  //           break;
  //         }
  //       }
  //     }
  //   }
  // }

  // for(int i=0;i<mesh->Ne_b;i++){
  //   int grp = mesh->connect_b[i][0];
  //   int node = mesh->connect_b[i][1]-1;
  //   if(data->isDirichlet(grp)){
  //     double val = data->getDirichletValue(grp);
  //     for(int j=row[node];j<row[node+1];j++){
  //       if(col[j]==node){
  //         A[j] = 1.0;
  //       }
  //       else{
  //         A[j]==0.0;
  //       }
  //     }
  //     b[node] = val;
  //   }
  // }
}




//----------------------------------------------------------------------------
// apply dirichlet boundary conditions to global sparse matrix A
//----------------------------------------------------------------------------
void Solver::applyBCGSM(int *row, int *col, double *A, BoundaryData *data, Mesh *mesh)
{
  //apply dirichlet BC to A using method of large numbers
  for(int i=0;i<mesh->get_Ne_b();i++){
    int node = mesh->get_connect_b(i,1)-1;
    if(data->isDirichlet(node)){
      for(int j=row[node];j<row[node+1];j++){
        if(col[j]==node){
          A[j] = LARGE_NUM;
          break;
        }
      }
    }
  }
}



//----------------------------------------------------------------------------
// apply dirichlet boundary conditions to global right hand side vector b
//----------------------------------------------------------------------------
void Solver::applyBCGVEC(double *b, BoundaryData *data, Mesh *mesh)
{
  //add boundary conditions to global RHS vector b
  for(int i=0;i<mesh->get_Ne_b();i++){
    int node = mesh->get_connect_b(i,1)-1;
    if(data->isDirichlet(node)){
      b[node] = data->getDirichletValue(node)*LARGE_NUM;
    }
    else if(data->isFlux(node)){
      Element e(mesh->get_Type_b());
      for(int k=0;k<e.get_Npe();k++){
        e.nodes[k] = mesh->get_connect_b(i,k+1);
        e.xpts[k] = mesh->get_xpoint(e.nodes[k]-1);
        e.ypts[k] = mesh->get_ypoint(e.nodes[k]-1);
        e.zpts[k] = mesh->get_zpoint(e.nodes[k]-1);
      }

      e.elementVector();
      for(int k=0;k<e.get_Npe();k++){
        b[e.nodes[k]-1] += data->getFluxValue(node)*e.evec[k];
      }
    }
  }
}














//********************************************************************************
//
//  Poisson equation solver
//
//  d^2u/dx^2 + d^2u/dy^2 + d^2u/dz^2 = f
//
//********************************************************************************


//----------------------------------------------------------------------------
// Constructor for poisson equation solver
//----------------------------------------------------------------------------
SolverPoisson::SolverPoisson(Mesh *mesh1, FeModel *fem) : Solver(mesh1,fem)
{
  filename = "poisson.msh";
  neq1 = mesh1->get_N();

  std::cout<<"MAX_NNZ: "<<MAX_NNZ<<std::endl;

  row1 = new int[neq1+1];
  col1 = new int[neq1*MAX_NNZ];

  K = new double[neq1*MAX_NNZ];

  u = new double[neq1];
  f = new double[neq1];

  for(int i=0;i<neq1+1;i++){row1[i] = 0;}
  for(int i=0;i<neq1*MAX_NNZ;i++){
    col1[i] = -1;
    K[i] = 0.0;
  }
  for(int i=0;i<neq1;i++){
    u[i] = 0.0;
    f[i] = 1.0;
  }
}



//----------------------------------------------------------------------------
// return pointer to poisson variable array 
//----------------------------------------------------------------------------
double* SolverPoisson::getPoissonVariable()
{
  return u;
}



//-------------------------------------------------------------------
// Solve poisson equation
//-------------------------------------------------------------------
int SolverPoisson::solve()
{
  int iter = 0;

  //assemble global stiffness
  assembleGSM(row1,col1,K,0,mesh1);
  assembleGLV(f,mesh1);

  //enforce dirichlet boundary conditions
  applyBCGSM(row1,col1,K,fem->tdata,mesh1);
  applyBCGVEC(f,fem->tdata,mesh1);
  iter = IterSolvers::pcg(row1,col1,K,u,f,neq1,MAX_ITER,EPSILON);

  //write solution vector to output file
  PostProcessing::write_results_gmsh_msh(u,filename,mesh1);

  return 0;
}



//----------------------------------------------------------------------------
// destructor for poisson solver
//----------------------------------------------------------------------------
SolverPoisson::~SolverPoisson()
{
  delete [] row1;
  delete [] col1;

  delete [] K;

  delete [] u;
  delete [] f;
}

















//********************************************************************************
//
//  Heat equation solver
//
//  c*rho*dT/dt = k*(d^2T/dx^2 + d^2T/dy^2 + d^2T/dz^2) 
//
//********************************************************************************



//----------------------------------------------------------------------------
// Constructor for heat equation solver
//----------------------------------------------------------------------------
SolverHeat::SolverHeat(Mesh *mesh1, FeModel *fem) : Solver(mesh1,fem)
{
  filename = "heat.msh";
  neq1 = mesh1->get_N();

  row1 = new int[neq1+1];
  col1 = new int[neq1*MAX_NNZ];

  K = new double[neq1*MAX_NNZ];
  M = new double[neq1*MAX_NNZ];

  temp = new double[neq1];
  b = new double[neq1];
  f = new double[neq1];

  for(int i=0;i<neq1+1;i++){row1[i] = 0;}
  for(int i=0;i<neq1*MAX_NNZ;i++){
    col1[i] = -1;
    K[i] = 0.0;
    M[i] = 0.0;
  }
  for(int i=0;i<neq1;i++){
    temp[i] = 0.0;
    b[i] = 0.0;
    f[i] = 0.0;
  }
}



//----------------------------------------------------------------------------
// return pointer to temperature array 
//----------------------------------------------------------------------------
double* SolverHeat::getTemperature()
{
  return temp;
}



//-------------------------------------------------------------------
// theta method timestepping
//-------------------------------------------------------------------
void SolverHeat::theta_method()
{
  int iter = 0;

  //assemble global b = (M-dt*(1-theta)/Re*K)*temp
  assembleGVEC(row1,col1,M,temp,b,mesh1);

  //solve using preconditioned conjugate gradient
  applyBCGVEC(b,fem->tdata,mesh1);

  iter = IterSolvers::pcg(row1,col1,K,temp,b,neq1,MAX_ITER,EPSILON);
}




//-------------------------------------------------------------------
// Solve heat equation
//-------------------------------------------------------------------
int SolverHeat::solve()
{
  double theta = 0.5;
  double alpha = fem->get_k()/(fem->get_rho()*fem->get_c());

  //assemble global stiffness and global mass matrices
  assembleGSM(row1,col1,K,0,mesh1);
  assembleGSM(row1,col1,M,1,mesh1);

  //form K = M+theta*dt*K and M = M-(1-theta)*dt*K 
  for(int i=0;i<row1[neq1];i++){
    double K_i = K[i];
    double M_i = M[i];
    K[i] = M_i+theta/alpha*fem->get_dt()*K_i;
    M[i] = M_i-(1-theta)/alpha*fem->get_dt()*K_i;
  }

  //enforce dirichlet boundary conditions
  applyBCGSM(row1,col1,K,fem->tdata,mesh1);

  //time loop
  for(int i=0;i<fem->get_steps();i++){
    std::cout<<"timestep: "<<i<<std::endl;
    std::cout<<"temp[0]: "<<temp[0]<<std::endl;
    theta_method(); 
  }

  //write solution vector to output file
  PostProcessing::write_results_gmsh_msh(temp,filename,mesh1);

  return 0;
}



//----------------------------------------------------------------------------
// destructor for heat solver
//----------------------------------------------------------------------------
SolverHeat::~SolverHeat()
{
  delete [] row1;
  delete [] col1;
  delete [] K;
  delete [] M;

  delete [] temp;
  delete [] b;
  delete [] f;
}



















//********************************************************************************
//
//  Fluid equations solver
//
//  du/dt + u*du/dx + v*du/dy + w*du/dz = -dp/dx + (1/Re)*(d^2u/dx^2 + d^2u/dy^2 d^2u/dz^2)
//  dv/dt + u*dv/dx + v*dv/dy + w*dv/dz = -dp/dy + (1/Re)*(d^2v/dx^2 + d^2v/dy^2 d^2v/dz^2)
//  dw/dt + u*dw/dx + v*dw/dy + w*dw/dz = -dp/dz + (1/Re)*(d^2w/dx^2 + d^2w/dy^2 d^2w/dz^2)
//  du/dx + dv/dy + dw/dz = 0
//
//********************************************************************************



//----------------------------------------------------------------------------
// Constructor for fluid (momentum and continuity equation) Solver
//----------------------------------------------------------------------------
SolverFluid::SolverFluid(Mesh *mesh1, Mesh *mesh2, FeModel *fem) : Solver(mesh1,mesh2,fem)
{
  ufilename = "fluid_u.msh";
  vfilename = "fluid_v.msh";
  wfilename = "fluid_w.msh";
  pfilename = "fluid_p.msh";

  neq1 = mesh1->get_N();
  neq2 = mesh2->get_N();

  row1 = new int[neq1+1];
  col1 = new int[neq1*MAX_NNZ];
  row2 = new int[neq2+1];
  col2 = new int[neq2*MAX_NNZ];

  K = new double[neq1*MAX_NNZ];
  M = new double[neq1*MAX_NNZ];
  Hx = new double[neq1*MAX_NNZ];
  Hy = new double[neq1*MAX_NNZ];
  Hz = new double[neq1*MAX_NNZ];
  Kp = new double[neq2*MAX_NNZ];
  Hxp = new double[neq2*MAX_NNZ];
  Hyp = new double[neq2*MAX_NNZ];
  Hzp = new double[neq2*MAX_NNZ];

  u_vel = new double[neq1];
  v_vel = new double[neq1];
  w_vel = new double[neq1];
  pres = new double[neq1];
  div = new double[neq2];

  u_old = new double[neq1];
  v_old = new double[neq1];
  w_old = new double[neq1];
  pres_old = new double[neq1];
 
  b_u = new double[neq1];
  b_v = new double[neq1];
  b_w = new double[neq1];
  
  for(int i=0;i<neq1+1;i++){row1[i] = 0;}
  for(int i=0;i<neq2+1;i++){row2[i] = 0;}
  for(int i=0;i<neq1*MAX_NNZ;i++){
    col1[i] = -1;
    K[i] = 0.0;
    M[i] = 0.0;
    Hx[i] = 0.0;
    Hy[i] = 0.0;
    Hz[i] = 0.0;
  }
  for(int i=0;i<neq2*MAX_NNZ;i++){
    col2[i] = -1;
    Kp[i] = 0.0;
    Hxp[i] = 0.0;
    Hyp[i] = 0.0;
    Hzp[i] = 0.0;
  }
  for(int i=0;i<neq1;i++){
    u_vel[i] = 0.0;
    v_vel[i] = 0.0;
    w_vel[i] = 0.0;
    u_old[i] = 0.0;
    v_old[i] = 0.0;
    w_old[i] = 0.0;
    pres[i] = 0.0;
    pres_old[i] = 0.0;
    b_u[i] = 0.0;
    b_v[i] = 0.0;
    b_w[i] = 0.0;
  }
  for(int i=0;i<neq2;i++){
    div[i] = 0.0;
  }
}



//----------------------------------------------------------------------------
// return pointer to u, v, w velocities and pressure for fluid solver
//----------------------------------------------------------------------------
double* SolverFluid::getUVelocity()
{
  return u_vel;
}


double* SolverFluid::getVVelocity()
{
  return v_vel;
}


double* SolverFluid::getWVelocity()
{
  return w_vel;
}


double* SolverFluid::getPressure()
{
  return pres;
}



//----------------------------------------------------------------------------
// theta method time stepping (2D)
// (see Finite element for incompressible Navier-Stokes equations by Ir. A. Segal 2015)
//----------------------------------------------------------------------------
void SolverFluid::theta_method2D(double theta)
{
  int iter = 0;

  //assemble global RHS = (M-dt*(1-theta)/Re*K)*u_old
  assembleGVEC(row1,col1,M,u_old,b_u,mesh1);
  assembleGVEC(row1,col1,M,v_old,b_v,mesh1);

  //add convective term to rhs
  for(int i=0;i<neq1;i++){
    for(int j=row1[i];j<row1[i+1];j++){
      b_u[i] -= fem->get_dt()*(1-theta)*(Hx[j]*u_old[col1[j]]*u_old[col1[j]] + 
                                  Hy[j]*u_old[col1[j]]*v_old[col1[j]]);
      b_v[i] -= fem->get_dt()*(1-theta)*(Hx[j]*v_old[col1[j]]*u_old[col1[j]] + 
                                  Hy[j]*v_old[col1[j]]*v_old[col1[j]]);
    }
  }

  //solve using preconditioned conjugate gradient
  applyBCGVEC(b_u,fem->udata,mesh1);
  applyBCGVEC(b_v,fem->vdata,mesh1);
  iter = IterSolvers::pcg(row1,col1,K,u_vel,b_u,neq1,MAX_ITER,EPSILON);
  iter = IterSolvers::pcg(row1,col1,K,v_vel,b_v,neq1,MAX_ITER,EPSILON);


  //calculate rhs of pressure poisson equation 
  for(int i=0;i<neq2;i++){div[i] = 0.0;}
  for(int i=0;i<neq2;i++){
    for(int j=row2[i];j<row2[i+1];j++){
      div[i] -= (Hxp[j]*u_vel[link2[col2[j]]-1] + 
                 Hyp[j]*v_vel[link2[col2[j]]-1])/(theta*fem->get_dt());
    }
  }
  for(int i=row2[0];i<row2[1];i++){Kp[i] = 0.0;}
  div[0] = 0.0; Kp[0] = 1.0;
  for(int i=1;i<neq2;i++){
    for(int j=row2[i];j<row2[i+1];j++){
      if(col2[j]==0){
        div[i] = div[i] - 1.0*Kp[j]; 
        Kp[j] = 0.0; 
        break;
     }
    }
  }

  //solve poisson equation -Kp*(pres-pres_old) = div/(2*dt) 
  double *ptemp = new double[neq2];
  for(int i=0;i<neq1;i++){pres[i] = 0.0;}
  for(int i=0;i<neq2;i++){ptemp[i] = 0.0;}
  iter = IterSolvers::pcg(row2,col2,Kp,ptemp,div,neq2,MAX_ITER,EPSILON);
  std::cout<<"iter: "<<iter<<std::endl; 
  for(int i=0;i<mesh1->get_Ne();i++){
    Element e(mesh2->get_Type());
    for(int j=0;j<e.get_Npe();j++){
      e.nodes[j] = mesh1->get_connect(i,j);
      e.xpts[j] = mesh1->get_xpoint(e.nodes[j]-1);
      e.ypts[j] = mesh1->get_ypoint(e.nodes[j]-1);
      e.zpts[j] = mesh1->get_zpoint(e.nodes[j]-1);
    }
    
    for(int j=0;j<mesh1->get_Npe();j++){
      if(link1[mesh1->get_connect(i,j)-1]==-1){
        double xpt = mesh1->get_xpoint(mesh1->get_connect(i,j)-1);
        double ypt = mesh1->get_ypoint(mesh1->get_connect(i,j)-1);
        double zpt = mesh1->get_zpoint(mesh1->get_connect(i,j)-1);
        double val1 = ptemp[link1[mesh1->get_connect(i,0)-1]-1];
        double val2 = ptemp[link1[mesh1->get_connect(i,1)-1]-1];
        double val3 = ptemp[link1[mesh1->get_connect(i,2)-1]-1];
        pres[mesh1->get_connect(i,j)-1] = e.value(xpt,ypt,zpt,val1,val2,val3); 
      }
      else{
        pres[mesh1->get_connect(i,j)-1] = ptemp[link1[mesh1->get_connect(i,j)-1]-1];
      }
    }
  }
  delete [] ptemp;

  
  //update velocities to include pressure
  assembleGVEC(row1,col1,K,u_vel,b_u,mesh1);
  assembleGVEC(row1,col1,K,v_vel,b_v,mesh1);
  for(int i=0;i<neq1;i++){
    for(int j=row1[i];j<row1[i+1];j++){
      b_u[i] -= fem->get_dt()*theta*Hx[j]*pres[col1[j]];
      b_v[i] -= fem->get_dt()*theta*Hy[j]*pres[col1[j]];
    }
  }
  
  applyBCGVEC(b_u,fem->udata,mesh1);
  applyBCGVEC(b_v,fem->vdata,mesh1);
  iter = IterSolvers::pcg(row1,col1,K,u_old,b_u,neq1,MAX_ITER,EPSILON);
  iter = IterSolvers::pcg(row1,col1,K,v_old,b_v,neq1,MAX_ITER,EPSILON);
}



//----------------------------------------------------------------------------
// theta method time stepping (3D)
// (see Finite element for incompressible Navier-Stokes equations by Ir. A. Segal 2015)
//----------------------------------------------------------------------------
void SolverFluid::theta_method3D(double theta)
{
  int iter = 0;

  //assemble global i.e. b_u = (M-dt*(1-theta)/Re*K)*u_old
  assembleGVEC(row1,col1,M,u_old,b_u,mesh1);
  assembleGVEC(row1,col1,M,v_old,b_v,mesh1);
  assembleGVEC(row1,col1,M,w_old,b_w,mesh1);


  //solve using preconditioned conjugate gradient
  applyBCGVEC(b_u,fem->udata,mesh1);
  applyBCGVEC(b_v,fem->vdata,mesh1);
  applyBCGVEC(b_w,fem->wdata,mesh1);
  iter = IterSolvers::pcg(row1,col1,K,u_vel,b_u,neq1,MAX_ITER,EPSILON);
  iter = IterSolvers::pcg(row1,col1,K,v_vel,b_v,neq1,MAX_ITER,EPSILON);
  iter = IterSolvers::pcg(row1,col1,K,w_vel,b_w,neq1,MAX_ITER,EPSILON);


  //calculate rhs of pressure poisson equation 
  for(int i=0;i<neq2;i++){div[i] = 0.0;}
  for(int i=0;i<neq2;i++){
    for(int j=row2[i];j<row2[i+1];j++){
      div[i] -= (Hxp[j]*u_vel[link2[col2[j]]-1] + 
                 Hyp[j]*v_vel[link2[col2[j]]-1] + 
                 Hzp[j]*w_vel[link2[col2[j]]-1])/(theta*fem->get_dt());
    }
  }
  for(int i=row2[0];i<row2[1];i++){Kp[i] = 0.0;}
  div[0] = 0.0; Kp[0] = 1.0;
  for(int i=1;i<neq2;i++){
    for(int j=row2[i];j<row2[i+1];j++){
      if(col2[j]==0){
        div[i] = div[i] - 1.0*Kp[j];
        Kp[j] = 0.0;
        break;
     }
    }
  }

  //solve poisson equation -Kp*(pres-pres_old) = div/(2*dt) 
  double *ptemp = new double[neq2];
  for(int i=0;i<neq1;i++){pres[i] = 0.0;}
  for(int i=0;i<neq2;i++){ptemp[i] = 0.0;}
  iter = IterSolvers::pcg(row2,col2,Kp,ptemp,div,neq2,MAX_ITER,EPSILON);
  std::cout<<"iter: "<<iter<<std::endl;
  for(int i=0;i<mesh1->get_Ne();i++){
    Element e(mesh2->get_Type());
    for(int j=0;j<e.get_Npe();j++){
      e.nodes[j] = mesh1->get_connect(i,j);
      e.xpts[j] = mesh1->get_xpoint(e.nodes[j]-1);
      e.ypts[j] = mesh1->get_ypoint(e.nodes[j]-1);
      e.zpts[j] = mesh1->get_zpoint(e.nodes[j]-1);
    }

    for(int j=0;j<mesh1->get_Npe();j++){
      if(link1[mesh1->get_connect(i,j)-1]==-1){
        double xpt = mesh1->get_xpoint(mesh1->get_connect(i,j)-1);
        double ypt = mesh1->get_ypoint(mesh1->get_connect(i,j)-1);
        double zpt = mesh1->get_zpoint(mesh1->get_connect(i,j)-1);
        double val1 = ptemp[link1[mesh1->get_connect(i,0)-1]-1];
        double val2 = ptemp[link1[mesh1->get_connect(i,1)-1]-1];
        double val3 = ptemp[link1[mesh1->get_connect(i,2)-1]-1];
        pres[mesh1->get_connect(i,j)-1] = e.value(xpt,ypt,zpt,val1,val2,val3);
      }
      else{
        pres[mesh1->get_connect(i,j)-1] = ptemp[link1[mesh1->get_connect(i,j)-1]-1];
      }
    }
  }
  delete [] ptemp;


  //update velocities to include pressure
  assembleGVEC(row1,col1,K,u_vel,b_u,mesh1);
  assembleGVEC(row1,col1,K,v_vel,b_v,mesh1);
  assembleGVEC(row1,col1,K,w_vel,b_w,mesh1);
  for(int i=0;i<neq1;i++){
    for(int j=row1[i];j<row1[i+1];j++){
      b_u[i] -= fem->get_dt()*theta*Hx[j]*pres[col1[j]];
      b_v[i] -= fem->get_dt()*theta*Hy[j]*pres[col1[j]];
      b_w[i] -= fem->get_dt()*theta*Hz[j]*pres[col1[j]];
    }
  }

  applyBCGVEC(b_u,fem->udata,mesh1);
  applyBCGVEC(b_v,fem->vdata,mesh1);
  applyBCGVEC(b_w,fem->wdata,mesh1);
  iter = IterSolvers::pcg(row1,col1,K,u_old,b_u,neq1,MAX_ITER,EPSILON);
  iter = IterSolvers::pcg(row1,col1,K,v_old,b_v,neq1,MAX_ITER,EPSILON);
  iter = IterSolvers::pcg(row1,col1,K,w_old,b_w,neq1,MAX_ITER,EPSILON);
}



//----------------------------------------------------------------------------
// Solve Fluid (momentum and continuity) equations
//----------------------------------------------------------------------------
int SolverFluid::solve()
{
  double theta = 0.5;

  void (SolverFluid::*timestepper)(double) = NULL;
 

  //assemble global matrices
  assembleGSM(row1,col1,K,0,mesh1);
  assembleGSM(row1,col1,M,1,mesh1);
  assembleGSM(row1,col1,Hx,2,mesh1);
  assembleGSM(row1,col1,Hy,3,mesh1);
  assembleGSM(row1,col1,Hz,4,mesh1);
  assembleGSM(row2,col2,Kp,0,mesh2);
  assembleGSM(row2,col2,Hxp,2,mesh2);
  assembleGSM(row2,col2,Hyp,3,mesh2);
  assembleGSM(row2,col2,Hzp,4,mesh2); 


  //form K = M+theta*dt*K and M = M-(1-theta)*dt*K 
  for(int i=0;i<row1[neq1];i++){
    double K_i = K[i];
    double M_i = M[i];
    K[i] = M_i+theta/fem->get_Re()*fem->get_dt()*K_i;
    M[i] = M_i-(1-theta)/fem->get_Re()*fem->get_dt()*K_i;
  }


  //enforce dirichlet boundary conditions
  //applyBCGSM(row1,col1,K,(fem->udata)->groups,(fem->udata)->types,mesh1);
  applyBCGSM(row1,col1,K,fem->udata,mesh1);


  //set time stepper and solve
  switch(mesh1->get_Dim())
  {
    case 2:
      timestepper = &SolverFluid::theta_method2D;
      break;
    case 3:
      timestepper = &SolverFluid::theta_method3D;
      break;
  }

  for(int i=0;i<fem->get_steps();i++){
    std::cout<<"timestep: "<<i<<std::endl;
    std::cout<<"u_vel[0]: "<<u_vel[0]<<std::endl;
    (*this.*timestepper)(theta); // ugly c++...
  }


  //write solution vector to output file
  PostProcessing::write_results_gmsh_msh(u_vel,ufilename,mesh1);
  PostProcessing::write_results_gmsh_msh(v_vel,vfilename,mesh1);
  PostProcessing::write_results_gmsh_msh(pres,pfilename,mesh1);

  if(mesh1->get_Dim()==3){
    PostProcessing::write_results_gmsh_msh(w_vel,wfilename,mesh1);
  }
  
  return 0;
}




//----------------------------------------------------------------------------
// destructor for fluid solver
//----------------------------------------------------------------------------
SolverFluid::~SolverFluid()
{
  delete [] row1;
  delete [] col1;
  delete [] row2;
  delete [] col2;

  delete [] K;
  delete [] M;
  delete [] Hx;
  delete [] Hy;
  delete [] Hz;
  delete [] Kp;
  delete [] Hxp;
  delete [] Hyp;
  delete [] Hzp;

  delete [] u_vel;
  delete [] v_vel;
  delete [] w_vel;
  delete [] div;
  delete [] pres;

  delete [] u_old;
  delete [] v_old;
  delete [] w_old;
  delete [] pres_old;

  delete [] b_u;
  delete [] b_v;
  delete [] b_w;
}




//----------------------------------------------------------------------------
// Global Force Vector f
//----------------------------------------------------------------------------
// void Solver::assembleGFV(double *fvec, Mesh *mesh)
// {
//   int n = mesh->N;

//   double *temp = new double[n];
//   for(int i=0;i<n;i++){temp[i] = 0.0;}

//   //for(int i=0;i<n;i++){fvec[i] = 0.0;}

//   //compute internal heat generation vector fvec=Q
//   for(int k=0;k<mesh->Ne;k++){
//     Element e(mesh->Type);
//     for(int l=0;l<e.Npe;l++){
//       e.nodes[l] = mesh->connect[k][l];
//       e.xpts[l] = mesh->xpoints[e.nodes[l]-1];
//       e.ypts[l] = mesh->ypoints[e.nodes[l]-1];
//       e.zpts[l] = mesh->zpoints[e.nodes[l]-1];
//     }

//     e.elementVector();
//     for(int i=0;i<e.Npe;i++){
//       //fvec[e.nodes[i]-1] += fem->get_Q()*e.evec[i];
//       temp[e.nodes[i]-1] += fvec[e.nodes[i]-1]*e.evec[i];
//     }
//   }

//   for(int i=0;i<n;i++){fvec[i] = temp[i];}

//   delete [] temp;
// }
