//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#include<iostream>
#include<math.h>
#include"QuadratureRule.h"

 
//--------------------------------------------------------------------------------
// quadrature rule constructor 
//--------------------------------------------------------------------------------
QuadratureRule::QuadratureRule(int n, int dim, int typ)
{
  nDim = dim;

  //static quadrature tables
  static double point[] = {1.0};

  static double lin_1D1[] = {0.0,
                             2.0};

  static double lin_1D2[] = {-1.0/sqrt(3.0),1.0/sqrt(3.0),
                             1.0,1.0};

  static double lin_1D3[] = {-sqrt(3.0/5.0),0.0,sqrt(3.0/5.0),
                             5.0/9.0,8.0/9.0,5.0/9.0};

  static double tri_2D1[] = {1.0/3.0,
                             1.0/3.0,
                             1.0};

  static double tri_2D3[] = {1.0/6.0,2.0/3.0,1.0/6.0,
                             1.0/6.0,1.0/6.0,2.0/3.0,
                             1.0/6.0,1.0/6.0,1.0/6.0};

  static double tri_2D4[] = {1.0/3.0,3.0/5.0,1.0/5.0,1.0/5.0,
                             1.0/3.0,1.0/5.0,3.0/5.0,1.0/5.0,
                            -9.0/32.0,25.0/96.0,25.0/96.0,25.0/96.0};

  static double tri_2D7[] = {0.0,1.0/2.0,1.0,1.0/2.0,0.0,0.0,1.0/3.0,
                             0.0,0.0,0.0,1.0/2.0,1.0,1.0/2.0,1.0/3.0,
                             1.0/40.0,1.0/15.0,1.0/40.0,1.0/15.0,1.0/40.0,1.0/15.0,9.0/40.0};

  static double sqr_2D1[] = {0.0,
                             0.0,
                             4.0};

  static double sqr_2D4[] = {-1.0/sqrt(3.0),1.0/sqrt(3.0),-1.0/sqrt(3.0),1.0/sqrt(3.0),
                             -1.0/sqrt(3.0),-1.0/sqrt(3.0),1.0/sqrt(3.0),1.0/sqrt(3.0),
                              1.0,1.0,1.0,1.0};

  static double sqr_2D9[] = {-sqrt(3.0/5.0),0.0,sqrt(3.0/5.0),-sqrt(3.0/5.0),0.0,sqrt(3.0/5.0),
                             -sqrt(3.0/5.0),0.0,sqrt(3.0/5.0),-sqrt(3.0/5.0),-sqrt(3.0/5.0),
                             -sqrt(3.0/5.0),0.0,0.0,0.0,sqrt(3.0/5.0),sqrt(3.0/5.0),sqrt(3.0/5.0),
                              25.0/81.0,40.0/81.0,25.0/81.0,40.0/81.0,64.0/81.0,40.0/81.0,
                              25.0/81.0,40.0/81.0,25.0/81.0};

  static double tet_3D1[] = {1.0/4.0,
                             1.0/4.0,
                             1.0/4.0,
                             1.0/6.0};

  static double tet_3D4[] = {(5.0+3.0*sqrt(5.0))/20.0,(5.0-sqrt(5.0))/20.0,(5.0-sqrt(5.0))/20.0,
                             (5.0-sqrt(5.0))/20.0,(5.0-sqrt(5.0))/20.0,(5.0+3.0*sqrt(5.0))/20.0,
                             (5.0-sqrt(5.0))/20.0,(5.0-sqrt(5.0))/20.0,(5.0-sqrt(5.0))/20.0,                                         (5.0-sqrt(5.0))/20.0,(5.0+3.0*sqrt(5.0))/20.0,(5.0-sqrt(5.0))/20.0,
                             1.0/24.0,1.0/24.0,1.0/24.0,1.0/24.0};

  static double tet_3D5[] = {1.0/4.0,1.0/2.0,1.0/6.0,1.0/6.0,1.0/6.0,
                             1.0/4.0,1.0/6.0,1.0/2.0,1.0/6.0,1.0/6.0,
                             1.0/4.0,1.0/6.0,1.0/6.0,1.0/2.0,1.0/6.0,
                             -4.0/30.0,9.0/120.0,9.0/120.0,9.0/120.0,9.0/120.0};

  //set pointer to correct table depending on element type
  switch(typ)
  {
    case 1:
      if(n==1){pt = &lin_1D1[0];}
      else if(n==2){pt = &lin_1D2[0];}
      else if(n==3){pt = &lin_1D3[0];}
      break;
    case 2:
      if(n==1){pt = &tri_2D1[0];}
      else if(n==3){pt = &tri_2D3[0];}
      else if(n==4){pt = &tri_2D4[0];}
      break;
    case 3:
      if(n==1){pt = &sqr_2D1[0];}
      else if(n==4){pt = &sqr_2D4[0];}
      else if(n==9){pt = &sqr_2D9[0];}
      break;
    case 4:
      if(n==1){pt = &tet_3D1[0];}
      else if(n==4){pt = &tet_3D4[0];}
      else if(n==5){pt = &tet_3D5[0];}
      break;
    case 8:
      if(n==1){pt = &lin_1D1[0];}
      else if(n==2){pt = &lin_1D2[0];}
      else if(n==3){pt = &lin_1D3[0];}
      break;
    case 9:
      if(n==1){pt = &tri_2D1[0];}
      else if(n==3){pt = &tri_2D3[0];}
      else if(n==4){pt = &tri_2D4[0];}
      break;
    case 10:
      if(n==1){pt = &sqr_2D1[0];}
      else if(n==4){pt = &sqr_2D4[0];}
      else if(n==9){pt = &sqr_2D9[0];}
      break;
    case 11:
      if(n==1){pt = &tet_3D1[0];}
      else if(n==4){pt = &tet_3D4[0];}
      else if(n==5){pt = &tet_3D5[0];}
      break;
    case 15:
      if(n==1){pt = &point[0];}
      break;
  }

  nIntPoints = n;

  wi = new double[nIntPoints];
  if(nDim>0){xii = new double[nIntPoints];}
  if (nDim>1){eti = new double[nIntPoints];}
  if (nDim>2){zei = new double[nIntPoints];}

  switch (nDim)
  {
  case 0:    //0D
    for(int i=0;i<nIntPoints;i++){
      wi[i] = pt[i];
    }
    break;
  case 1:    //1D
    for(int i=0;i<nIntPoints;i++){
      xii[i] = pt[i];
      wi[i] = pt[i+n];
    }
    break;

  case 2:    //2D
    for(int i=0;i<nIntPoints;i++){
      xii[i] = pt[i];
      eti[i] = pt[i+n];
      wi[i]  = pt[i+2*n];
    }
    break;

  case 3:    //3D
    for(int i=0;i<nIntPoints;i++){
      xii[i] = pt[i];
      eti[i] = pt[i+n];
      zei[i] = pt[i+2*n];
      wi[i]  = pt[i+3*n];
    }
    break;
  }
}


//--------------------------------------------------------------------------------
// quadrature rule destructor
//--------------------------------------------------------------------------------
QuadratureRule::~QuadratureRule()
{
  delete [] wi;
  if (nDim>0){delete [] xii;}
  if (nDim>1){delete [] eti;}
  if (nDim>2){delete [] zei;}
}
