//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QGLWidget>

class GLWidget : public QGLWidget
{
  Q_OBJECT

  public:
    GLWidget(QWidget *parent = 0);
    ~GLWidget();

    GLuint setObjectToZero();
    GLuint setObjectToMesh(int **connect, double *x_pts, double *y_pts, double *z_pts, int ne, int typ);  
    GLuint setObjectToSoln(int **connect, double *x, double *x_pts, double *y_pts, double *z_pts, 
                           int ne, int typ); 
    QSize minimumSizeHint() const;
    QSize sizeHint() const;

  public slots:
    void setXRotation(int angle);
    void setYRotation(int angle);
    void setZRotation(int angle);
    void setXTranslation(int increment);
    void setYTranslation(int increment);
    void setScale(double factor);

  signals:
    void xRotationChanged(int angle);
    void yRotationChanged(int angle);
    void zRotationChanged(int angle);
    void xTranslationChanged(int increment);
    void yTranslationChanged(int increment);
    void scaleChanged(double factor);

  protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

  private:
    void drawWireElement(int *nodes, double *x_pts, double *y_pts, double *z_pts, int npe);
    void drawSolidElement(int *nodes, double *x, double *x_pts, double *y_pts, double *z_pts, int npe);
    double jetColorRed(double s);
    double jetColorGreen(double s);
    double jetColorBlue(double s);
    void normalizeAngle(int *angle);

    int xRot, yRot, zRot;
    double xTrans, yTrans;
    double scale;

    GLuint object;
    QPoint lastPos;
    QColor trolltechGreen;
    QColor trolltechPurple;
};

#endif
