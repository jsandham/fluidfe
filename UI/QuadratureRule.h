//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef QUADRATURE_H
#define QUADRATURE_H

class QuadratureRule
{
  public:
    int nDim;
    int nIntPoints;
    double *xii, *eti, *zei;
    double *wi;

  private:
    double *pt;

  public: 
    QuadratureRule(int n, int dim, int typ);
    ~QuadratureRule();
};

#endif




































//double f(double x,double y)
//{
//  return x*y;
//}

//QuadratureRule g(4,2);
//double I;
//double* s;
//double* t;
//s=g.xii;
//t=g.eti;

//I=0;
//for(int i=0;i<4;i++)
//{
//  I=I+g.wi[i]*f(g.xii[i],g.eti[i]);
  //cout<<g.wi[i]<<endl;
//}

//cout<<I<<endl;




//int TriLin2D::stiffnessMatrix()
//{ 
//  QuadratureRule g(3,2);
//  double I=0, J=0, C=0,area=0;
//
//  J = xpts[1]*(ypts[2]-ypts[0]) - xpts[2]*(ypts[1]-ypts[0]) + xpts[0]*(ypts[1]-ypts[2]);
//
//  for(int k=0;k<3;k++)
//    I = I + g.wi[k];
//
//  C = (0.25/(J*I));
//  kmat[0][0] = C*((ypts[1]-ypts[2])*(ypts[1]-ypts[2]) + (xpts[2]-xpts[1])*(xpts[2]-xpts[1]));
//  kmat[0][1] = C*((ypts[1]-ypts[2])*(ypts[2]-ypts[0]) + (xpts[2]-xpts[1])*(xpts[0]-xpts[2]));
//  kmat[0][2] = C*((ypts[1]-ypts[2])*(ypts[0]-ypts[1]) + (xpts[2]-xpts[1])*(xpts[1]-xpts[0]));
//  kmat[1][1] = C*((ypts[2]-ypts[0])*(ypts[2]-ypts[0]) + (xpts[0]-xpts[2])*(xpts[0]-xpts[2]));
//  kmat[1][2] = C*((ypts[2]-ypts[0])*(ypts[0]-ypts[1]) + (xpts[0]-xpts[2])*(xpts[1]-xpts[0]));
//  kmat[2][2] = C*((ypts[0]-ypts[1])*(ypts[0]-ypts[1]) + (xpts[1]-xpts[0])*(xpts[1]-xpts[0]));
//  kmat[1][0] = C*((ypts[1]-ypts[2])*(ypts[2]-ypts[0]) + (xpts[2]-xpts[1])*(xpts[0]-xpts[2]));
//  kmat[2][0] = C*((ypts[1]-ypts[2])*(ypts[0]-ypts[1]) + (xpts[2]-xpts[1])*(xpts[1]-xpts[0]));
//  kmat[2][1] = C*((ypts[2]-ypts[0])*(ypts[0]-ypts[1]) + (xpts[0]-xpts[2])*(xpts[1]-xpts[0]));
//  return 0;
//}





  //det = displaceMatrix(1.0,1.0,1.0);

  //QuadratureRule g(3,2);
  //for(int k=0;k<3;k++)
  //  a = a + g.wi[k];

  //for(int i=0;i<3;i++)
  //  for(int j=0;j<3;j++)
  //  {
  //    for(int k=0;k<2;k++)
  //      kmat[i][j] = kmat[i][j] + bmat[k][j]*bmat[k][i]; 
  //    kmat[i][j] = det*a*kmat[i][j];
  //  }
