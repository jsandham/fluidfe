//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************

#ifndef SOLVER_H
#define SOLVER_H
#include"Mesh.h"
#include"FeModel.h"


class Solver
{
  public:
    Mesh mesh;       //finie element mesh
    FeModel fem;     //finite element model

  public:
    Solver(Mesh mesh, FeModel fem);

    virtual int solve() = 0;   //solve linear system
 
    int pcg(int row[], int col[], double A[], double x[], double b[], int n);

  //private:
    void diagonalPreconditioner(int row[], int col[], double A[], double md[], int n);
    void matrixVectorProduct(int row[], int col[], double A[], double x[], double y[], int n);
    void assembleGSM(int *row, int *col, double *A, int type, int n);
    void assembleGLM(int *row, double *A, double *L, int n);
    void assembleGRHSV(int *row, int *col, double *A, double *rhs, double *vec, int n);
    void assembleGFV(double *fvec, int n);
    void dirichletGSM(int *row, int *col, double *A, int *groups, int *types, int Ng);
    void dirichletGRHSV(double *rhs, int *groups, int *types, double *values, int Ng);
};




class SolverHeat : public Solver
{
  private:
    int neq;           //number of equations (size of global matrices)
    int* krow;         //pointers to stiffness matrix rows
    int* kcol;         //column numbers for nonzero values in K
    double* K;         //nonzero values of the global stiffness matrix by row
    
    int* mrow;         //pointers to mass matrix rows
    int* mcol;         //column numbers for nonzero values in M
    double* M;         //nonzero values of the global mass matrix by row

    double *temp, *b, *f;  //temperature and RHS

  public:
    SolverHeat(Mesh mesh, FeModel fem);
    ~SolverHeat();

    double* getTemperature(); 

    virtual int solve();

  private:
    void diffusion();
    void writeTemperature(); 
};




class SolverFluid : public Solver
{
  private:
    int neq;         //number of equations (size of global matrices)
    int *krow;       //pointers to stiffness matrix rows
    int *kcol;       //column numbers for nonzero values in K
    double* K;       //nonzero values of the global stiffness matrix by row
  
    int *mrow;       //pointers to mass matrix rows
    int *mcol;       //column numbers for nonzero values in M
    double* M;       //nonzero values of the global mass matrix by row

    int *hxrow;      //pointers to x-advection matrix rows
    int *hxcol;      //column numbers for nonzero values in Hx
    double* Hx;      //nonzero values of the global x-advection matrix by row

    int *hyrow;      //pointers to y-advection matrix rows
    int *hycol;      //column numbers for nonzero values in Hy
    double* Hy;      //nonzero values of the global y-advection matrix by row

    int *hzrow;      //pointers to z-advection matrix rows
    int *hzcol;      //column numbers for nonzero values in Hz
    double* Hz;      //nonzero values of the global z-advection matrix by row

    int *kprow;       //pointers to pressure stiffness matrix rows
    int *kpcol;       //column numbers for nonzero values in Kp
    double* Kp;       //nonzero values of the global pressure stiffness matrix by row

    double *LM;      //nonzero diagonal values of the global lumped mass matrix

    double *u_vel, *u_old;     //u velocity 
    double *v_vel, *v_old;     //v velocity
    double *w_vel, *w_old;     //w velocity
    double *div;               //divergence
    double *pres, *pres_old;   //pressure

    double *b_u;       //RHS vector b_u
    double *b_v;       //RHS vector b_v
    double *b_w;       //RHS vector b_w

  public: 
    SolverFluid(Mesh mesh, FeModel fem);
    ~SolverFluid();

    double* getUVelocity();
    double* getVVelocity();
    double* getWVelocity();
    double* getPressure();

    virtual int solve();

  private:
    void fadvec(double *u_in, double *v_in, double *fu_out, double *fv_out);
    void rk1();
    void rk2();
    void rk4();

    void advection();
    void diffusion();
    void divergence();
    void pressure();

    void writeUVelocity();  
    void writeVVelocity();   
    void writeWVelocity();   
};


#endif
