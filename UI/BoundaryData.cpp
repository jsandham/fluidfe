//********************************************************************************
//*
//*  C++ finite element method for heat equation
//*  James sandham
//*  15 April 2015
//*
//********************************************************************************

//********************************************************************************
//
// HeatFE is free software; you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License (as published by the Free
// Software Foundation) version 2.1 dated February 1999.
//
//********************************************************************************


#include "BoundaryData.h"


//----------------------------------------------------------------------------
// constructor for boundary data class
//----------------------------------------------------------------------------
BoundaryData::BoundaryData(int n)
{
  numOfGrps = n;
  groups = new int[numOfGrps];
  types = new int[numOfGrps];
  values = new double[numOfGrps];

  for(int i=0;i<numOfGrps;i++){groups[i] = 0;}
  for(int i=0;i<numOfGrps;i++){types[i] = 0;}
  for(int i=0;i<numOfGrps;i++){values[i] = 0.0;}
}


//----------------------------------------------------------------------------
// destructor for boundary data class
//----------------------------------------------------------------------------
BoundaryData::~BoundaryData()
{
  delete[] groups;
  delete[] types;
  delete[] values;
}


